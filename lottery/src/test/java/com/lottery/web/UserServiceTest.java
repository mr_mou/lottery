package com.lottery.web;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.lottery.web.dao.IAccountDao;
import com.lottery.web.model.Account;

public class UserServiceTest {

	public static void main(String[] args) {
		ApplicationContext ctx = null;
		ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		IAccountDao userDao = (IAccountDao) ctx.getBean("accountDao");
		Account user = new Account();
		// 添加数据
		user.setUserCode("ag_2");
		user.setUserName("zhimou2");
		user.setPassword("123");
		user.setRole("agent");
		userDao.saveUser(user);
		
		// 查询数据
//		user.setName("zhimou");
//		user.setPassword("123");
//		System.out.println(userDao.getUser(user).toString());
		
		// 修改数据
//		user.setPassword("234");
//		userDao.updateUser(user);
//		System.out.println("修改成功");

	}
}
