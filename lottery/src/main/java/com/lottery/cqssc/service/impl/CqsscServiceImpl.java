package com.lottery.cqssc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lottery.common.dto.CqsscDTO;
import com.lottery.common.exception.BIZException;
import com.lottery.cqssc.service.CqsscService;
import com.lottery.web.dao.ICqsscDAO;

/**
 * 
 * @描述: 重庆时时彩服务类
 *
 * @author LiBing
 * @date 2016年10月23日 下午4:25:03
 *
 */
@Component("cqsscService")
public class CqsscServiceImpl implements CqsscService {
	
	@Autowired
	private ICqsscDAO cqsscDAO;

	@Override
	public void createCqssc(CqsscDTO cqsscDto) throws BIZException {
		cqsscDAO.createCqssc(cqsscDto);

	}

	@Override
	public void updateCqssc(CqsscDTO cqsscDto) throws BIZException {
		cqsscDAO.updateCqssc(cqsscDto);

	}

	@Override
	public void deleteCqssc(CqsscDTO cqsscDto) throws BIZException {
		cqsscDAO.deleteCqssc(cqsscDto);

	}

	@Override
	public CqsscDTO queryCqssc(CqsscDTO cqsscDto) throws BIZException {
		return cqsscDAO.queryCqssc(cqsscDto);
	}

	@Override
	public CqsscDTO queryLastestCqssc() throws BIZException {
		return cqsscDAO.queryLastestCqssc();
	}
	
}
