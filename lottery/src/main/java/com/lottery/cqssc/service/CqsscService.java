package com.lottery.cqssc.service;

import com.lottery.common.dto.CqsscDTO;
import com.lottery.common.exception.BIZException;

/**
 * 
 * @描述: 重庆时时彩服务类
 *
 * @author LiBing
 * @date 2016年10月23日 下午4:21:44
 *
 */
public interface CqsscService {
	
	/**
	 * 
	 * @描述: 插入重庆时时彩开奖结果
	 * 
	 * @param cqsscDto
	 * @throws BIZException
	 */
	public void createCqssc(CqsscDTO cqsscDto) throws BIZException;
	
	/**
	 * 
	 * @描述: 更新重庆时时彩开奖结果
	 * 
	 * @param cqsscDto
	 * @throws BIZException
	 */
	public void updateCqssc(CqsscDTO cqsscDto) throws BIZException;
	
	/**
	 * 
	 * @描述: 删除重庆时时彩开奖结果
	 * 
	 * @param cqsscDto
	 * @throws BIZException
	 */
	public void deleteCqssc(CqsscDTO cqsscDto) throws BIZException;
	
	/**
	 * 
	 * @描述: 查询重庆时时彩开奖结果
	 * 
	 * @param cqsscDto
	 * @throws BIZException
	 */
	public CqsscDTO queryCqssc(CqsscDTO cqsscDto) throws BIZException;
	
	/** 
	 * @描述: 查询重庆时时彩最新的开奖结果
	 * 
	 * @param cqsscDto
	 * @throws DAOException
	 */
	public CqsscDTO queryLastestCqssc() throws BIZException;

}
