package com.lottery.common.dto;

import java.util.List;
import java.util.Map;

/**
 * 接口入参
 * @author apple
 *
 */
public class CommonListMapDTO {
	
	private List<Map<String,Object>> commonList;

	public List<Map<String, Object>> getCommonList() {
		return commonList;
	}

	public void setCommonList(List<Map<String, Object>> commonList) {
		this.commonList = commonList;
	}
	
	
}
