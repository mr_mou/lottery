package com.lottery.common.dto;

/**
 * 
 * @描述: 重庆时时彩
 *
 * @author LiBing
 * @date 2016年10月21日 下午5:03:19
 *
 */
public class CqsscDTO extends AbstractLotteryDTO{

	// 1. 期数 e.g. "expect":"20161021065";
	private String expect;
	
	// 2. 开奖号码 e.g."opencode":"0,0,9,3,4";
	private String opencode;
	
	// 3. 开奖时间 e.g. "opentime":"2016-10-21 16:50:40";
	private String opentime;
	
	// 4. 开奖时间戳 e.g. "opentimestamp":1477039840;
	private String opentimestamp;

	public String getExpect() {
		return expect;
	}

	public void setExpect(String expect) {
		this.expect = expect;
	}

	public String getOpencode() {
		return opencode;
	}

	public void setOpencode(String opencode) {
		this.opencode = opencode;
	}

	public String getOpentimestamp() {
		return opentimestamp;
	}

	public void setOpentimestamp(String opentimestamp) {
		this.opentimestamp = opentimestamp;
	}

	public String getOpentime() {
		return opentime;
	}

	public void setOpentime(String opentime) {
		this.opentime = opentime;
	}

	
}
