package com.lottery.common.dto;

import java.util.List;

/**
 * 
 * @描述: 彩票Response
 *
 * @author LiBing
 * @date 2016年10月21日 下午5:13:01
 *
 */
public class CqsscResponseDTO {
	
	private int rows;
	private String code;
	private String info;
	private List<CqsscDTO> data;
	
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public List<CqsscDTO> getData() {
		return data;
	}
	public void setData(List<CqsscDTO> data) {
		this.data = data;
	}
}
