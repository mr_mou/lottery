package com.lottery.common.props;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 
 * @描述: 自动加载properties文件的内容到Bean中
 *
 * @author LiBing
 * @date 2016年10月21日 下午3:35:20
 *
 */
@Component("contextProperties")
public class ContextLotteryProperties {
	//1. 重庆时时彩的URL
	@Value("${cqssc.url}")
	private String cqsscURL;
	
	//2. 山东11选5的URL
	@Value("${sd11x5.url}")
	private String sd11x5URL;
	
 	//3. 广东11选5的URL
	@Value("${gd11x5.url}")
	private String gd11x5URL;
	
 	//4. 江西11选5的URL
	@Value("${jx11x5.url}")
	private String jx11x5URL;
	
 	//5. 浙江11选5的URL
	@Value("${zj11x5.url}")
	private String zj11x5URL;
	
 	//6. 江苏快3(老快3)的URL
	@Value("${juk3.url}")
	private String juk3URL;
	
 	//7. 吉林快3(新快3)的URL
	@Value("${jlk3.url}")
	private String jlk3URL;
	
 	//6. 安徽快3(乐快3)的URL
	@Value("${ahk3.url}")
	private String ahk3URL;
	
 	//8. 排列三的URL
	@Value("${pl3.url}")
	private String pl3URL;
	
 	//9. 福彩3D(一天开一次)的URL
	@Value("${fc3d.url}")
	private String fc3dURL;

	public String getCqsscURL() {
		return cqsscURL;
	}

	public void setCqsscURL(String cqsscURL) {
		this.cqsscURL = cqsscURL;
	}

	public String getSd11x5URL() {
		return sd11x5URL;
	}

	public void setSd11x5URL(String sd11x5url) {
		sd11x5URL = sd11x5url;
	}

	public String getGd11x5URL() {
		return gd11x5URL;
	}

	public void setGd11x5URL(String gd11x5url) {
		gd11x5URL = gd11x5url;
	}

	public String getJx11x5URL() {
		return jx11x5URL;
	}

	public void setJx11x5URL(String jx11x5url) {
		jx11x5URL = jx11x5url;
	}

	public String getZj11x5URL() {
		return zj11x5URL;
	}

	public void setZj11x5URL(String zj11x5url) {
		zj11x5URL = zj11x5url;
	}

	public String getJuk3URL() {
		return juk3URL;
	}

	public void setJuk3URL(String juk3url) {
		juk3URL = juk3url;
	}

	public String getJlk3URL() {
		return jlk3URL;
	}

	public void setJlk3URL(String jlk3url) {
		jlk3URL = jlk3url;
	}

	public String getAhk3URL() {
		return ahk3URL;
	}

	public void setAhk3URL(String ahk3url) {
		ahk3URL = ahk3url;
	}

	public String getPl3URL() {
		return pl3URL;
	}

	public void setPl3URL(String pl3url) {
		pl3URL = pl3url;
	}

	public String getFc3dURL() {
		return fc3dURL;
	}

	public void setFc3dURL(String fc3dURL) {
		this.fc3dURL = fc3dURL;
	}
	
}
