package com.lottery.common.exception;

/**
 * 
 * @描述: 服务层异常
 *
 * @author LiBing
 * @date 2016年10月23日 下午4:17:27
 *
 */
@SuppressWarnings("serial")
public class BIZException extends RuntimeException{

	private String errorCode;
	
	public BIZException(){}
	
	public BIZException(String msg){
		super(msg);
	}
	
	public BIZException(String msg, Throwable t){
		super(msg,t);
	}
	
	public BIZException(String errorCode, String msg){
		super(msg);
		this.errorCode = errorCode;
	}

	public BIZException(String errorCode, String msg, Throwable t){
		super(msg,t);
		this.errorCode = errorCode;
	}
	
	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
}
