package com.lottery.common.exception;

/**
 * 
 * @描述: 数据操作层异常
 *
 * @author LiBing
 * @date 2016年10月23日 下午4:17:27
 *
 */
@SuppressWarnings("serial")
public class DAOException extends RuntimeException{

	private String errorCode;
	
	public DAOException(){}
	
	public DAOException(String msg){
		super(msg);
	}
	
	public DAOException(String msg, Throwable t){
		super(msg,t);
	}
	
	public DAOException(String errorCode, String msg){
		super(msg);
		this.errorCode = errorCode;
	}

	public DAOException(String errorCode, String msg, Throwable t){
		super(msg,t);
		this.errorCode = errorCode;
	}
	
	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
}
