package com.lottery.common.util;

/**
 * 常量类 
 * @author linzhimou
 * @since 2016年10月17日
 */
public final class Constant {

	/**成功或失败状态码*/
	public final static String SECCESS = "00";
	public final static String FAIL = "01";
	
	/**返回结果状态，成功或失败*/
	public final static String STATUS = "status";
	public final static String ERRORMSG = "errorMsg";
	
	/**用户所属的三种角色*/
	public final static String ADMIN = "admin";
	public final static String AGENT = "agent";
	public final static String PLAYER = "player";

	/**新增用户时用到的序列名*/
	public final static String AGENT_SEQ = "account_agent_seq";
	public final static String PLAYER_SEQ = "account_player_seq";
	
	/**session中的账户信息key*/
	public final static String USER = "user";
	public final static String USERCODE = "userCode";
	
	/**service处理成功失败标示 */
	public static final String RESULT_FLAG = "resultFlag";
	public static final String RESULT_MSG = "resultMsg";
	
	/**session中的金额信息key*/
	public final static String TOTAL_MONEY = "totalMoney";
}
