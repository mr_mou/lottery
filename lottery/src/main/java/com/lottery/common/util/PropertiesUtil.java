package com.lottery.common.util;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.springframework.core.io.support.PropertiesLoaderUtils;

public class PropertiesUtil {
	private static Logger log = Logger.getLogger(PropertiesUtil.class);
	
	
	private static Properties getSystemProperties(String resourceName) {
		Properties prop = null;
		try {
			prop = PropertiesLoaderUtils.loadAllProperties(resourceName);
		} catch (Exception e) {
			log.error("getSystemProperties: " + e.getMessage());;
		}
		
		return prop;
	}
	
	public static Properties getDefaulSystemProperties() {
		return getSystemProperties("context-lottery.properties");
	}
}
