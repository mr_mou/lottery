package com.lottery.common.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * 使用舍入模式的格式化操作
 **/
public class DoubleFormat {
	public static void main(String args[]) {
		System.out.println(DoubleFormat.doubleOutPut(12.345, 2));
		System.out.println(DoubleFormat.roundNumber(12.335, 2));
		System.out.println(DoubleFormat.round(DoubleFormat.sub(1.23, 1.31), 2));
	}

	public static String doubleOutPut(double v, Integer num) {
		if (v == Double.valueOf(v).intValue()) {
			return Double.valueOf(v).intValue() + "";
		} else {
			BigDecimal b = new BigDecimal(Double.toString(v));
			return b.setScale(num, BigDecimal.ROUND_HALF_UP).toString();// 四舍五入
		}
	}

	public static String roundNumber(double v, int num) {
		String fmtString = "0000000000000000"; // 16bit c
		fmtString = num > 0 ? "0." + fmtString.substring(0, num) : "0";
		DecimalFormat dFormat = new DecimalFormat(fmtString);
		return dFormat.format(v);
	}

	public static double add(double d1, double d2) { // 进行加法运算
		BigDecimal b1 = new BigDecimal(d1);
		BigDecimal b2 = new BigDecimal(d2);
		return b1.add(b2).doubleValue();
	}

	public static double sub(double d1, double d2) { // 进行减法运算
		BigDecimal b1 = new BigDecimal(d1);
		BigDecimal b2 = new BigDecimal(d2);
		return b1.subtract(b2).doubleValue();
	}

	public static double mul(double d1, double d2) { // 进行乘法运算
		BigDecimal b1 = new BigDecimal(d1);
		BigDecimal b2 = new BigDecimal(d2);
		return b1.multiply(b2).doubleValue();
	}

	public static double div(double d1, double d2, int len) {// 进行除法运算
		BigDecimal b1 = new BigDecimal(d1);
		BigDecimal b2 = new BigDecimal(d2);
		return b1.divide(b2, len, BigDecimal.ROUND_HALF_UP).doubleValue();
	}

	public static double round(double d, int len) { // 进行四舍五入操作
		BigDecimal b1 = new BigDecimal(d);
		BigDecimal b2 = new BigDecimal(1);
		// 任何一个数字除以1都是原数字
		// ROUND_HALF_UP是BigDecimal的一个常量，表示进行四舍五入的操作
		return b1.divide(b2, len, BigDecimal.ROUND_HALF_UP).doubleValue();
	}

}