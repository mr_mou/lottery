package com.lottery.common.util;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * 
 * @描述: SpBringContext工具类，用于从applicationContext中获取bean
 *
 * @author LiBing
 * @date 2016年10月22日 上午11:42:26
 *
 */
public class SpringContextHolder implements ApplicationContextAware{
	
	private static ApplicationContext applicationContext = null;

	@Override
	public void setApplicationContext(ApplicationContext context)
			throws BeansException {
		setAC(context);
		
	}
	
	public static void setAC(ApplicationContext context){
		applicationContext = context;
	}
	
	public static ApplicationContext getApplicationContext(){
		return applicationContext;
	}
	
	public static Object getBean(String name) throws BeansException{
		return applicationContext.getBean(name);
	}
	
	public static <T> T getBean(Class<T> requiredType) throws BeansException{
		return applicationContext.getBean(requiredType);
	}
	
	public static <T> T getBean(String name, Class<T> requiredType) throws BeansException{
		return applicationContext.getBean(name, requiredType);
	}
	
	public static boolean containBean(String name){
		return applicationContext.containsBean(name);
	}
	
	public static boolean isSingleton(String name) throws NoSuchBeanDefinitionException{
		return applicationContext.isSingleton(name);
	}
	
	public static Class<?> getType(String name) throws NoSuchBeanDefinitionException{
		return applicationContext.getType(name);
	}
	
	public static String[] getAliases(String name) throws NoSuchBeanDefinitionException{
		return applicationContext.getAliases(name);
	}

}
