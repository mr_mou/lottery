package com.lottery.common.util;

import java.security.MessageDigest;

/**
 * MD5工具类
 * @author linzhimou
 * @since 2016年10月27日
 */
public class MD5Util {

	public static void main(String[] args) {
		System.out.println(MD5Util.md5("123456"));
	}
	
	/**返回字符串的md5值*/
	public final static String md5(String str) {
		return md5(str, null);
	}
	
	/**
	 * 返回字符串的md5值
	 * @param str 字符串原文
	 * @param charset 字符编码
	 * @return
	 */
	public final static String md5(String str, String charset) {
		if(charset == null)
			charset = "UTF-8";
        try {
            byte[] btInput = str.getBytes(charset);
            MessageDigest mdInst = MessageDigest.getInstance("MD5");
            mdInst.update(btInput);
            byte[] md = mdInst.digest();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < md.length; i++) {
                int val = ((int) md[i]) & 0xff;
                if (val < 16)
                    sb.append("0");
                sb.append(Integer.toHexString(val));

            }
            return sb.toString();
        } catch (Exception e) {
            return null;
        }
    }

}
