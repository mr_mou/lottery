package com.lottery.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.lottery.common.util.Constant;
import com.lottery.web.model.Account;

/**
 * 拦截rest请求并判断是否已登陆
 * @author linzhimou
 */
public class LoginInterceptor implements HandlerInterceptor{

	private static Logger logger = Logger.getLogger(LoginInterceptor.class);
	
	@Override
	public boolean preHandle(HttpServletRequest request, 
			HttpServletResponse response, Object arg2) throws Exception {
		
//		HttpSession session = request.getSession(false);
//		if(session == null){
//			logger.info("-----session is null-----");
//			String contextPath = request.getContextPath();
//			response.sendRedirect(contextPath + "/static/layout/login.jsp");
//			return false;
//		}
//		
//		Account account = (Account) session.getAttribute(Constant.USER);
//		if(account == null){
//			logger.info("-----account is null-----");
//			String contextPath = request.getContextPath();
//			response.sendRedirect(contextPath + "/static/layout/login.jsp");
//			return false;
//		}
		
		return true;
	}
	
	@Override
	public void afterCompletion(HttpServletRequest arg0,
			HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1,
			Object arg2, ModelAndView arg3) throws Exception {
		//可在此往model中添加数据
	}

	

}
