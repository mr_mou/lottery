package com.lottery.web.model;

/**
 * 代理和玩家的对应关系
 * @author linzhimou
 * @since 2016年10月24日
 */
public class AgentPlayer {
	
    private String agent;

    private String player;

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent == null ? null : agent.trim();
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player == null ? null : player.trim();
    }
}