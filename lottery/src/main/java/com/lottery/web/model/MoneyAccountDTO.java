package com.lottery.web.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class MoneyAccountDTO {
	private Integer moneyAccountId;
	private String userCode;
	private String optType;
	private double optMoney;
	private double totalMoney;
	private String remark;
	@DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	private Date createDate;
	@DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	private Date updateDate;
	public Integer getMoneyAccountId() {
		return moneyAccountId;
	}
	public void setMoneyAccountId(Integer moneyAccountId) {
		this.moneyAccountId = moneyAccountId;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public String getOptType() {
		return optType;
	}
	public void setOptType(String optType) {
		this.optType = optType;
	}
	public double getOptMoney() {
		return optMoney;
	}
	public void setOptMoney(double optMoney) {
		this.optMoney = optMoney;
	}
	public double getTotalMoney() {
		return totalMoney;
	}
	public void setTotalMoney(double totalMoney) {
		this.totalMoney = totalMoney;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
}
