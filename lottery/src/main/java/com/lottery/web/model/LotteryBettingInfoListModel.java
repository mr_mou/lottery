package com.lottery.web.model;

import java.util.List;

public class LotteryBettingInfoListModel {
	private List<LotteryBettingInfo> lotteryList;

	public List<LotteryBettingInfo> getLotteryList() {
		return lotteryList;
	}

	public void setLotteryList(List<LotteryBettingInfo> lotteryList) {
		this.lotteryList = lotteryList;
	}
	
}
