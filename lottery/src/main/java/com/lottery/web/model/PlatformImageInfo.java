package com.lottery.web.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class PlatformImageInfo {
	private String createdUser;//创建人
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")  
    private Date createdDate;//创建时间
    private String updatedUser;//修改人
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")  
    private Date updatedDate;//修改时间
    private String platformImageId;//int 图片id
    private String platformImageName;//图片名称
    private String platformImageData;//图片base64流
    private String platformImageStatusCode;//int图片状态，1:启用 2:禁用
    private String platformImageType;// int 图片类型，1:收款账号 9:其他
    
	public String getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedUser() {
		return updatedUser;
	}
	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getPlatformImageId() {
		return platformImageId;
	}
	public void setPlatformImageId(String platformImageId) {
		this.platformImageId = platformImageId;
	}
	public String getPlatformImageName() {
		return platformImageName;
	}
	public void setPlatformImageName(String platformImageName) {
		this.platformImageName = platformImageName;
	}
	public String getPlatformImageData() {
		return platformImageData;
	}
	public void setPlatformImageData(String platformImageData) {
		this.platformImageData = platformImageData;
	}
	public String getPlatformImageStatusCode() {
		return platformImageStatusCode;
	}
	public void setPlatformImageStatusCode(String platformImageStatusCode) {
		this.platformImageStatusCode = platformImageStatusCode;
	}
	public String getPlatformImageType() {
		return platformImageType;
	}
	public void setPlatformImageType(String platformImageType) {
		this.platformImageType = platformImageType;
	}
    
}
