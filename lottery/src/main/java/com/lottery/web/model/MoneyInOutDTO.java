package com.lottery.web.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class MoneyInOutDTO {
	private Integer MoneyInOutId;
	private String userCode;
	private String inBankCode;
	private String inBankType;
	private double amount;
	private String optType;
	private String remark;
	private String state;
	private String password;
	@DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	private Date createDate;
	@DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	private Date updateDate;
	
	public Integer getMoneyInOutId() {
		return MoneyInOutId;
	}
	public void setMoneyInOutId(Integer moneyInOutId) {
		MoneyInOutId = moneyInOutId;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public String getInBankCode() {
		return inBankCode;
	}
	public void setInBankCode(String inBankCode) {
		this.inBankCode = inBankCode;
	}
	public String getInBankType() {
		return inBankType;
	}
	public void setInBankType(String inBankType) {
		this.inBankType = inBankType;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getOptType() {
		return optType;
	}
	public void setOptType(String optType) {
		this.optType = optType;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
}
