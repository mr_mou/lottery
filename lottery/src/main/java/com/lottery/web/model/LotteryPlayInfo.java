package com.lottery.web.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class LotteryPlayInfo {
	
	private String createdUser;//创建人
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")  
    private Date createdDate;//创建时间
    private String updatedUser;//修改人
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")  
    private Date updatedDate;//修改时间
    private String lotteryPlayId;//玩法id
    private String lotteryPlayName;//玩法名称
    private String lotteryPlayBasePrize;//玩法奖金基础值
	private String lotteryTypeId;//彩种id
    private String statusCode;//1:启用，0:禁用
    
	public String getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedUser() {
		return updatedUser;
	}
	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getLotteryPlayId() {
		return lotteryPlayId;
	}
	public void setLotteryPlayId(String lotteryPlayId) {
		this.lotteryPlayId = lotteryPlayId;
	}
	public String getLotteryPlayName() {
		return lotteryPlayName;
	}
	public void setLotteryPlayName(String lotteryPlayName) {
		this.lotteryPlayName = lotteryPlayName;
	}
	public String getLotteryPlayBasePrize() {
		return lotteryPlayBasePrize;
	}
	public void setLotteryPlayBasePrize(String lotteryPlayBasePrize) {
		this.lotteryPlayBasePrize = lotteryPlayBasePrize;
	}
	public String getLotteryTypeId() {
		return lotteryTypeId;
	}
	public void setLotteryTypeId(String lotteryTypeId) {
		this.lotteryTypeId = lotteryTypeId;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	@Override
	public String toString() {
		return "LotteryPlayInfo [createdUser=" + createdUser + ", createdDate="
				+ createdDate + ", updatedUser=" + updatedUser
				+ ", updatedDate=" + updatedDate + ", lotteryPlayId="
				+ lotteryPlayId + ", lotteryPlayName=" + lotteryPlayName
				+ ", lotteryPlayBasePrize=" + lotteryPlayBasePrize
				+ ", lotteryTypeId=" + lotteryTypeId + ", statusCode="
				+ statusCode + "]";
	}
    
	
}
