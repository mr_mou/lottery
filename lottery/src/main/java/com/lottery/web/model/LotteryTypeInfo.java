package com.lottery.web.model;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

public class LotteryTypeInfo {
	
	private String createdUser;//创建人
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")  
    private Date createdDate;//创建时间
    private String updatedUser;//修改人
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")  
    private Date updatedDate;//修改时间
	private String lotteryTypeId;//彩种id
    private String lotteryTypeName;//彩种名称
    private String lotteryTypeImgUrl;//彩种走势图url
    private String lotteryTypeBaseAmount;//彩种单价基础值
    private List<LotteryPlayInfo> lotteryPlayList;//玩法list 一对多关系

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getLotteryTypeId() {
		return lotteryTypeId;
	}

	public void setLotteryTypeId(String lotteryTypeId) {
		this.lotteryTypeId = lotteryTypeId;
	}

	public String getLotteryTypeName() {
		return lotteryTypeName;
	}

	public void setLotteryTypeName(String lotteryTypeName) {
		this.lotteryTypeName = lotteryTypeName;
	}

	public String getLotteryTypeImgUrl() {
		return lotteryTypeImgUrl;
	}

	public void setLotteryTypeImgUrl(String lotteryTypeImgUrl) {
		this.lotteryTypeImgUrl = lotteryTypeImgUrl;
	}

	public String getLotteryTypeBaseAmount() {
		return lotteryTypeBaseAmount;
	}

	public void setLotteryTypeBaseAmount(String lotteryTypeBaseAmount) {
		this.lotteryTypeBaseAmount = lotteryTypeBaseAmount;
	}

	public List<LotteryPlayInfo> getLotteryPlayList() {
		return lotteryPlayList;
	}

	public void setLotteryPlayList(List<LotteryPlayInfo> lotteryPlayList) {
		this.lotteryPlayList = lotteryPlayList;
	}

	@Override
	public String toString() {
		return "LotteryTypeInfo [createdUser=" + createdUser + ", createdDate="
				+ createdDate + ", updatedUser=" + updatedUser
				+ ", updatedDate=" + updatedDate + ", lotteryTypeId="
				+ lotteryTypeId + ", lotteryTypeName=" + lotteryTypeName
				+ ", lotteryTypeImgUrl=" + lotteryTypeImgUrl
				+ ", lotteryTypeBaseAmount=" + lotteryTypeBaseAmount
				+ ", lotteryPlayList=" + lotteryPlayList + "]";
	}
    
	
    
}
