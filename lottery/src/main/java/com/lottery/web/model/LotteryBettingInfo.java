package com.lottery.web.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @author lx
 *
 */
public class LotteryBettingInfo {

	private Integer lotteryId; // 主键id
	private String createdUser; // 创建人
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")  
	private Date createdDate;// 创建时间
	private String updatedUser;// 修改人
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")  
	private Date updatedDate;// 修改时间
	private String lotteryUserCode;// 投注玩家id
	private String userName;// 投注玩家名字
	private String role;//角色
	private String lotteryPeriods;// 彩票开奖期号
	private String lotteryType;// 彩票种类
	private String lotteryPlay;// 彩票玩法
	private String lotteryBettingCode;// 投注号码
	private Integer lotteryBettingNumber;// 投注数量
	private Double lotteryBettingAmount;// 投注金额
	private String lotteryBettingMode;// 投注模式（1、元 2、角 3、分)
	private Double lotteryBettingPrize;// 投注奖金
	private Integer lotteryBettingMultiple;// 投注倍数
	private String lotteryBettingStatus;// 状态:1、未开奖 2、开奖中 3、已中奖 4、未中奖 5、已撤单
	private String lotteryBettingCodeList;//投注号码组集
	
	public String getLotteryBettingCodeList() {
		return lotteryBettingCodeList;
	}
	public void setLotteryBettingCodeList(String lotteryBettingCodeList) {
		this.lotteryBettingCodeList = lotteryBettingCodeList;
	}
	public Integer getLotteryId() {
		return lotteryId;
	}
	public void setLotteryId(Integer lotteryId) {
		this.lotteryId = lotteryId;
	}
	public String getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedUser() {
		return updatedUser;
	}
	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getLotteryUserCode() {
		return lotteryUserCode;
	}
	public void setLotteryUserCode(String lotteryUserCode) {
		this.lotteryUserCode = lotteryUserCode;
	}
	public String getLotteryPeriods() {
		return lotteryPeriods;
	}
	public void setLotteryPeriods(String lotteryPeriods) {
		this.lotteryPeriods = lotteryPeriods;
	}
	public String getLotteryType() {
		return lotteryType;
	}
	public void setLotteryType(String lotteryType) {
		this.lotteryType = lotteryType;
	}
	public String getLotteryPlay() {
		return lotteryPlay;
	}
	public void setLotteryPlay(String lotteryPlay) {
		this.lotteryPlay = lotteryPlay;
	}
	public String getLotteryBettingCode() {
		return lotteryBettingCode;
	}
	public void setLotteryBettingCode(String lotteryBettingCode) {
		this.lotteryBettingCode = lotteryBettingCode;
	}
	public Integer getLotteryBettingNumber() {
		return lotteryBettingNumber;
	}
	public void setLotteryBettingNumber(Integer lotteryBettingNumber) {
		this.lotteryBettingNumber = lotteryBettingNumber;
	}
	public Double getLotteryBettingAmount() {
		return lotteryBettingAmount;
	}
	public void setLotteryBettingAmount(Double lotteryBettingAmount) {
		this.lotteryBettingAmount = lotteryBettingAmount;
	}
	public String getLotteryBettingMode() {
		return lotteryBettingMode;
	}
	public void setLotteryBettingMode(String lotteryBettingMode) {
		this.lotteryBettingMode = lotteryBettingMode;
	}
	public Double getLotteryBettingPrize() {
		return lotteryBettingPrize;
	}
	public void setLotteryBettingPrize(Double lotteryBettingPrize) {
		this.lotteryBettingPrize = lotteryBettingPrize;
	}
	public Integer getLotteryBettingMultiple() {
		return lotteryBettingMultiple;
	}
	public void setLotteryBettingMultiple(Integer lotteryBettingMultiple) {
		this.lotteryBettingMultiple = lotteryBettingMultiple;
	}
	public String getLotteryBettingStatus() {
		return lotteryBettingStatus;
	}
	public void setLotteryBettingStatus(String lotteryBettingStatus) {
		this.lotteryBettingStatus = lotteryBettingStatus;
	}
	@Override
	public String toString() {
		return "LotteryBettingInfo [lotteryId=" + lotteryId + ", createdUser="
				+ createdUser + ", createdDate=" + createdDate
				+ ", updatedUser=" + updatedUser + ", updatedDate="
				+ updatedDate + ", lotteryUserCode=" + lotteryUserCode
				+ ", lotteryPeriods=" + lotteryPeriods + ", lotteryType="
				+ lotteryType + ", lotteryPlay=" + lotteryPlay
				+ ", lotteryBettingCode=" + lotteryBettingCode
				+ ", lotteryBettingNumber=" + lotteryBettingNumber
				+ ", lotteryBettingAmount=" + lotteryBettingAmount
				+ ", lotteryBettingMode=" + lotteryBettingMode
				+ ", lotteryBettingPrize=" + lotteryBettingPrize
				+ ", lotteryBettingMultiple=" + lotteryBettingMultiple
				+ ", lotteryBettingStatus=" + lotteryBettingStatus + "]";
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	

	
}
