package com.lottery.web.dao;

import org.springframework.stereotype.Component;

/**
 * commonDao
 * @author linzhimou
 */
@Component("commonDao")
public interface ICommonDao {

    /**
     * 根据序列名查询下一个序列值
     * @param seqName 序列名
     * @return
     */
    Integer getSeqValue(String seqName);
}