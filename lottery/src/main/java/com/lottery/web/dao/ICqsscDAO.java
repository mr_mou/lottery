package com.lottery.web.dao;

import org.springframework.stereotype.Component;

import com.lottery.common.dto.CqsscDTO;
import com.lottery.common.exception.DAOException;

/**
 * 
 * @描述: 重庆时时彩数据库操作接口
 *
 * @author LiBing
 * @date 2016年10月23日 下午4:21:44
 *
 */
@Component("cqsscDAO")
public interface ICqsscDAO {
	
	/**
	 * 
	 * @描述: 插入重庆时时彩开奖结果
	 * 
	 * @param cqsscDto
	 * @throws DAOException
	 */
	public void createCqssc(CqsscDTO cqsscDto) throws DAOException;
	
	/**
	 * 
	 * @描述: 更新重庆时时彩开奖结果
	 * 
	 * @param cqsscDto
	 * @throws DAOException
	 */
	public void updateCqssc(CqsscDTO cqsscDto) throws DAOException;
	
	/**
	 * 
	 * @描述: 删除重庆时时彩开奖结果
	 * 
	 * @param cqsscDto
	 * @throws DAOException
	 */
	public void deleteCqssc(CqsscDTO cqsscDto) throws DAOException;
	
	/**
	 * 
	 * @描述: 查询重庆时时彩开奖结果
	 * 
	 * @param cqsscDto
	 * @throws DAOException
	 */
	public CqsscDTO queryCqssc(CqsscDTO cqsscDto) throws DAOException;
	
	 /** 
	 * @描述: 查询重庆时时彩最新的开奖结果
	 * 
	 * @param cqsscDto
	 * @throws DAOException
	 */
	public CqsscDTO queryLastestCqssc() throws DAOException;

}
