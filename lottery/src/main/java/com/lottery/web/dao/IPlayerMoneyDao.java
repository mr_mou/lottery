package com.lottery.web.dao;

import org.springframework.stereotype.Component;

import com.lottery.web.model.PlayerMoney;

/**
 * 用户的资金管理
 * @author linzhimou
 * @since 2016年10月27日
 */
@Component
public interface IPlayerMoneyDao {

	/**
	 * 新增玩家可用金额，新增玩家时初始化
	 * @param record
	 * @return
	 */
    int addPlayerMoney(PlayerMoney record);

    /**
     * 获取玩家的当前可用资金值
     * @param userCode
     * @return
     */
    Double getPlayerAmount(String userCode);

    /**
     * 更新玩家可用资金
     * @param record
     * @return
     */
    int updatePlayerAmount(PlayerMoney record);
    
}