package com.lottery.web.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.lottery.web.model.Account;

/**
 * 帐户管理dao
 * @author linzhimou
 */
@Component("accountDao")
public interface IAccountDao {

    void saveUser(Account account);

    Account getUser(Account account);
    
    void updateUser(Account account);
    
    /**查询帐户序列，获取自增的id*/
    Integer getSeqValue(String seqName);
    
    /**检查用户名*/
    Integer checkUserName(String userName);
    
    Map<String,Object> getUserLotteryForms(Map<String,Object> params);
    
    List<Map<String, Object>> queryUsers(Map<String, Object> param);

}