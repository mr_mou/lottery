package com.lottery.web.dao;

import java.util.List;
import java.util.Map;

import com.lottery.web.model.LotteryBettingInfo;

public interface ILotteryBettingDao {
	
	public void saveLotteryBettingRecord(List<LotteryBettingInfo> bettinInfoList);
	
	public void updateLotteryBettingRecord(LotteryBettingInfo bettingInfo);
	
	public List<LotteryBettingInfo> queryBettingInfo(LotteryBettingInfo bettingInfo);
	
	public List<LotteryBettingInfo> queryLastestBetteryInfo(Map<String, Object> param);
}
