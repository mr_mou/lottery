package com.lottery.web.dao;

import java.util.List;
import java.util.Map;

import com.lottery.web.model.MessageDTO;

public interface IMessageDao {
	public void saveMessage(MessageDTO messageDTO);
	
	public void deleteMessage(MessageDTO messageDTO);
	
	public List<MessageDTO> queryMessages(Map<String, Object> queryParam);
	
	public void updateMessage(MessageDTO messageDTO);
}
