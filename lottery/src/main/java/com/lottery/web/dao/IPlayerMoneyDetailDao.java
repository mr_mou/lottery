package com.lottery.web.dao;

import org.springframework.stereotype.Component;

import com.lottery.web.model.PlayerMoneyDetail;

/**
 * 玩家的充值、奖金、提现等资金流水纪录
 * @author linzhimou
 * @since 2016年10月27日
 */
@Component
public interface IPlayerMoneyDetailDao {

    int insert(PlayerMoneyDetail record);

}