package com.lottery.web.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.lottery.web.model.LotteryTypeInfo;
import com.lottery.web.model.PlatformImageInfo;

@Component("adminDao")
public interface IAdminDao {
	
	int addLotteryType(Map<String, Object> lotteryType);
	
	void addLotteryPlay(Map<String, Object> lotteryPlay);
	
	void updateLotteryType(Map<String, Object> lotteryType);
	
	void updateLotteryPlay(List<Map<String, Object>> lotteryPlay);
	
	List<LotteryTypeInfo> queryLotteryTypePlay();
	
	List<PlatformImageInfo> queryImageList();
	
	void updateImage(PlatformImageInfo imageInfo);
	
	void saveImage(PlatformImageInfo imageInfo);
	
	void mergeImage(PlatformImageInfo imageInfo);
	
	void updatePlatformBaseInfo(Map<String, Object> baseInfo);

	Map<String, Object> queryPlatformBaseInfo();

}
