package com.lottery.web.dao;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.lottery.web.model.BankInfoDTO;

@Component("iBankInfoDao")
public interface IBankInfoDao {

	public void saveBankInfo(BankInfoDTO bankInfoDTO);
	
	public void modifyWithdrawPassword(Map<String, Object> param);
	
	public void updateBankInfo(BankInfoDTO bankInfoDTO);
	
	public Map<String, Object> queryBankInfo(Map<String, Object> param);
}
