package com.lottery.web.dao;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.lottery.web.model.AgentPlayer;

/**
 * 操作代理和玩家关系表
 * @author linzhimou
 * @since 2016年10月24日
 */
@Component("agentPlayerDao")
public interface IAgentPlayerDao {

    int addAgentPlayer(AgentPlayer record);
    
    Map<String, Object> queryAgentPlayer(Map<String, Object> queryParam);

}