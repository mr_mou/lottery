package com.lottery.web.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.lottery.web.model.MoneyAccountDTO;
import com.lottery.web.model.MoneyInOutDTO;

@Component
public interface IMoneyInOutDao {
	public void saveMoneyInOutInfo(MoneyInOutDTO moneyInOutDTO);
	
	public List<MoneyInOutDTO> queryMoneyInOutList(Map<String, Object> queryParam);
	
	public List<MoneyAccountDTO> queryMoneyAccountList(Map<String, Object> queryParam);
}
