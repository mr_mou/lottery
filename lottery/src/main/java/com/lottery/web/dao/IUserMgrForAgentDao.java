package com.lottery.web.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.lottery.web.model.Account;

@Component("IUserMgrForAgentDao")
public interface IUserMgrForAgentDao {

	public List<Account> queryAllSubUsersAndAgents(Map<String, Object> param);
}
