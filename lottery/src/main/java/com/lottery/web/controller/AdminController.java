package com.lottery.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lottery.common.dto.CommonListMapDTO;
import com.lottery.common.util.Constant;
import com.lottery.web.model.LotteryTypeInfo;
import com.lottery.web.model.PlatformImageInfo;
import com.lottery.web.service.AdminService;

/**
 * 后续增加拦截器做admin接口访问限制
 * @author apple
 *
 */
@Controller
@RequestMapping("/admin")
public class AdminController extends BaseController{
	
	private static Logger logger = Logger.getLogger(LotteryBettingController.class);
	
	@Resource
	private AdminService adminService;
	
	public Map<String,Object> addLotteryTypePlay(Map<String,Object> params){
		
		return null;
	}
	
	@RequestMapping("/updateLotteryTypePlay")
	@ResponseBody
	public Map<String,Object> updateLotteryTypePlay(@RequestBody CommonListMapDTO commonListMapDTO){
		Map<String,Object> resultMap = new HashMap<String,Object>();
		if(logger.isInfoEnabled())
			logger.info(this.getClass().getName() + ".updateLotteryTypePlay start with :" + commonListMapDTO);
		try {
			adminService.updateLotteryTypePlay(commonListMapDTO);
			resultMap.put(Constant.STATUS, Constant.SECCESS);
			resultMap.put(Constant.ERRORMSG, "success");
		} catch (Exception e) {
			logger.error(e);
			resultMap.put(Constant.STATUS, Constant.FAIL);
			resultMap.put(Constant.ERRORMSG, "system error");
		}
		return resultMap;
	}
	
	@RequestMapping("/queryLotteryTypePlay")
	@ResponseBody
	public Map<String,Object> queryLotteryTypePlay(){
		Map<String,Object> resultMap = new HashMap<String,Object>();
		try{
			List<LotteryTypeInfo> dataList = adminService.queryLotteryTypePlay();
			resultMap.put("dataList", dataList);
			resultMap.put(Constant.STATUS, Constant.SECCESS);
			resultMap.put(Constant.ERRORMSG, "success");
		}catch(Exception e){
			logger.error(e);
			resultMap.put(Constant.STATUS, Constant.FAIL);
			resultMap.put(Constant.ERRORMSG, "system error");
		}
		return resultMap;
	}
	
	@RequestMapping("/saveImage")
	@ResponseBody
	public Map<String,Object> saveImage(PlatformImageInfo imageInfo){
		Map<String,Object> resultMap = new HashMap<String,Object>();
		try{
			adminService.saveImage(imageInfo);
			resultMap.put(Constant.STATUS, Constant.SECCESS);
			resultMap.put(Constant.ERRORMSG, "success");
		}catch(Exception e){
			resultMap.put(Constant.STATUS, Constant.FAIL);
			resultMap.put(Constant.ERRORMSG, "system error");
		}
		return resultMap;
	}
	
	@RequestMapping("/updateImage")
	@ResponseBody
	public Map<String,Object> updateImage(PlatformImageInfo imageInfo){
		Map<String,Object> resultMap = new HashMap<String,Object>();
		try{
			adminService.updateImage(imageInfo);
			resultMap.put(Constant.STATUS, Constant.SECCESS);
			resultMap.put(Constant.ERRORMSG, "success");
		}catch(Exception e){
			resultMap.put(Constant.STATUS, Constant.FAIL);
			resultMap.put(Constant.ERRORMSG, "system error");
		}
		return resultMap;
	}
	
	@RequestMapping("/mergeImage")
	@ResponseBody
	public Map<String,Object> mergeImage(PlatformImageInfo imageInfo){
		Map<String,Object> resultMap = new HashMap<String,Object>();
		try{
			adminService.mergeImage(imageInfo);
			resultMap.put(Constant.STATUS, Constant.SECCESS);
			resultMap.put(Constant.ERRORMSG, "success");
		}catch(Exception e){
			resultMap.put(Constant.STATUS, Constant.FAIL);
			resultMap.put(Constant.ERRORMSG, "system error");
		}
		return resultMap;
	}
	
	@RequestMapping("/queryImageList")
	@ResponseBody
	public Map<String,Object> queryImageList(){
		Map<String,Object> resultMap = new HashMap<String,Object>();
		try{
			List<PlatformImageInfo> dataList = adminService.queryImageList();
			resultMap.put("dataList", dataList);
			resultMap.put(Constant.STATUS, Constant.SECCESS);
			resultMap.put(Constant.ERRORMSG, "success");
		}catch(Exception e){
			resultMap.put(Constant.STATUS, Constant.FAIL);
			resultMap.put(Constant.ERRORMSG, "system error");
		}
		return resultMap;
	}
	
	@RequestMapping("/setBaseInfo")
	@ResponseBody
	public Map<String,Object> setBaseInfo(@RequestParam Map<String,Object> params){
		Map<String,Object> resultMap = new HashMap<String,Object>();
		try{
			adminService.updatePlatformBaseInfo(params);
			resultMap.put(Constant.STATUS, Constant.SECCESS);
			resultMap.put(Constant.ERRORMSG, "success");
		}catch(Exception e){
			resultMap.put(Constant.STATUS, Constant.FAIL);
			resultMap.put(Constant.ERRORMSG, "system error");
		}
		return resultMap;
	}
	
	@RequestMapping("/queryPlatformBaseInfo")
	@ResponseBody
	public Map<String, Object> queryPlatformBaseInfo(){
		Map<String,Object> resultMap = new HashMap<String,Object>();
		try{
			Map<String,Object> platformBaseInfo = adminService.queryPlatformBaseInfo();
			resultMap.put("data", platformBaseInfo);
			resultMap.put(Constant.STATUS, Constant.SECCESS);
			resultMap.put(Constant.ERRORMSG, "success");
		}catch(Exception e){
			resultMap.put(Constant.STATUS, Constant.FAIL);
			resultMap.put(Constant.ERRORMSG, "system error");
		}
		return resultMap;
	}
}
