package com.lottery.web.controller;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.lottery.common.util.SpringContextHolder;
import com.lottery.common.util.TimerThread;

public class ThreadStartListener implements ServletContextListener {  
	
    private TimerThread timerThread;  
  
    public void contextDestroyed(ServletContextEvent e) {  
        if (timerThread != null && timerThread.isInterrupted()) {  
            timerThread.interrupt();  
        }  
    }  
  
    public void contextInitialized(ServletContextEvent e) {
        if (timerThread == null) {  
//        	System.out.println("start timer thread...");
//            timerThread = SpringContextHolder.getBean(TimerThread.class);
//            timerThread.start();
        }  
    }  
}  