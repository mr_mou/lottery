package com.lottery.web.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lottery.common.util.Constant;
import com.lottery.web.model.Account;
import com.lottery.web.model.MessageDTO;
import com.lottery.web.service.MessageService;
import com.lottery.web.service.UserMgrService;

@Controller
@RequestMapping("/message")
public class MessageController extends BaseController {
	private static Logger logger = Logger.getLogger(MessageController.class);
	
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private UserMgrService userMgrService;
	
	
	@RequestMapping("/saveMessage")
	@ResponseBody
	public String saveMessage(MessageDTO message, HttpSession session) {
		logger.info("begin save message, param: " + message);
		try {
			Account userInfo = (Account) session.getAttribute(Constant.USER);
			String receiveUserCode = message.getReceiveUserCode();
			String sendUserCode = userInfo.getUserCode();
			Map<String, Object> queryParam = new HashMap<String, Object>();
			if ("player".equals(userInfo.getRole())) {
				queryParam.put("agent", receiveUserCode);
				queryParam.put("player", sendUserCode);
			} else {
				queryParam.put("agent", sendUserCode);
				queryParam.put("player", receiveUserCode);
			}
			if (null == this.userMgrService.queryAgentPlayer(queryParam)) {
				return super.jsonResult("没有权限给此用户发送私信");
			}
			message.setSendUserCode(sendUserCode);
			message.setMessageState("01");
			this.messageService.saveMessage(message);
			return super.jsonResult(null);
		} catch (Exception e) {
			logger.error("save message exception: " + e.getMessage());
			return super.errorResult("系统异常");
		}
	}
	
	@RequestMapping("/queryMessages")
	@ResponseBody
	public String queryMessages(@RequestParam Map<String, Object> param, HttpSession session) {
		logger.info("begin query messages...");
		try {
			Account userInfo = (Account) session.getAttribute(Constant.USER);
			Map<String, Object> queryParam = new HashMap<String, Object>();
			String flag = (String) param.get("flag");
			if ("00".equals(flag)) {
				queryParam.put("sendUserCode", userInfo.getUserCode());
			} else if ("01".equals(flag)) {
				queryParam.put("receiveUserCode", userInfo.getUserCode());
			} else {
				return super.errorResult("参数出错");
			}
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date beginDate = sdf.parse((String) param.get("beginDate"));
			Date endDate = sdf.parse((String) param.get("endDate"));
			if (beginDate.getTime() > endDate.getTime()) {
				return super.errorResult("起始时间大于结束时间");
			}
			queryParam.put("messageState", param.get("messageState"));
			queryParam.put("beginDate", param.get("beginDate"));
			queryParam.put("endDate", param.get("endDate"));
			List<MessageDTO> messageList = this.messageService.queryMessages(queryParam);
			logger.info("query messges result: " + messageList);
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("messageList", messageList);
			return super.jsonResult(resultMap);
		} catch (Exception e) {
			logger.error("query messages exception: " + e.getMessage());
			return super.errorResult("系统异常");
		}
	}
	
	@RequestMapping("/deleteMessage")
	@ResponseBody
	public String deleteMessage(MessageDTO message, HttpSession session) {
		logger.info("begin delete message...");
		try {
			Account userInfo = (Account) session.getAttribute(Constant.USER);
			message.setSendUserCode(userInfo.getUserCode());
			this.messageService.deleteMessage(message);
			return super.jsonResult(null);
		} catch (Exception e) {
			logger.equals("delete message exception: " + e.getMessage());
			return super.errorResult("系统异常");
		}
	}
	
	@RequestMapping("/updateMessage")
	@ResponseBody
	public String updateMessage(MessageDTO message, HttpSession session) {
		logger.info("begin update message... param: " + message);
		try {
			Account userInfo = (Account) session.getAttribute(Constant.USER);
			message.setReceiveUserCode(userInfo.getUserCode());
			this.messageService.updateMessage(message);
			return super.jsonResult(null);
		} catch (Exception e) {
			logger.error("update message exception: " + e.getMessage());
			return super.errorResult("系统异常");
		}
	}
	
	@RequestMapping("/querySystemTips")
	@ResponseBody
	public String querySystemTips(HttpSession session) {
		logger.info("begin query system tips...: ");
		try {
			Account userInfo = (Account) session.getAttribute(Constant.USER);
			if (null == userInfo) {
				return super.errorResult("会话超时");
			}
			Map<String, Object> queryParam = new HashMap<String, Object>();
			queryParam.put("sendUserCode", "admin1");
			queryParam.put("receiveUserCode", "system");
			List<MessageDTO> systemTips =  this.messageService.queryMessages(queryParam);
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("systemTipList", systemTips);
			return super.jsonResult(result);
		} catch (Exception e) {
			logger.error("update message exception: " + e.getMessage());
			return super.errorResult("系统异常");
		}
	}
	
	
}
