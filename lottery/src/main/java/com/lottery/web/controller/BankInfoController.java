package com.lottery.web.controller;


import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lottery.common.util.Constant;
import com.lottery.web.model.Account;
import com.lottery.web.model.BankInfoDTO;
import com.lottery.web.service.BankInfoService;

@Controller
@RequestMapping("/bank")
public class BankInfoController extends BaseController {
	private static Logger logger = Logger.getLogger(BankInfoController.class);
	
	@Resource
	private BankInfoService bankInfoService;
	
	
	@RequestMapping("/saveBankInfo")
	@ResponseBody
	public String saveBankInfo(BankInfoDTO bankInfo, HttpSession session) {
		logger.info("begin save bank information...");
		try {
			String errMsg = this.checkBankInfo(bankInfo);
			if (null != errMsg) {
				return super.errorResult(errMsg);
			}
			Account userInfo = (Account) session.getAttribute(Constant.USER);
			bankInfo.setUserCode(userInfo.getUserCode());
			if ("2".equals(bankInfo.getOptType())) {
				this.bankInfoService.updateBankInfo(bankInfo);
			} else {
				this.bankInfoService.saveBankInfo(bankInfo);
			}
			return super.jsonResult(null);
		} catch (Exception e) {
			logger.error("save bank information exception: " + e.getMessage());
			return super.errorResult("系统异常");
		}
	}
	
	@RequestMapping("/queryBankInfo")
	@ResponseBody
	public String queryBankInfo(HttpSession session) {
		logger.info("begin query bank information...");
		try {
			Account userInfo = (Account) session.getAttribute(Constant.USER);
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("userCode", userInfo.getUserCode());
			Map<String, Object> result = this.bankInfoService.queryBankInfo(param);
			logger.info("end query bank information, result is: " + result);
			return super.jsonResult(result);
		} catch (Exception e) {
			logger.error("query bank information exception: " + e.getMessage());
			return super.errorResult("系统异常");
		}
	}
	
	@RequestMapping("/modifyWithdrawPassword")
	@ResponseBody
	public String modifyWithdrawPassword(@RequestBody Map<String, Object> reqParam, HttpSession session) {
		logger.info("begin modify widthdraw password... param: " + reqParam);
		try {
			Account userInfo = (Account) session.getAttribute(Constant.USER);
			String oldPassword = (String) reqParam.get("oldPasswrod");
			String newPassword = (String) reqParam.get("newPassword");
			if (StringUtils.isEmpty(oldPassword) || StringUtils.isEmpty(newPassword)) {
				return super.errorResult("密码为空");
			}
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("userCode", userInfo.getUserCode());
			param.put("bankPassword", oldPassword);
			Map<String, Object> queryResult = this.bankInfoService.queryBankInfo(param);
			if (queryResult != null) {
				param.put("bankPassword", newPassword);
				this.bankInfoService.modifyWithdrawPassword(param);
				return super.jsonResult(null);
			} 
			return super.errorResult("旧密码错误");
		} catch (Exception e) {
			logger.error("modify password exception: " + e.getMessage());
			return super.errorResult("系统异常");
		}
	}
	
	private String checkBankInfo(BankInfoDTO bankInfo) {
		String message = null;
		if (null == bankInfo) {
			message = "银行卡信息为空";
		} else if (StringUtils.isEmpty(bankInfo.getBankCode()) || StringUtils.isEmpty(bankInfo.getBankType())) {
			message = "银行类型为空";
		} else if (StringUtils.isEmpty(bankInfo.getBankAccount())) {
			message = "银行账户不能为空";
		} else if (StringUtils.isEmpty(bankInfo.getBankUser())) {
			message = "银行户名不能为空";
		} else if (StringUtils.isEmpty(bankInfo.getBankName())) {
			message = "开户行不能为空";
		}
		return message;
		
		
	}
}
