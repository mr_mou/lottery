package com.lottery.web.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lottery.common.util.Constant;
import com.lottery.web.model.Account;
import com.lottery.web.model.MoneyAccountDTO;
import com.lottery.web.model.MoneyInOutDTO;
import com.lottery.web.service.BankInfoService;
import com.lottery.web.service.MoneyInOutService;

@Controller
@RequestMapping("/money")
public class MoneyInOutController extends BaseController {
	private Logger logger = Logger.getLogger(MoneyInOutController.class);
	
	@Autowired
	private MoneyInOutService moneyInOutService;
	
	@Autowired
	private BankInfoService bankInfoService;
	
	@RequestMapping("/saveMoneyInOutInfo")
	@ResponseBody
	public String saveMoneyInOutInfo(MoneyInOutDTO moneyInOut, HttpSession session) {
		logger.info("begin save money in out information... param: " + moneyInOut);
		
		try {
			String errMsg = this.checkMoneyInOutInfo(moneyInOut);
			if (null != errMsg) {
				return super.errorResult(errMsg);
			}
			String optType = moneyInOut.getOptType();
 			Account userInfo = (Account) session.getAttribute(Constant.USER);
			moneyInOut.setUserCode(userInfo.getUserCode());
			if ("02".equals(optType)) {
				Map<String, Object> param = new HashMap<String, Object>();
				param.put("userCode", userInfo.getUserCode());
				param.put("bankPassword", moneyInOut.getPassword());
				Map<String, Object> resultMap = this.bankInfoService.queryBankInfo(param);
				if (null == resultMap) {
					return super.errorResult("取款密码错误");
				}
			}
			this.moneyInOutService.saveMoneyInOutInfo(moneyInOut);
			return super.jsonResult(null);
		} catch (Exception e) {
			logger.info("save money in out information exception: " + e.getMessage());
			return super.errorResult("系统异常");
		}
	}
	
	@RequestMapping("/queryMoneyInList")
	@ResponseBody
	public String queryMoneyInList(@RequestBody Map<String, Object> param, HttpSession session) {
		logger.info("begin query money in list... param: " + param);
		
		try {
			Account userInfo = (Account) session.getAttribute(Constant.USER);
			Map<String, Object> queryParam = new HashMap<String, Object>();
			queryParam.put("userCode", userInfo.getUserCode());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date beginDate = sdf.parse((String) param.get("beginDate"));
			Date endDate = sdf.parse((String) param.get("endDate"));
			if (beginDate.getTime() > endDate.getTime()) {
				return super.errorResult("起始时间大于结束时间");
			}
			queryParam.put("beginDate", param.get("beginDate"));
			queryParam.put("endDate", param.get("endDate"));
			queryParam.put("optType", "01");
			List<MoneyInOutDTO> moneyInList = this.moneyInOutService.queryMoneyInOutList(queryParam);
			logger.info("end query money in list, result: " + moneyInList);
			Map<String, Object> retMap = new HashMap<String, Object>();
			retMap.put("moneyInList", moneyInList);
			return super.jsonResult(retMap);
		} catch (Exception e) {
			logger.error("query money in list exception: " + e.getMessage());
			return super.errorResult("系统异常");
		}
	}
	
	@RequestMapping("/queryMoneyOutList")
	@ResponseBody
	public String queryMoneyOutList(@RequestBody Map<String, Object> param, HttpSession session) {
		logger.info("begin query money in list... param: " + param);
		
		try {
			Account userInfo = (Account) session.getAttribute(Constant.USER);
			Map<String, Object> queryParam = new HashMap<String, Object>();
			queryParam.put("userCode", userInfo.getUserCode());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date beginDate = sdf.parse((String) param.get("beginDate"));
			Date endDate = sdf.parse((String) param.get("endDate"));
			if (beginDate.getTime() > endDate.getTime()) {
				return super.errorResult("起始时间大于结束时间");
			}
			queryParam.put("beginDate", param.get("beginDate"));
			queryParam.put("endDate", param.get("endDate"));
			queryParam.put("optType", "02");
			List<MoneyInOutDTO> moneyInList = this.moneyInOutService.queryMoneyInOutList(queryParam);
			logger.info("end query money out list, result: " + moneyInList);
			Map<String, Object> retMap = new HashMap<String, Object>();
			retMap.put("moneyInList", moneyInList);
			return super.jsonResult(retMap);
		} catch (Exception e) {
			logger.error("query money out list exception: " + e.getMessage());
			return super.errorResult("系统异常");
		}
	}
	
	@RequestMapping("/queryMoneyAccountList")
	@ResponseBody
	public String queryMoneyAccountList(@RequestBody Map<String, Object> param, HttpSession session) {
		logger.info("begin query money account list... param: " + param);
		try {
			Account userInfo = (Account) session.getAttribute(Constant.USER);
			Map<String, Object> queryParam = new HashMap<String, Object>();
			queryParam.put("userCode", userInfo.getUserCode());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date beginDate = sdf.parse((String) param.get("beginDate"));
			Date endDate = sdf.parse((String) param.get("endDate"));
			if (beginDate.getTime() > endDate.getTime()) {
				return super.errorResult("起始时间大于结束时间");
			}
			queryParam.put("beginDate", param.get("beginDate"));
			queryParam.put("endDate", param.get("endDate"));
			queryParam.put("optType", param.get("optType"));
			List<MoneyAccountDTO> moneyAccountList = this.moneyInOutService.queryMoneyAccountList(queryParam);
			logger.info("end query money account list, result: " + moneyAccountList);
			Map<String, Object> retMap = new HashMap<String, Object>();
			retMap.put("moneyAccountList", moneyAccountList);
			return super.jsonResult(retMap);
		} catch (Exception e) {
			logger.error("query money account exception: " + e.getMessage());
			return super.errorResult("系统异常");
		}
	}
	
	
	private String checkMoneyInOutInfo(MoneyInOutDTO moneyInOut) {
		String message = null;
		if (StringUtils.isEmpty(moneyInOut)) {
			message = "参数为空";
		} else if (StringUtils.isEmpty(moneyInOut.getOptType())) {
			message = "操作类型为空";
		} else if ("01".equals(moneyInOut.getOptType()) && (StringUtils.isEmpty(moneyInOut.getInBankCode()) || StringUtils.isEmpty(moneyInOut.getInBankType()))) {
			message = "充值账号类型为空";
		} else if (StringUtils.isEmpty(moneyInOut.getAmount())) {
			message = "金额为空";
		} else if ("02".equals(moneyInOut.getPassword()) && StringUtils.isEmpty(moneyInOut.getPassword())) {
			message = "取款密码为空";
		}
		return message;
	}

}
