package com.lottery.web.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lottery.common.util.Constant;
import com.lottery.web.model.Account;
import com.lottery.web.model.PlayerMoney;
import com.lottery.web.service.PlayerMoneyService;

@Controller
public class PlayerMoneyController extends BaseController {
	private Logger logger = Logger.getLogger(PlayerMoney.class);
	
	@Resource
	private PlayerMoneyService playerMoneyService;
	
	@RequestMapping(value = "/getTotalMoney", method = RequestMethod.POST)
	@ResponseBody
	public String getTotalMoney(HttpSession session) {
		logger.info("begin get total money method: ");
		Map<String, Object> totalMoneyMap = new HashMap<String, Object>();
		try {
			Account account = (Account) session.getAttribute(Constant.USER);
			if (account != null) {
				double totalMoney = this.playerMoneyService.getPlayerAmount(account.getUserCode());
				totalMoneyMap.put("totalMoney", totalMoney);
			}
		} catch (Exception e) {
			logger.error("getTotalMoney exception: " + e.getMessage());
			return super.errorResult("查询金额出错");
		}
		return super.jsonResult(totalMoneyMap);
	}

}
