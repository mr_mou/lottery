package com.lottery.web.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lottery.common.util.Constant;

/**
 * controller公用父类
 * @author linzhimou
 * @since 2016年10月17日
 */
public class BaseController {

	/**将返回结果转换成json并添加默认成功的状态*/
	protected String jsonResult(Object object){
		return jsonResult(object, Constant.SECCESS, null);
	}
	
	/**返回错误信息*/
	protected String errorResult(String errorMsg){
		return jsonResult(null, Constant.FAIL, errorMsg);
	}
	
	/**将返回结果转换成json并添加状态及提示消息*/
	protected String jsonResult(Object object, 
			String status, String errorMsg){
		JSONObject result = new JSONObject();
		
		if(object != null)
			result = (JSONObject) JSON.toJSON(object);
		if(errorMsg != null)
			result.put(Constant.ERRORMSG, errorMsg);
		
		result.put(Constant.STATUS, status);
		
		return result.toString();
	}
}
