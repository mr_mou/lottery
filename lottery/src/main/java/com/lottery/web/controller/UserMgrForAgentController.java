package com.lottery.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lottery.common.util.Constant;
import com.lottery.web.model.Account;
import com.lottery.web.service.UserMgrForAgentService;

@Controller
@RequestMapping("/userMgrForAgent")
public class UserMgrForAgentController extends BaseController {
	private static Logger logger = Logger.getLogger(UserMgrForAgentController.class);
	
	@Autowired
	private UserMgrForAgentService userMgrForAgentService;
	
	@RequestMapping("/queryAllSubUsersAndAgents")
	@ResponseBody
	public String queryAllSubUsersAndAgents(HttpSession session) {
		logger.info("begin query all sub users and agents...");
		
		try {
			Account userInfo = (Account) session.getAttribute(Constant.USER);
			String userRole = userInfo.getRole();
			if ("agent".equals(userRole)) {
				String userCode = userInfo.getUserCode();
				Map<String, Object> paramMap = new HashMap<String, Object>();
				paramMap.put("userCode", userCode);
				List<Account> allSubUsersAndAgents = this.userMgrForAgentService.queryAllSubUsersAndAgents(paramMap);
				Map<String, Object> resultMap = new HashMap<String, Object>();
				resultMap.put("allSubUsersAndAgents", allSubUsersAndAgents);
				logger.info("query all sub users and agents result: " + resultMap);
				return super.jsonResult(resultMap);
			}
			return super.errorResult("用户权限不足");
		} catch (Exception e) {
			logger.error("query all sub users and agents exception: " + e.getMessage());
			return super.errorResult("系统内部错误");
		}
	}
}
