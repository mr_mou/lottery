package com.lottery.web.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lottery.common.dto.CommonListMapDTO;
import com.lottery.web.dao.IAdminDao;
import com.lottery.web.model.LotteryTypeInfo;
import com.lottery.web.model.PlatformImageInfo;
import com.lottery.web.service.AdminService;

@Service
public class AdminServiceImpl implements AdminService {

	@Resource
	private IAdminDao adminDao;

	@Override
	public void addLotterTypePlay(Map<String, Object> params) {

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateLotteryTypePlay(CommonListMapDTO commonListMapDTO) {
		List<Map<String,Object>> commonList = commonListMapDTO.getCommonList();
		for (Map<String, Object> map : commonList) {
			adminDao.updateLotteryType(map);
			List<Map<String,Object>> lotteryPlayList = (List<Map<String, Object>>) map.get("lotteryPlayList");
			if(lotteryPlayList!=null && !lotteryPlayList.isEmpty())
				adminDao.updateLotteryPlay(lotteryPlayList);
		}
	}

	@Override
	public List<LotteryTypeInfo> queryLotteryTypePlay() {
		return adminDao.queryLotteryTypePlay();
	}

	@Override
	public List<PlatformImageInfo> queryImageList() {
		return adminDao.queryImageList();
	}

	@Override
	public void updateImage(PlatformImageInfo imageInfo) {
		adminDao.updateImage(imageInfo);
	}

	@Override
	public void saveImage(PlatformImageInfo imageInfo) {
		adminDao.saveImage(imageInfo);
	}

	@Override
	public void mergeImage(PlatformImageInfo imageInfo) {
		adminDao.mergeImage(imageInfo);
	}

	@Override
	public void updatePlatformBaseInfo(Map<String, Object> baseInfo) {
		adminDao.updatePlatformBaseInfo(baseInfo);
	}

	@Override
	public Map<String, Object> queryPlatformBaseInfo() {
		return adminDao.queryPlatformBaseInfo();
	}

}
