package com.lottery.web.service.impl;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.lottery.web.dao.IPlayerMoneyDao;
import com.lottery.web.model.PlayerMoney;
import com.lottery.web.service.PlayerMoneyService;

@Service
public class PlayerMoneyServiceImpl implements PlayerMoneyService {
	private Logger logger = Logger.getLogger(PlayerMoneyServiceImpl.class);
	
	@Resource
	private IPlayerMoneyDao playerMoneyDao;
	

	@Override
	public int addPlayerMoney(PlayerMoney record) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Double getPlayerAmount(String userCode) {
		return this.playerMoneyDao.getPlayerAmount(userCode);
	}

	@Override
	public int updatePlayerAmount(PlayerMoney record) {
		// TODO Auto-generated method stub
		return 0;
	}

}
