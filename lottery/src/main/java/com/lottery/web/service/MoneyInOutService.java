package com.lottery.web.service;

import java.util.List;
import java.util.Map;

import com.lottery.web.model.MoneyAccountDTO;
import com.lottery.web.model.MoneyInOutDTO;

public interface MoneyInOutService {
	public void saveMoneyInOutInfo(MoneyInOutDTO moneyInOutDTO);
	
	public List<MoneyInOutDTO> queryMoneyInOutList(Map<String, Object> queryParam);
	
	public List<MoneyAccountDTO> queryMoneyAccountList(Map<String, Object> queryParam);
}
