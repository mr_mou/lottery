package com.lottery.web.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.lottery.web.dao.IUserMgrForAgentDao;
import com.lottery.web.model.Account;
import com.lottery.web.service.UserMgrForAgentService;

@Service
public class UserMgrForAgentServiceImpl implements UserMgrForAgentService {
	
	@Resource
	private IUserMgrForAgentDao userMgrForAgentDao;

	@Override
	public List<Account> queryAllSubUsersAndAgents(Map<String, Object> param) {
		return this.userMgrForAgentDao.queryAllSubUsersAndAgents(param);
	}

}
