package com.lottery.web.service;

import java.util.List;
import java.util.Map;

import com.lottery.web.model.Account;
import com.lottery.web.model.AgentPlayer;

/**
 * 用户管理
 * @author linzhimou
 * @since 2016年10月17日
 */
public interface UserMgrService {
	
	/**
	 * 查询用户信息
	 * @param account
	 * @return Account
	 */
	Account getUser(Account account);
	
	/**
	 * 新增用户
	 * @param account	新用户
	 * @param agentPlayer 代理与玩家关系，可为null
	 */
	void addUser(Account account, AgentPlayer agentPlayer);
	
	/**
	 * 更新用户信息
	 * @param account
	 */
	void updataUser(Account account);
	
	/**检查用户名*/
    Integer checkUserName(String userName);
	
	/**
	 * 获取序列下一个值
	 * @param seqName 序列名
	 * @return
	 */
	Integer getSeqValue(String seqName);
	
	Map<String,Object> getUserLotteryForms(String userCode,String queryType);
	
	Map<String, Object> queryAgentPlayer(Map<String, Object> queryParam);
	
	List<Map<String, Object>> queryUsers(Map<String, Object> queryParam);
	
	
}
