package com.lottery.web.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lottery.common.util.Constant;
import com.lottery.common.util.DoubleFormat;
import com.lottery.web.dao.ILotteryBettingDao;
import com.lottery.web.dao.IPlayerMoneyDao;
import com.lottery.web.model.LotteryBettingInfo;
import com.lottery.web.model.PlayerMoney;
import com.lottery.web.service.LotteryBettingService;

@Service
public class LotteryBettingServiceImpl implements LotteryBettingService {

	private static Logger logger = Logger
			.getLogger(LotteryBettingServiceImpl.class);

	@Resource
	private ILotteryBettingDao bettingDao;

	@Resource
	private IPlayerMoneyDao playerMoneyDao;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Map<String, Object> saveLotteryBettingRecord(
			List<LotteryBettingInfo> bettingInfoList, double totalAmount) throws Exception{
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put(Constant.RESULT_FLAG, true);
		if (logger.isDebugEnabled())
			logger.debug("process saveLotteryBettingRecord start with bettingInfoList:" + bettingInfoList);
		String userCode = bettingInfoList.get(0).getLotteryUserCode();
		Double userAmount = playerMoneyDao.getPlayerAmount(userCode);
		if(userAmount == null){//用户资金不存在
			resultMap.put(Constant.RESULT_FLAG, false);
			resultMap.put(Constant.RESULT_MSG, "用户资金不存在");
			return resultMap;
		}
		
		//计算扣除消费后的余额
		double balance = DoubleFormat.round(DoubleFormat.sub(userAmount, totalAmount), 2);
		if(balance < 0){//余额不足
			resultMap.put(Constant.RESULT_FLAG, false);
			resultMap.put(Constant.RESULT_MSG, "余额不足");
			return resultMap;
		}
			
		try {
			bettingDao.saveLotteryBettingRecord(bettingInfoList);
		} catch (Exception e) {
			logger.error("saveLotteryBettingRecord exception:" + e);
			throw new Exception("save lottery detail error, exception:" + e);
		}
		
		//更新用户余额
		PlayerMoney playerMoney = new PlayerMoney();
		playerMoney.setUserCode(userCode);
		playerMoney.setAmount(balance);
		try {
			playerMoneyDao.updatePlayerAmount(playerMoney);
		} catch (Exception e) {
			logger.error("updatePlayerAmount exception:" + e);
			throw new Exception("updatePlayerAmount error, exception:" + e);
		}
		
		if(logger.isDebugEnabled())
			logger.debug("process saveLotteryBettingRecord end with resultMap:" + resultMap);
		return resultMap;
	}

	@Override
	public Map<String, Object> updateLotteryBettingRecord(LotteryBettingInfo bettingInfo) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		if (logger.isDebugEnabled())
			logger.debug("updateLotteryBettingRecord start with bttingInfo:" + bettingInfo);
		try {
			bettingDao.updateLotteryBettingRecord(bettingInfo);
			resultMap.put(Constant.RESULT_FLAG, true);
		} catch (Exception e) {
			logger.error("updateLotteryBettingRecord exception:" + e);
			resultMap.put(Constant.RESULT_FLAG, false);
		}
		return resultMap;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Map<String, Object> cancelOrder(LotteryBettingInfo bettingInfo) throws Exception{
		Map<String, Object> resultMap = new HashMap<String, Object>();
		boolean updateFlag = true;
		if (logger.isDebugEnabled())
			logger.debug("cancelOrder start with bttingInfo:" + bettingInfo);
		bettingInfo.setLotteryBettingStatus("5");//5:撤单状态
		try {
			bettingDao.updateLotteryBettingRecord(bettingInfo);
		} catch (Exception e) {
			logger.error("updateLotteryBettingRecord exception:" + e);
			throw new Exception("撤单失败，exception:" + e);
		}
		//修改用户资金，撤单成功，钱退回
		if(updateFlag){
			
		}
		
		resultMap.put(Constant.RESULT_FLAG, updateFlag);
		if(logger.isDebugEnabled())
			logger.debug("cancelOrder end with resultMap:" + resultMap);
		return resultMap;
	}

	@Override
	public List<LotteryBettingInfo> queryBettingInfo(LotteryBettingInfo bettingInfo) {
		return bettingDao.queryBettingInfo(bettingInfo);
	}
	
	@Override
	public List<LotteryBettingInfo> queryLastestBetteryInfo(Map<String, Object> param) {
		return this.bettingDao.queryLastestBetteryInfo(param);
	}

}
