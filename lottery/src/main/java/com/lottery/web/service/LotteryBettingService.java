package com.lottery.web.service;

import java.util.List;
import java.util.Map;

import com.lottery.web.model.LotteryBettingInfo;

public interface LotteryBettingService {
	
	public Map<String, Object> saveLotteryBettingRecord(List<LotteryBettingInfo> bettinInfoList,double totalAmount) throws Exception;

	public Map<String, Object> updateLotteryBettingRecord(LotteryBettingInfo bettingInfo);
	
	public Map<String, Object> cancelOrder(LotteryBettingInfo bettingInfo) throws Exception;
	
	public List<LotteryBettingInfo> queryBettingInfo(LotteryBettingInfo bettingInfo);
	
	public List<LotteryBettingInfo> queryLastestBetteryInfo(Map<String, Object> param);
	
}
