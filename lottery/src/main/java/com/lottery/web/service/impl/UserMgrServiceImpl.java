package com.lottery.web.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lottery.web.dao.IAccountDao;
import com.lottery.web.dao.IAgentPlayerDao;
import com.lottery.web.dao.ICommonDao;
import com.lottery.web.dao.IPlayerMoneyDao;
import com.lottery.web.model.Account;
import com.lottery.web.model.AgentPlayer;
import com.lottery.web.model.PlayerMoney;
import com.lottery.web.service.UserMgrService;

@Service
public class UserMgrServiceImpl implements UserMgrService{

	private static Logger logger = Logger.getLogger(UserMgrServiceImpl.class);

	@Autowired
	private IAccountDao accountDao;

	@Autowired
	private IAgentPlayerDao agentPlayerDao;

	@Autowired
	private IPlayerMoneyDao playerMoneyDao;
	
	@Autowired
	private ICommonDao commonDao;
	
	@Override
//	@Cacheable(value="cacheOne",key="#account.getUserName()")
	public Account getUser(Account account) {
		return accountDao.getUser(account);
	}

	@Override
	@Transactional
	public void addUser(Account account, AgentPlayer agentPlayer) {
		//保存用户
		accountDao.saveUser(account);
		
		//如果是玩家
		if(agentPlayer != null){
			//保存代理与玩家关系
			agentPlayerDao.addAgentPlayer(agentPlayer);
			//初始化资金表
			PlayerMoney pm = new PlayerMoney();
			pm.setUserCode(account.getUserCode());
			playerMoneyDao.addPlayerMoney(pm);
		}
	}

	@Override
	public void updataUser(Account account) {
		accountDao.updateUser(account);
	}

	@Override
	public Integer getSeqValue(String seqName) {
		return commonDao.getSeqValue(seqName);
	}

	@Override
	public Integer checkUserName(String userName) {
		return accountDao.checkUserName(userName);
	}

	@Override
	public Map<String, Object> getUserLotteryForms(String userCode,
			String queryType) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("userCode", userCode);
		params.put("type", queryType);
		return accountDao.getUserLotteryForms(params);	
	}

	@Override
	public Map<String, Object> queryAgentPlayer(Map<String, Object> queryParam) {
		return this.agentPlayerDao.queryAgentPlayer(queryParam);
	}

	@Override
	public List<Map<String, Object>> queryUsers(Map<String, Object> queryParam) {
		return this.accountDao.queryUsers(queryParam);
	}


}
