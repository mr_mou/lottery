package com.lottery.web.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lottery.web.dao.IMoneyInOutDao;
import com.lottery.web.model.MoneyAccountDTO;
import com.lottery.web.model.MoneyInOutDTO;
import com.lottery.web.service.MoneyInOutService;

@Service
public class MoneyInOutServiceImpl implements MoneyInOutService {
	@Autowired
	private IMoneyInOutDao moneyInOutDao;
		
	@Override
	public void saveMoneyInOutInfo(MoneyInOutDTO moneyInOutDTO) {
		this.moneyInOutDao.saveMoneyInOutInfo(moneyInOutDTO);
	}

	@Override
	public List<MoneyInOutDTO> queryMoneyInOutList(
			Map<String, Object> queryParam) {
		return this.moneyInOutDao.queryMoneyInOutList(queryParam);
	}

	@Override
	public List<MoneyAccountDTO> queryMoneyAccountList(
			Map<String, Object> queryParam) {
		return this.moneyInOutDao.queryMoneyAccountList(queryParam);
	}

}
