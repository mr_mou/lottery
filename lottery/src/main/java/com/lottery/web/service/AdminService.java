package com.lottery.web.service;

import java.util.List;
import java.util.Map;

import com.lottery.common.dto.CommonListMapDTO;
import com.lottery.web.model.LotteryTypeInfo;
import com.lottery.web.model.PlatformImageInfo;

public interface AdminService {
	void addLotterTypePlay(Map<String, Object> params);

	void updateLotteryTypePlay(CommonListMapDTO commonListMapDTO);
	
	List<LotteryTypeInfo> queryLotteryTypePlay();
	
	List<PlatformImageInfo> queryImageList();
	
	void updateImage(PlatformImageInfo imageInfo);
	
	void saveImage(PlatformImageInfo imageInfo);
	
	void mergeImage(PlatformImageInfo imageInfo);
	
	void updatePlatformBaseInfo(Map<String, Object> baseInfo);

	Map<String, Object> queryPlatformBaseInfo();
}
