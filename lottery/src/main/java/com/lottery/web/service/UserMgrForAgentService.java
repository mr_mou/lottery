package com.lottery.web.service;

import java.util.List;
import java.util.Map;

import com.lottery.web.model.Account;

public interface UserMgrForAgentService {
	public List<Account> queryAllSubUsersAndAgents(Map<String, Object> param);
}
