package com.lottery.web.service;

import com.lottery.web.model.PlayerMoney;


public interface PlayerMoneyService {
	/**
	 * 新增玩家可用金额，新增玩家时初始化
	 * @param record
	 * @return
	 */
    int addPlayerMoney(PlayerMoney record);

    /**
     * 获取玩家的当前可用资金值
     * @param userCode
     * @return
     */
    Double getPlayerAmount(String userCode);

    /**
     * 更新玩家可用资金
     * @param record
     * @return
     */
    int updatePlayerAmount(PlayerMoney record);
}
