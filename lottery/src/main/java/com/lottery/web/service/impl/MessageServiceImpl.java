package com.lottery.web.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lottery.web.dao.IMessageDao;
import com.lottery.web.model.MessageDTO;
import com.lottery.web.service.MessageService;

@Service
public class MessageServiceImpl implements MessageService {
	
	@Autowired
	private IMessageDao messageDao;

	@Override
	public void saveMessage(MessageDTO messageDTO) {
		this.messageDao.saveMessage(messageDTO);
	}

	@Override
	public void deleteMessage(MessageDTO messageDTO) {
		this.messageDao.deleteMessage(messageDTO);
	}

	@Override
	public List<MessageDTO> queryMessages(Map<String, Object> queryParam) {
		return this.messageDao.queryMessages(queryParam);
	}

	@Override
	public void updateMessage(MessageDTO messageDTO) {
		this.messageDao.updateMessage(messageDTO);
	}

}
