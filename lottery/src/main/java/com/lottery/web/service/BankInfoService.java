package com.lottery.web.service;

import java.util.Map;

import com.lottery.web.model.BankInfoDTO;

public interface BankInfoService {

	public void saveBankInfo(BankInfoDTO bankInfoDTO);
	
	public void modifyWithdrawPassword(Map<String, Object> param);
	
	public void updateBankInfo(BankInfoDTO bankInfoDTO);
	
	public Map<String, Object> queryBankInfo(Map<String, Object> param);
}
