package com.lottery.web.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lottery.web.dao.IBankInfoDao;
import com.lottery.web.model.BankInfoDTO;
import com.lottery.web.service.BankInfoService;

@Service
public class BankInfoServiceImpl implements BankInfoService {
	@Autowired
	private IBankInfoDao iBankInfoDao;
	
	@Override
	public void saveBankInfo(BankInfoDTO bankInfoDTO) {
		this.iBankInfoDao.saveBankInfo(bankInfoDTO);
	}

	@Override
	public void modifyWithdrawPassword(Map<String, Object> param) {
		this.iBankInfoDao.modifyWithdrawPassword(param);

	}

	@Override
	public void updateBankInfo(BankInfoDTO bankInfoDTO) {
		this.iBankInfoDao.updateBankInfo(bankInfoDTO);
	}
	
	@Override
	public Map<String, Object> queryBankInfo(Map<String, Object> param) {
		return this.iBankInfoDao.queryBankInfo(param);
	}

}
