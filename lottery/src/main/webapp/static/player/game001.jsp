<%@ page pageEncoding="utf-8"%>
<%@include file = '../layout/playerCommon.jsp'%>
			<div id="game-lottery" type="5" ctype="1">
				<div class="lottery-container" style="display: block;"><div class="info">
				<div class="name icon-list-alt">重庆时时彩</div>
				<span id="kjsay" class="hide">开奖倒计时(<em class="kjtips">00:00</em>)</span>
<!-- 				<div class="right">
							<a href="/zst/index.php?typeid=5" target="_blank" class="gray icon-target">号码分布与遗漏分析</a>
							<a href="javascript:;" class="gray" onclick="voice.switcher()">
						<span id="voice" class="icon-volume-down">关闭声音</span>
					</a>
					<a href="javascript:scroll_to('#game-bets');" class="gray icon-shareable">快速撤单</a>
				</div> -->
			</div>
			<div class="data">
				<div class="last block">
					<div class="name icon-clock">第 <span id="last_action_no">1</span> 期投注截止计时</div>
					<div id="timer_lottery"><div class="h"><span class="number"></span><span class="number"></span></div><div class="sep"></div><div class="m"><span class="number"></span><span class="number"></span></div><div class="sep"></div><div class="s"><span class="number"></span><span class="number"></span></div></div>
				</div>
				<div class="current block">
					<div class="name icon-award"><span id="lottery-current-text">第 0 期<span class="val">开奖号码</span></span></div>
					                            
					<div class="num_right kj-hao" ctype="ssc">
						<em class="num_red_b ball_0">0</em>
						<em class="num_red_b ball_1">4</em>
						<em class="num_red_b ball_2">3</em>
						<em class="num_red_b ball_3">4</em>
						<em class="num_red_b ball_4">5</em>
					</div>
						</div>
			</div>
			</div>
			</div>
			<div id="game-play">
				<div class="group">
					<div class="name icon-th-large">玩法分类</div>
					<ul class="list" id="group_list">
									<li><a data-id="1" href="javascript:;">五星玩法</a></li>
									<li><a data-id="2" href="javascript:;">四星玩法</a></li>
									<li><a data-id="3" href="javascript:;">三星玩法</a></li>
									<li><a data-id="4" href="javascript:;">三星组选</a></li>
									<li><a data-id="5" href="javascript:;">二星直选</a></li>
									<li><a data-id="6" href="javascript:;">二星组选</a></li>
									<li><a data-id="7" href="javascript:;" class="on">定位胆</a></li>
									<li><a data-id="8" href="javascript:;">不定胆</a></li>
									<li><a data-id="9" href="javascript:;">任选玩法</a></li>
									<li><a data-id="10" href="javascript:;">趣味</a></li>
								</ul>
				</div>
				<div class="play">
					<div class="play-list">
				</div>
			<div id="play-data">
				<div class="play-info">
				</div>
				<div class="play-select">
					<div class="num-table" id="num-select">
<!-- 						<input type="hidden" name="playedGroup" value="6">
						<input type="hidden" name="playedId" value="37">
						<input type="hidden" name="type" value="5"> -->
					</div>
				</div>
			</div>
			</div>	</div>
				<div class="play-work">
					<div id="play-work-setting">
						<div id="fandian-value" data-bet-count="80000" data-bet-zj-amount="200000" max="0.3" game-fan-dian="5" fan-dian="0.3" game-fan-dian-bdw="8" fan-dian-bdw="0.0" class="left"><div class="cs-select" tabindex="0"><span class="cs-placeholder">奖金：19.06 - 返点：0.0%</span><div class="cs-options"><ul><li data-option="" data-value="eyJmYW5EaWFuIjoiMC4wIiwiYm9udXMiOiIxOS4wNiJ9" class="cs-selected"><span>奖金：19.06 - 返点：0.0%</span></li><li data-option="" data-value="eyJmYW5EaWFuIjoiMC41IiwiYm9udXMiOiIxOC45NiJ9"><span>奖金：18.96 - 返点：0.5%</span></li><li data-option="" data-value="eyJmYW5EaWFuIjoiMS4wIiwiYm9udXMiOiIxOC44NyJ9"><span>奖金：18.87 - 返点：1.0%</span></li><li data-option="" data-value="eyJmYW5EaWFuIjoiMS41IiwiYm9udXMiOiIxOC43NyJ9"><span>奖金：18.77 - 返点：1.5%</span></li><li data-option="" data-value="eyJmYW5EaWFuIjoiMi4wIiwiYm9udXMiOiIxOC42OCJ9"><span>奖金：18.68 - 返点：2.0%</span></li><li data-option="" data-value="eyJmYW5EaWFuIjoiMi41IiwiYm9udXMiOiIxOC41OCJ9"><span>奖金：18.58 - 返点：2.5%</span></li><li data-option="" data-value="eyJmYW5EaWFuIjoiMy4wIiwiYm9udXMiOiIxOC40OSJ9"><span>奖金：18.49 - 返点：3.0%</span></li><li data-option="" data-value="eyJmYW5EaWFuIjoiMy41IiwiYm9udXMiOiIxOC4zOSJ9"><span>奖金：18.39 - 返点：3.5%</span></li><li data-option="" data-value="eyJmYW5EaWFuIjoiNC4wIiwiYm9udXMiOiIxOC4zMCJ9"><span>奖金：18.30 - 返点：4.0%</span></li><li data-option="" data-value="eyJmYW5EaWFuIjoiNC41IiwiYm9udXMiOiIxOC4yMCJ9"><span>奖金：18.20 - 返点：4.5%</span></li><li data-option="" data-value="eyJmYW5EaWFuIjoiNS4wIiwiYm9udXMiOiIxOC4xMSJ9"><span>奖金：18.11 - 返点：5.0%</span></li></ul></div><select class="cs-select"><option selected="" value="eyJmYW5EaWFuIjoiMC4wIiwiYm9udXMiOiIxOS4wNiJ9">奖金：19.06 - 返点：0.0%</option><option value="eyJmYW5EaWFuIjoiMC41IiwiYm9udXMiOiIxOC45NiJ9">奖金：18.96 - 返点：0.5%</option><option value="eyJmYW5EaWFuIjoiMS4wIiwiYm9udXMiOiIxOC44NyJ9">奖金：18.87 - 返点：1.0%</option><option value="eyJmYW5EaWFuIjoiMS41IiwiYm9udXMiOiIxOC43NyJ9">奖金：18.77 - 返点：1.5%</option><option value="eyJmYW5EaWFuIjoiMi4wIiwiYm9udXMiOiIxOC42OCJ9">奖金：18.68 - 返点：2.0%</option><option value="eyJmYW5EaWFuIjoiMi41IiwiYm9udXMiOiIxOC41OCJ9">奖金：18.58 - 返点：2.5%</option><option value="eyJmYW5EaWFuIjoiMy4wIiwiYm9udXMiOiIxOC40OSJ9">奖金：18.49 - 返点：3.0%</option><option value="eyJmYW5EaWFuIjoiMy41IiwiYm9udXMiOiIxOC4zOSJ9">奖金：18.39 - 返点：3.5%</option><option value="eyJmYW5EaWFuIjoiNC4wIiwiYm9udXMiOiIxOC4zMCJ9">奖金：18.30 - 返点：4.0%</option><option value="eyJmYW5EaWFuIjoiNC41IiwiYm9udXMiOiIxOC4yMCJ9">奖金：18.20 - 返点：4.5%</option><option value="eyJmYW5EaWFuIjoiNS4wIiwiYm9udXMiOiIxOC4xMSJ9">奖金：18.11 - 返点：5.0%</option></select></div></div>
						<div id="play-mod">
							<span class="name icon-gauge">模式：</span>
							<b value="2.000" data-max-fan-dian="5" class="danwei trans on">元</b><b value="0.200" data-max-fan-dian="5" class="danwei trans">角</b><b value="0.020" data-max-fan-dian="5" class="danwei trans">分</b><b value="0.002" data-max-fan-dian="5" class="danwei trans">厘</b>			</div>
						<div id="beishu-warp">
							<span class="name icon-wrench">倍数：</span>
							<i class="sur trans icon-minus"></i>
							<input type="text" autocomplete="off" id="beishu-value" value="1" readonly>
							<i class="add trans icon-plus"></i>
						</div>
						<div class="opt">
							<a href="javascript:void(0);" id="addGameCode" class="add btn btn-red icon-pin">添加</a>
							<a href="javascript:void(0);" id="clearGameCode" class="del btn btn-green icon-trash-1">清空</a>
						</div>
						<div class="bet-info icon-chart-bar"><span id="all-count">0</span>注，<span id="all-amount">0.00</span>元</div>
					</div>
					<div id="play-work-data">
						<div id="bets-cart">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tbody><tr class="head">
									<td>玩法</td>
									<td>投注号码</td>
									<td>投注数量</td>
									<td>投注金额</td>
									<td>投注倍数</td>
									<td>投注模式</td>
									<td>奖金 - 返点</td>
									<td>操作</td>
								</tr>
							</tbody></table>
						</div>
						<div id="play-btn">
							<input type="hidden" id="zhuiHao" name="zhuiHao" value="0">
							<a href="javascript:;" id="btnPostBet" class="btn btn-red icon-basket">确认投注</a>
							<a href="javascript:;" id="btnZhuiHao" class="btn btn-yellow icon-magic">智能追号</a>
							<a href="javascript:;" class="btn btn-purple icon-sitemap hide">合买跟单</a>
						</div>
					</div>
				</div>
			</div>
			<div id="game-bets">
				<div class="menu">
					<a href="javascript: void(0);" class="icon-flag-empty on">近期投注<span class="triangle"></span></a>
					<a href="javascript:beter.remove_batch();" class="icon-trash hide" id="bet-cancel">批量撤销选中投注</a>
					<a href="touzhu.jsp" class="more icon-flag" target="_self">所有投注记录</a>
				</div>
				<div class="container">
					<div id="my-bets">
						<div class="none">您还没有当前彩种的近期投注记录</div>
						<table  cellspacing="0" cellpadding="0" class="touzhutable">
							<thead>
								<th>订单编号</th>
								<th>彩种</th>
								<th>玩法</th>
								<th>期号</th>
								<th>选号</th>
								<th>投注数量</th>
								<th>盈亏</th>
								<th>投注金额</th>
								<th>投注时间</th>
								<th>状态</th>
							</thead>
						</table>
					</div>
				</div>
			</div>
			</div>
		</div>
	<%@include file = '../layout/systemTip.jsp'%>
</body>
<script src="<%=commonURL%>/static/js/player/constant.js" type="text/javascript"></script>
<script src="<%=commonURL%>/static/js/player/gameCode.js" type="text/javascript"></script>
<script src="<%=commonURL%>/static/js/player/game001.js" type="text/javascript"></script>
<script src="<%=commonURL%>/static/js/player/gameView.js" type="text/javascript"></script>
</html>