<%@ page pageEncoding="utf-8"%>
<%@include file = '../layout/playerCommon.jsp'%>
			<div id="recharge-panel" class="money-panel">
				<div class="main">
					<!-- <form action="/user/pay" id="recharge-form" method="post" target="_blank" func="form_submit"> -->
					<input type="hidden" id="bank-id" name="bankid" value="2">
					<div id="recharge-type" class="type">
			 
						<span class="choose icon-down-dir" id="bankName">支付宝</span>
						<span class="hover icon-down-dir" style="top: 41px;">点击切换银行</span>
					</div>
					<div class="input mr15">
						<span class="icon icon-yen"></span>
						<input autocomplete="off" name="amount" required="required" type="number" id="input-money" min="10" max="50000" placeholder="请输入您的充值金额，最低：10元，最高：50000元">
						<textarea id="remark" placeholder="请输入充值的账户的详细信息"></textarea>
					</div>
					<button type="submit" class="submit btn btn-blue icon-ok" id="chongzhi">充值</button>
					<!-- </form> -->
				</div>
				<div id="bank-list" class="addon hide" onclick="choseBankFun()">
					<img width="103" height="41" class="trans active" src="../img/bank_2.jpg" title="支付宝" data-id="2">
					<img width="103" height="41" class="trans" src="../img/bank_17.jpg" title="中信银行" data-id="17">
					<img width="103" height="41" class="trans" src="../img/bank_16.jpg" title="招商银行" data-id="16">
					<img width="103" height="41" class="trans" src="../img/bank_15.jpg" title="中国银行" data-id="15">
					<img width="103" height="41" class="trans" src="../img/bank_14.jpg" title="中国农业银行" data-id="14">
					<img width="103" height="41" class="trans" src="../img/bank_13.jpg" title="中国民生银行" data-id="13">
					<img width="103" height="41" class="trans" src="../img/bank_12.jpg" title="中国建设银行" data-id="12">
					<img width="103" height="41" class="trans" src="../img/bank_11.jpg" title="中国工商银行" data-id="11">
					<img width="103" height="41" class="trans" src="../img/bank_10.jpg" title="中国光大银行" data-id="10">
					<img width="103" height="41" class="trans" src="../img/bank_9.jpg" title="中国邮政储蓄银行" data-id="9">
					<img width="103" height="41" class="trans" src="../img/bank_6.jpg" title="平安银行" data-id="6">
					<img width="103" height="41" class="trans" src="../img/bank_5.jpg" title="交通银行" data-id="5">
					<img width="103" height="41" class="trans" src="../img/bank_4.jpg" title="华夏银行" data-id="4">
					<img width="103" height="41" class="trans" src="../img/bank_3.jpg" title="广东发展银行" data-id="3">
					<img width="103" height="41" class="trans" src="../img/bank_18.jpg" title="在线支付" data-id="18">
				</div>
			</div>
			<div id="recharge-log" class="common">
				<div class="head">
					<div class="name icon-credit-card">充值记录</div>
					<form action="/user/recharge_search" class="search" data-ispage="true" container="#recharge-log .body" target="ajax" func="form_submit">
						<div class="timer">
							<input autocomplete="off" type="text" name="fromTime" value="2016-10-11 17:19" id="datetimepicker_fromTime" class="timer Wdate" onclick="WdatePicker()">
							<span class="icon icon-calendar"></span>
						</div>
						<div class="sep icon-exchange"></div>
						<div class="timer">
							<input autocomplete="off" type="text" name="toTime" value="2016-10-18 17:21" id="datetimepicker_toTime" class="timer Wdate" onclick="WdatePicker()">
							<span class="icon icon-calendar"></span>
						</div>
						<button type="button" id="queryMoneyDetail" class="btn btn-brown icon-search">查询</button>
					</form>
				</div>
				<div class="body">
					<div class="empty">
						<table  cellspacing="0" cellpadding="0" class="touzhutable" id="moneyDetailList">
							<thead>
								<th>单号</th>
								<th>类型</th>
								<th>银行</th>
								<th>金额</th>
								<th>备注</th>
								<th>时间</th>
								<th>状态</th>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="dialogue" class="dialogue mid" style="display: none;">
	<div class="dialogue-warp" style="margin-top: -167.5px;">
		<div class="dialogue-head">
			<span class="dialogue-title icon-lamp">系统提示</span>
		</div>
		<div class="dialogue-body" style="min-height: 0px;">
			<div class="detail">
				<table cellpadding="0" cellspacing="0" width="100%">
					<tbody>
						<tr>
							<td>充值单号</td>
							<td>971499</td>
						</tr>
						<tr>
							<td>充值金额</td>
							<td>1000.00 元</td>
						</tr>
						<tr></tr>
						<tr>
							<td>收款账号</td>
							<td>李宝祥</td>
						</tr>
						<tr>
							<td>收款人姓名</td>
							<td>lecaiyule@yeah.net</td>
						</tr>
						<tr>
							<td colspan="2">
								<a class="icon-link-ext" style="color:#35928f" href="https://auth.alipay.com" target="_blank">前往充值</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="dialogue-foot" style="display: block;">
			<div class="dialogue-auto" style="display: none;">
				<span class="dialogue-sec"></span>秒后自动关闭
			</div>
			<div class="right">
				<button class="dialogue-yes btn btn-blue icon-ok" style="display: inline-block;">确定</button>
				<button class="dialogue-no btn btn-white icon-undo" style="display: none;"></button>
			</div>
		</div>
	</div>
</div>
<%@include file = '../layout/systemTip.jsp'%>
</body>
<script src="../js/player/chongzhi.js"></script>
</html>