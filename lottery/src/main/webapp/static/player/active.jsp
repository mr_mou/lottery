<%@ page pageEncoding="utf-8"%>
<%@include file = '../layout/playerCommon.jsp'%>
			<div id="activity-rotary" class="common">
				<div class="head">
					<div class="name icon-globe">幸运大转盘</div>
					<div class="tab">
						<span style="background-color:#f5ecdc">幸运大转盘</span>
						<a href="/activity/exchange" target="ajax" func="loadpage">积分兑换</a>
						<!--<a href="/activity/treasure" target="ajax" func="loadpage">夺宝奇兵</a>
						<a href="/activity/bank" target="ajax" func="loadpage">电子银行</a>-->
					</div>
				</div>
				<div class="addon" style="border-top:none;border-bottom:1px solid #f0e6d4">
					<ul class="list">
						<li>您当前积分为<span class="btn btn-red" id="dom-activity-score">0</span>，可以抽奖<span class="btn btn-green" id="dom-activity-times">0</span>次；</li>
						<li>每次抽奖需要<span class="btn btn-blue">500</span>积分；</li>
						<li>积分不足不能参与抽奖活动，抽奖次数不限；</li>
						<li>本站全程监控，请勿作弊， 否则直接冻结您的账户。</li>
					</ul>
				</div>
				<div class="body">
					<div class="container">
						<div class="wheel">
							<div class="wheel-content clearfix">
								<div class="wheel-box">
									<div class="wheel-exec" id="startbtn"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@include file = '../layout/systemTip.jsp'%>
</body>
<script src="../js/player/active.js"></script>
</html>
