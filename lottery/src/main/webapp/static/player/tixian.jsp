<%@ page pageEncoding="utf-8"%>
<%@include file = '../layout/playerCommon.jsp'%>
			<div id="cash-panel" class="money-panel common">
				<div class="main">
					<input type="text" class="hide">
					<input type="password" class="hide">
					<div id="cash-type" class="type">
						<span class="choose icon-wrench">修改</span>
						<span class="hover icon-wrench" style="top: 41px;">点击修改银行信息</span>
					</div>
					<div class="input">
						<span class="icon icon-yen"></span>
						<input autocomplete="off" type="number" id="input-money" name="money" min="100" max="200000" placeholder="请输入您的提现金额，最低：100元，最高：200000元">
					</div>
					<div class="input password mr15">
						<span class="icon icon-key"></span>
						<input type="password" id="input-password" name="password" min="100" max="200000" placeholder="请输入资金密码">
					</div>
					<div class="input password mr15">
						<input type="text" id="remark" name="remark" placeholder="备注">
					</div>
					<button type="button" class="submit btn btn-blue icon-ok" id="tixian">提现</button>
				</div>
				<div id="cash-intro" class="addon">
							<ul class="list">
						 <li>您是尊贵的<span class="btn btn-red">VIP 1</span>用户，每天提现次数上限为<span class="btn btn-green" id="totalT">3</span>次，今天您已经提交<span class="btn btn-blue" id="haveT">0</span>次申请；</li>
						 <li>每天受理提现请求的时间段为<span class="color blue">11:00 ~ 22:59</span>；</li>
						 <li>提现金额最小为<span class="color red">100</span>元，最大为<span class="color red">200000</span>元；</li>
						 <li>消费比例公式：今日消费比例=今日投注量/今日充值额，消费比例未达到30%则不能提现；</li>
						 <li>如果今日未充值，则消费比例默认为100%，即使未投注也可随时提款（系统是从当天凌晨0点至第二天凌晨0点算一天）；</li>
						 <li>今日投注<span class="color green">0.020</span>元，今日充值<span class="color blue">0</span>元，您今日消费比例已达到<span class="color red">100%</span>；</li>
					</ul>
				</div>
			</div>
			<div id="cash-log" class="common">
				<div class="head">
					<div class="name icon-paper-plane">提现记录</div>
					<form action="/user/cash_search" class="search" data-ispage="true" container="#cash-log .body" target="ajax" func="form_submit">
						<div class="timer">
							<input type="text" autocomplete="off" name="fromTime" value="2016-10-13 14:05" id="datetimepicker_fromTime" class="timer Wdate"  onclick="WdatePicker()">
							<span class="icon icon-calendar"></span>
						</div>
						<div class="sep icon-exchange"></div>
						<div class="timer">
							<input type="text" autocomplete="off" name="toTime" value="2016-10-20 14:07" id="datetimepicker_toTime" class="timer Wdate" onclick="WdatePicker()">
							<span class="icon icon-calendar"></span>
						</div>
						<button type="button" class="btn btn-brown icon-search" id="queryOutRecord">查询</button>
					</form>
				</div>
				<div class="body">
					<div class="empty">
						<table  cellspacing="0" cellpadding="0" id="touzhutable" class="touzhutable">
							<thead>
								<th>单号</th>
								<th>类型</th>
								<th>金额</th>
								<th>备注</th>
								<th>时间</th>
								<th>状态</th>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@include file = '../layout/systemTip.jsp'%>
</body>
<script src="../js/player/tixian.js"></script>
</html>