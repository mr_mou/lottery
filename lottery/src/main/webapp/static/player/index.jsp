<%@ page pageEncoding="utf-8"%>
<%@include file = '../layout/playerCommon.jsp'%>
				<div id="home">
					<div class="bonus_data">
						<div id="yt_bonus">
							<div class="overview_today">
								<p class="overview_day1" id="winNum">0</p>
								<p class="overview_count" style="color:#76a4fa">总中奖数</p>
								<p class="overview_type"></p>
								<p class="overview_count1" style="color:#81c65b" id="winAllMoney">0</p>
								<p class="overview_type">总奖金额</p>
							</div>
						</div>
						<div id="yt_bonus_data">
							<div class="overview_today">
								<p class="overview_day">今日统计</p>
								<p class="overview"><span class="span1">盈亏额</span><span class="yingkuiM l" id="todayIncome">0</span></p>
								<p class="overview"><span class="span1">投注数</span><span class="l" id="todayCount">0</span></p>
								<p class="overview"><span class="span1">投注额</span><span class="l" id="todayCostMoney">0</span></p>
								<p class="overview"><span class="span1">中奖数</span><span class="l" id="todayWinNum">0</span></p>
								<p class="overview"><span class="span1">中奖额</span><span class="l" id="todayLotteryMoney">0</span></p>
							</div>
							<div class="overview_previous">
								<p class="overview_day">昨日统计</p>
								<p class="overview"><span class="span1">盈亏额</span><span class="l" id="yesterdayIncome">0</span></p>
								<p class="overview"><span class="span1">投注数</span><span class="l" id="yesterdayCount">0</span></p>
								<p class="overview"><span class="span1">投注额</span><span class="l" id="yesterdayCostMoney">0</span></p>
								<p class="overview"><span class="span1">中奖数</span><span class="l" id="yesterdayWinNum">0</span></p>
								<p class="overview"><span class="span1">中奖额</span><span class="l" id="yesterdayLotteryMoney">0</span></p>
							</div>
						</div>
						<div id="recent_bonus_data" data-highcharts-chart="0">
							<table class="yingkuiTable" cellspacing="0" cellpadding="0">
								<thead>
									<th>时间</th>
									<th>投注数</th>
									<th>投注额</th>
									<th>中奖数</th>
									<th>中奖额</th>
									<th>盈亏</th>
								</thead>
								<tbody id="yinKuiTbody">
									<tr>
										<td>2016/03/02</td>
										<td>13</td>
										<td>56</td>
										<td>62</td>
										<td>232</td>
										<td>-89</td>
									</tr>
									<tr>
										<td>2016/03/02</td>
										<td>1356565</td>
										<td>56</td>
										<td>627</td>
										<td>232</td>
										<td>-89</td>
									</tr>
									<tr>
										<td>2016/03/02</td>
										<td>13</td>
										<td>56</td>
										<td>627</td>
										<td>23277</td>
										<td>-8977</td>
									</tr>
									<tr>
										<td>2016/03/02</td>
										<td>13</td>
										<td>56</td>
										<td>62</td>
										<td>23277</td>
										<td>-89777</td>
									</tr>
									<tr>
										<td>2016/03/02</td>
										<td>13</td>
										<td>56</td>
										<td>62</td>
										<td>232</td>
										<td>-89</td>
									</tr>
									<tr>
										<td>2016/03/02</td>
										<td>13</td>
										<td>56</td>
										<td>62</td>
										<td>232</td>
										<td>-89</td>
									</tr>
									<tr>
										<td>2016/03/02</td>
										<td>13</td>
										<td>56</td>
										<td>62</td>
										<td>232</td>
										<td>-89</td>
									</tr>
								</tbody>
							</table>
						</div>
						
						<a href="yingkui.jsp" class="bonus_data_more btn btn-green icon-gauge" target="_self" func="loadpage">查看更多盈亏数据</a>
					</div>
						<div class="bet common">
						<div class="head">
							<div class="name icon-sweden">近期投注记录</div>
							<a href="touzhu.jsp" class="link icon-dot-3" style="margin-right: -415px;" target="_self">更多投注记录</a>
						</div>
						<div class="body">
							<div class="empty">
								<table  cellspacing="0" cellpadding="0" class="touzhutable">
									<thead>
										<th>订单编号</th>
										<th>彩种</th>
										<th>玩法</th>
										<th>期号</th>
										<th>选号</th>
										<th>投注数量</th>
										<th>盈亏</th>
										<th>投注金额</th>
										<th>投注时间</th>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
<%@include file = '../layout/systemTip.jsp'%>
</body>
<script src="<%=commonURL%>/static/js/player/index.js"></script>
</html>