<%@ page pageEncoding="utf-8"%>
<%@include file = '../layout/agentCommon.jsp'%>
			<div id="home" class="common">
				<div class="head">
					<div class="name icon-sitemap">代理推广数据</div>
					<a href="extendUrl.jsp" class="link icon-link-ext">获取推广链接</a>
				</div>
				<div class="bonus_data" style="height:450px;">
					<div id="yt_bonus" style="line-height:274px;border:none;height:370px;margin-left: 36px;">
						<div class="overview_today">
							<span class="icon icon-yen" style="font-size:43px"></span>
						</div>
					</div>
					<div id="yt_bonus_data" style="border:none;margin-left:-48px;height:370px;">
						<div class="overview_today" style="margin-top: 16%;">
							<p class="overview">总销量佣金</p>
							<p class="overview" id="winNum" style="color:#FA7681">1500.0</p>
							<p class="overview" >总盈利分红</p>
							<p class="overview" style="color:#FA7681">1321321.0</p>
							<!-- <p class="overview_count1" style="color:#81c65b" id="winAllMoney">5000</p>
							<p class="overview_type">总奖金额</p> -->
						</div>
						<div class="overview_previous" style="width:45%">
							<p class="overview" style="color:#03A9F4">实时资金</p>
							<p class="overview">销量佣金余额</p>
							<p class="overview" id="winNum" style="color:#FA7681">2013.3</p>
							<p class="overview">可提分红余额</p>
							<p class="overview"></p>
							<p class="overview" style="color:#FA7681" id="winAllMoney">5000.3</p>
							<p class="overview">冻结分红余额</p>
							<p class="overview" id="winNum" style="color:#FA7681">2013.3</p>
							<p class="overview" style="font-size:13px;margin-bottom: 20px;line-height:20px;">(每日分红冻结15日后提取，根据下家投注动态变化)</p>
						</div>
						<div class="morezhangbian"><a href="<%=commonURL%>/static/agent/zhangbian.jsp">查看更多账变记录 >></a></div>
					</div>
					<div id="yt_bonus" style="line-height:274px;border:none;height:370px">
						<div class="overview_today">
							<span class="icon icon-briefcase" style="font-size:43px;"></span>
						</div>
					</div>
					<div id="yt_bonus_data" style="border:none;margin-left:-48px;height:370px;">
						<div class="overview_today" style="margin-top: 16%; height: 267px;margin-left:10%;">
							<p class="overview" style="color:#03A9F4">实时人脉</p>
							<br>
							<p class="overview">所有下级代理</p>
							<p class="overview" id="subAgents" style="color:#FA7681">0</p>
							<p class="overview">所有下家玩家</p>
							<p class="overview" id="subPlayers" style="color:#FA7681">0</p>
							<!-- <p class="overview_count1" style="color:#81c65b" id="winAllMoney">5000</p>
							<p class="overview_type">总奖金额</p> -->
						</div>
						<div class="morezhangbian"><a href="<%=commonURL%>/static/agent/memberManage.jsp">查看团队管理 >></a></div>
					</div>
					<!-- <div id="recent_bonus_data" data-highcharts-chart="0">
						
					</div> -->
					
					
				</div>
				<div class="bet common">
					<div class="head">
						<div class="name icon-sweden">团队近期投注记录</div>
						<a href="touzhu.jsp" class="link icon-dot-3" target="_self">更多投注记录</a>
					</div>
					<div class="body">
						<div class="empty">
							<table  cellspacing="0" cellpadding="0" class="touzhutable">
								<thead>
									<th>订单编号</th>
									<th>用户</th>
									<th>彩种</th>
									<th>玩法</th>
									<th>期号</th>
									<th>选号</th>
									<th>投注数量</th>
									<th>盈亏</th>
									<th>投注金额(元)</th>
									<th>投注时间</th>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@include file = '../layout/systemTip.jsp'%>
</body>
<script src="<%=commonURL%>/static/js/agent/index.js"></script>
</html>