<%@ page pageEncoding="utf-8"%>
<%@include file = '../layout/agentCommon.jsp'%>
			<div id="agent-index-dom" class="common">
				<div class="intro">
					<div class="title">代理中心</div>
					<p class="desc">完善的推广、佣金及分红体系助您一臂之力</p>
				</div>
				<div class="head">
					<div class="name icon-shield">团队信息总览</div>
					<a href="javascript:$('#agent-spread').trigger('click');" class="link icon-link-ext">获取推广链接</a>
				</div>
				<div class="body" style="padding: 8px;">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tbody><tr>
							<td class="key">账号类型</td>
							<td class="val">代理</td>
							<td class="key">我的账号</td>
							<td class="val">atoz</td>
						</tr>
						<tr>
							<td class="key">可用余额</td>
							<td class="val">7.060</td>
							<td class="key">团队余额</td>
							<td class="val">8.460</td>
						</tr>
						<tr>
							<td class="key">直属下级</td>
							<td class="val">3</td>
							<td class="key">所有下级</td>
							<td class="val">3</td>
						</tr>
					</tbody></table>
				</div>
				<div class="head">
					<div class="name icon-glass">代理分红</div>
						</div>
				<div class="body" style="padding: 8px;">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tbody><tr>
							<td class="key">未结算盈亏</td>
							<td class="val">0.00 元</td>
							<td class="key">已结算盈亏</td>
							<td class="val">0.00 元</td>
						</tr>
						<tr>
							<td class="key">当前分红开始时间</td>
							<td class="val">2016-10-21 03:00:00</td>
							<td class="key">当前分红截止时间</td>
							<td class="val">2016-11-1 03:00:00</td>
						</tr>
						<tr>
							<td class="key">当前分红金额</td>
							<td class="val">0.00 元</td>
							<td class="key">已分红总计</td>
							<td class="val">0.00 元</td>
						</tr>
						<tr>
							<td class="key">已结算次数</td>
							<td class="val">0 次</td>
							<td class="key"></td>
							<td class="val"></td>
						</tr>
					</tbody></table>
				</div>
			</div>
		</div>
	</div>
</div>
<%@include file = '../layout/systemTip.jsp'%>
</body>
<script type="text/javascript" src="../js/agent/agentCenter.js"></script>
</html>