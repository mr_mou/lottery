<%@ page pageEncoding="utf-8"%>
<%@include file = '../layout/agentCommon.jsp'%>
			<div id="user-setting-dom" class="common">
				<div class="head">
					<div class="name icon-user">个人基本信息</div>
				</div>
				<div class="body" style="padding: 5px;">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tbody>
						<tr>
							<td class="key">账户名称</td>
							<td class="val" id="accountName"></td>
							<td class="key">账户类型</td>
							<td class="val" id="accountType"></td>

						</tr>
						<tr>
							<td class="key">总盈利</td>
							<td class="val">元</td>
							<td class="key">总销量返点</td>
							<td class="val">元</td>
						</tr>
						<tr>
							<td class="key">总盈利分红</td>
							<td class="val">元</td>
							<td class="key">可提取销量返点</td>
							<td class="val"></td>

						</tr>
						<tr>
							<td class="key">纯利分红（冻结）</td>
							<td class="val"> 元</td>
							<td class="key">可提取纯利分红</td>
							<td class="val"></td>
						</tr>
						<tr>
							<td class="key">腾讯QQ</td>
							<td class="val" id="tencentQQ"></td>
							<td class="key">手机号码</td>
							<td class="val" id="telephone"></td>
						</tr>
						<tr>
							<td class="key">注册时间</td>
							<td class="val" id="registerTime"></td>
							<td class="key"></td>
							<td class="val"></td>
						</tr>
					</tbody></table>
				</div>
				<div class="head">
					<div class="name icon-key">密码管理</div>
					<div class="desc">如果不修改密码，请忽略此项</div>
				</div>
				<div class="body password"  style="padding: 8px;">
					<div method="POST" action="" target="ajax" func="form_submit" class="mb">
						<div class="pwd_name">登录密码：</div>
						<input type="password" name="oldpassword" id="loginOldPassword" placeholder="请输入[当前登录密码]">
						<input type="password" name="newpassword" id="loginNewPassword" placeholder="请输入[新登录密码]">
						<input type="password" name="newpassword_confirm" id="confirmLoginNewPassword" placeholder="请重复输入[新登录密码]">
						<button type="button" class="btn btn-blue" id="changeLoginPw">修改登录密码</button>
					</div>
					<div method="POST" action="" target="ajax" func="form_submit">
						<div class="pwd_name">资金密码：</div>
						<input type="password" name="oldpassword" id="moneyOldPassword" placeholder="请输入[当前资金密码]">
						<input type="password" name="newpassword" id="moneyNewPassword" placeholder="请输入[新资金密码]">
						<input type="password" name="newpassword_confirm" id="confirmMoneyNewPassword" placeholder="请重复输入[新资金密码]">
						<button type="button" class="btn btn-green" id="changeMoneyPw">修改资金密码</button>
					</div>
				</div>
				<div class="head">
					<div class="name icon-credit-card">银行账户</div>
					<div class="desc">为了您的账户安全，确认银行账户后只能通过联系客服修改</div>
				</div>
				<div class="body card" style="padding: 5px;">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tbody>
							<tr>
								<td class="key">银行类型</td>
								<td class="val">
									<select name="bankId" id="bankId">
										<option value="2">支付宝</option>
										<option value="17">中信银行</option>
										<option value="16">招商银行</option>
										<option value="15">中国银行</option>
										<option value="14">中国农业银行</option>
										<option value="13">中国民生银行</option>
										<option value="12">中国建设银行</option>
										<option value="11">中国工商银行</option>
										<option value="10">中国光大银行</option>
										<option value="9">中国邮政储蓄银行</option>
										<option value="6">平安银行</option>
										<option value="5">交通银行</option>
										<option value="4">华夏银行</option>
										<option value="3">广东发展银行</option>
										<option value="18">在线支付</option>
									</select>
								</td>
								<td class="key">银行账户</td>
								<td class="val">
									<input id="account" type="text" name="account" placeholder="请输入[银行账户]">
								</td>
							</tr>
							<tr>
								<td class="key">银行户名</td>
								<td class="val">
									<input id="userName" type="text" name="username" placeholder="请输入[银行户名]">
								</td>
								<td class="key">开户行</td>
								<td class="val">
									<input id="countName" type="text" name="countname" placeholder="请输入[开户行]"></td>
							</tr>
							<tr>
								<td class="key">资金密码</td>
								<td class="val"><input id="coinPassword" type="password" name="coinPassword" placeholder="请输入[资金密码]"></td>
								<td></td>
								<td></td>
							</tr>
							</tbody>
						</table>
						<button id="setBankInfo" class="btn btn-brown" data-type="1">设置银行账户</button>
				</div>
			</div>
		</div>
	</div>
</div>
<%@include file = '../layout/systemTip.jsp'%>
</body>
<script src="../js/agent/set.js"></script>
</html>