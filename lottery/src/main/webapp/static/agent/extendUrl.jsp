<%@ page pageEncoding="utf-8"%>
<%@include file = '../layout/agentCommon.jsp'%>
			<div id="agent-spread-dom" class="common">
				<div class="head">
					<div class="name icon-link-ext">推广链接</div>
					<a href="javascript:void(0);" class="link_add btn btn-brown icon-plus" id="addUrl">添加推广链接</a>
				</div>
				<div class="link_add_box hide" container="#agent-spread-dom .body" id="addUrlForm" target="ajax" func="form_submit">
					<div class="item">
						<div class="name">推广类型</div>
						<div class="value type">
							<label><input type="radio" name="type" value="1" title="代理" checked="checked">代理</label>
							<label><input name="type" type="radio" value="0" title="会员">会员</label>
						</div>
					</div>
					<div class="item">
						<div class="name">用户返点</div>
						<div class="value fandian">
							<input type="text" name="fanDian" required="" title="用户返点" placeholder="用户注册后投注返点最大值">
						</div>
						<div class="addon name">%，设置上限：0.9</div>
					</div>
					<button type="submit" class="btn btn-blue icon-ok">确认添加</button>
				</div>
				<div class="body1" style="display: block;">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tbody>
							<tr class="title">
								<td title="用于区分不同的推广链接">链接编号</td>
								<td title="通过推广注册的用户类型">推广类型</td>
								<td title="用户注册后投注返点最大值">用户返点</td>
								<td title="通过该推广链接注册的人数">注册人次</td>
								<td title="该推广链接最后一次被使用的时间">更新时间</td>
								<td>相关操作</td>
							</tr>
							<tr id="spread-52">
								<td>52</td>
								<td>代理</td>
								<td>0.2%</td>
								<td>0</td>
								<td>--</td>
								<td>
									<a href="javascript:;" data-link="http://www.iba58.com/user/reg?id=5656" class="link_get icon-link-ext-alt">获取链接</a>
									<a href="/agent/spread_link_disable?lid=52" target="ajax" func="loadpage" class="disable icon-folder-empty">禁用</a>
									<a href="/agent/spread_link_enable?lid=52" target="ajax" func="loadpage" class="enable icon-folder hide">启用</a>
									<a href="/agent/spread_link_remove?lid=52" target="ajax" func="loadpage" class="icon-trash-2">删除</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<%@include file = '../layout/systemTip.jsp'%>
</body>
<script src="../js/agent/extendUrl.js"></script>
</html>