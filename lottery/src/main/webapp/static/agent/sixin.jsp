<%@ page pageEncoding="utf-8"%>
<%@include file = '../layout/agentCommon.jsp'%>
			<div id="message-receive-dom" class="common">
				<div class="head">
					<div class="name icon-mail-alt">私信</div>
						<div id="selectState" class="select-box mode">
							<div class="cs-select state" tabindex="0" id="sixin">
								<span class="cs-placeholder">所有</span>
								<div class="cs-options">
									<ul onclick="modelFun()">
										<li data-option="" data-value="0">
											<span>所有</span>
										</li>
										<li data-option="" data-value="1">
											<span>未读</span>
										</li>
										<li data-option="" data-value="2">
											<span>已读</span>
										</li>
									</ul>
								</div>
									<select name="state" class="cs-select state">
										<option value="0" selected="">所有</option>
										<option value="1">未读</option>
										<option value="2">已读</option>
									</select>
							</div>
						</div>
						<div class="timer">
							<input type="text" autocomplete="off" name="fromTime" value="2016-10-13 14:21" id="datetimepicker_fromTime" class="timer Wdate" onclick="WdatePicker()">
							<span class="icon icon-calendar"></span>
						</div>
						<div class="sep icon-exchange"></div>
						<div class="timer">
							<input type="text" autocomplete="off" name="toTime" value="2016-10-20 14:23" id="datetimepicker_toTime" class="timer Wdate" onclick="WdatePicker()">
							<span class="icon icon-calendar"></span>
						</div>
						<button type="button" id="queryToMessage" class="btn btn-brown icon-search">查询</button>
						<button type="button" id="queryFromMessage"  style="display: none;" class="btn btn-brown icon-search">查询</button>
					<div class="tab">
						<a href="javascript:void(0);" target="ajax" func="loadpage" id="shouEmail">收件箱</a>
						<a href="javascript:void(0);" target="ajax" func="loadpage" id="faEmail">发件箱</a>
						<a href="javascript:void(0);" id="wirteEmail">编写私信</a>
					</div>
				</div>
				<div class="body" style="display:none;" id="wirteE">
						<dl>
							<dt>收件人：</dt>
							<dd style="height:40px;line-height:40px" id="agentsjr">
								<label><input id="parent" name="touser" value="parent" checked="checked" type="radio">上级代理</label>
								<label><input id="children"  value="children" type="radio">下级用户</label>
								<input list="userInfo" id="toUserInfo" style="display: none;">
								<div></div>
								<input type="hidden" id="oneChildren" value="AG_1_P4">
							</dd>
						</dl>
						<dl>
							<dt>主&nbsp;&nbsp;&nbsp;题：</dt>
							<dd style="height:40px;line-height:40px"><input id="title" name="title" required="" style="padding:5px 10px;width:400px" type="text" placeholder="请输入私信主题"></dd>
						</dl>
						<dl>
							<dt>内&nbsp;&nbsp;&nbsp;容：</dt>
							<dd style="margin-top:17px"><textarea id="content" name="content" required="" style="width:700px;border:1px solid #ddd;color:#666;padding:10px;height:200px" placeholder="请输入私信内容"></textarea></dd>
						</dl>
						<button id="sendMessage" class="btn btn-green icon-ok" style="float:left;width:722px;height:40px;line-height:40px;font-size:15px;margin:15px 0 20px 95px">发送</button>
				</div>
				<div class="body" id="emailDetail">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<thead></thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<%@include file = '../layout/systemTip.jsp'%>
</body>
<script src="../js/cacheUtil.js"></script>
<script src="../js/agent/sixin.js"></script>
</html>