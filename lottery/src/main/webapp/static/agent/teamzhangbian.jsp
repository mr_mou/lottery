<%@ page pageEncoding="utf-8"%>
<%@include file = '../layout/agentCommon.jsp'%>
			<div id="coin-panel" class="panel-head">
				<div class="warp" onclick="modelFun()">
					<div class="item">
						<div class="item-name icon-right-dir">账户类</div>
						<div class="item-data">
							<a href="javascript:;" data-type="55">注册奖励</a>
							<a href="javascript:;" data-type="1">用户充值</a>
							<a href="javascript:;" data-type="9">系统充值</a>
							<a href="javascript:;" data-type="54">充值奖励</a>
							<a href="javascript:;" data-type="106">提现冻结</a>
							<a href="javascript:;" data-type="12">上级转款</a>
							<a href="javascript:;" data-type="8">提现失败返还</a>
							<a href="javascript:;" data-type="107">提现成功扣除</a>
							<a href="javascript:;" data-type="51">绑定银行奖励</a>
						</div>
					</div>
					<div class="item">
						<div class="item-name icon-right-dir">游戏类</div>
						<div class="item-data">
							<a href="javascript:;" data-type="101">投注扣款</a>
							<a href="javascript:;" data-type="108">开奖扣除</a>
							<a href="javascript:;" data-type="6">中奖奖金</a>
							<a href="javascript:;" data-type="7">撤单返款</a>
							<a href="javascript:;" data-type="102">追号投注</a>
							<a href="javascript:;" data-type="5">追号撤单</a>
							<a href="javascript:;" data-type="255">未开奖返还</a>
						</div>
					</div>
					<div class="item">
						<div class="item-name icon-right-dir">代理类</div>
						<div class="item-data">
							<a href="javascript:;" data-type="2">下级返点</a>
							<a href="javascript:;" data-type="3">代理分红</a>
							<a href="javascript:;" data-type="52">充值佣金</a>
							<a href="javascript:;" data-type="53">消费佣金</a>
							<a href="javascript:;" data-type="56">亏损佣金</a>
							<a href="javascript:;" data-type="13">转款给下级</a>
						</div>
					</div>
					<div class="item">
						<div class="item-name icon-right-dir">活动类</div>
						<div class="item-data">
							<a href="javascript:;" data-type="50">签到赠送</a>
							<a href="javascript:;" data-type="120">幸运大转盘</a>
							<a href="javascript:;" data-type="121">积分兑换</a>
						</div>
					</div>
				</div>
				<span class="triangle"></span>
			</div>
			<div id="agent-coin-dom" class="common">
				<div class="head">
					<div class="name icon-chart-bar">帐变日志</div>
					<form action="/agent/coin_search" class="search" data-ispage="true" container="#agent-coin-dom .body" target="ajax" func="form_submit">
						<div class="select trans fixed" id="select-type">
							<input type="hidden" name="type" id="input-type" value="0">
							<span class="icon icon-dot-circled"></span>
							<span class="text" id="text-type" style="width: 122px;">请选择上面帐变类型</span>
						</div>
						<div class="select-box mode">
							<div class="cs-select mode" tabindex="0" id="state">
								<span class="cs-placeholder">所有成员</span>
								<div class="cs-options">
									<ul onclick="stateFun()">
										<li data-option="" data-value="0">
											<span>所有成员</span>
										</li>
										<li data-option="" data-value="1">
											<span>直属下级</span>
										</li>
										<li data-option="" data-value="2">
											<span>所有下级</span>
										</li>
									</ul>
								</div>
								<select name="a_type" class="cs-select mode">
									<option value="0" selected="">所有成员</option>
									<option value="1">直属下级</option>
									<option value="2">所有下级</option>
								</select>
							</div>
						</div>
						<input type="text" name="username" id="username" value="用户名" class="input" style="width:100px" onfocus="if(this.value==='用户名') this.value='';" onblur="if (this.value==='') this.value='用户名';">
						<div class="timer">
							<input type="text" autocomplete="off" name="fromTime" value="2016-10-18 21:41" id="datetimepicker_fromTime" class="timer Wdate" onclick="WdatePicker()">
							<span class="icon icon-calendar"></span>
						</div>
						<div class="sep icon-exchange"></div>
						<div class="timer">
							<input type="text" autocomplete="off" name="toTime" value="2016-10-25 21:43" id="datetimepicker_toTime" class="timer Wdate" onclick="WdatePicker()">
							<span class="icon icon-calendar"></span>
						</div>
						<button type="submit" class="btn btn-brown icon-search" id="queryMoneyAccountDetail">查询</button>
					</form>
				</div>
				<div class="body">
					<table  cellspacing="0" cellpadding="0" id="moneyAccountList" class="touzhutable">
							<thead>
								<th>单号</th>
								<th>类型</th>
								<th>操作金额</th>
								<th>余额</th>
								<th>备注</th>
								<th>时间</th>
							</thead>
						</table>
			</div>
			</div>
		</div>
	</div>
</div>
<%@include file = '../layout/systemTip.jsp'%>
</body>
<script src="../js/agent/teamzhangbian.js"></script>
</html>