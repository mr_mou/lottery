<%@ page pageEncoding="utf-8"%>
<%@include file = '../layout/agentCommon.jsp'%>
			<div id="bet-log-dom" class="common">
				<div class="head">
					<div class="name icon-th-list">投注记录</div>
						<select name="type" id="betteryType">
							<option>选择彩种</option>
							<option disabled="">时时彩</option>
							<option value="1">&nbsp;&nbsp;&nbsp;重庆时时彩</option>
							<option value="14">&nbsp;&nbsp;&nbsp;乐彩五分彩</option>
							<option value="26">&nbsp;&nbsp;&nbsp;乐彩两分彩</option>
							<option value="5">&nbsp;&nbsp;&nbsp;乐彩分分彩</option>
							<option disabled="">11选5</option>
							<option value="7">&nbsp;&nbsp;&nbsp;山东11选5</option>
							<option value="6">&nbsp;&nbsp;&nbsp;广东11选5</option>
							<option value="16">&nbsp;&nbsp;&nbsp;江西11选5</option>
							<option disabled="">快三</option>
							<option disabled="">低频彩</option>
							<option value="9">&nbsp;&nbsp;&nbsp;福彩3D</option>
							<option value="10">&nbsp;&nbsp;&nbsp;排列三</option>
							<option disabled="">PK10</option>
							<option value="20">&nbsp;&nbsp;&nbsp;北京PK拾</option>
							<option disabled="">快乐8</option>
							<option disabled="">快乐十分</option>
						</select>
						<input type="text" id="betId" name="betId" placeholder="投注编号" class="input" style="width:65px">
						<div class="select-box">
							<div class="cs-select" data-index="0" id="state">
								<span class="cs-placeholder">所有状态</span>
								<div class="cs-options">
									<ul onclick="stateFun()">
										<li data-option="" data-value="0">
											<span>所有状态</span>
										</li>
										<li data-option="" data-value="1">
											<span>已派奖</span>
										</li>
										<li data-option="" data-value="2">
											<span>未中奖</span>
										</li>
										<li data-option="" data-value="3">
											<span>未开奖</span>
										</li>
										<li data-option="" data-value="4">
											<span>已追号</span>
										</li>
										<li data-option="" data-value="6">
											<span>已撤单</span>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="select-box mode">
							<div class="cs-select state" tabindex="0" id="unit">
								<span class="cs-placeholder">所有</span>
								<div class="cs-options">
									<ul onclick="unitFun()">
										<li data-option="" data-value="0.000">
											<span>所有</span>
										</li>
										<li data-option="" data-value="2.000">
											<span>元</span>
										</li>
										<li data-option="" data-value="0.200">
											<span>角</span>
										</li>
										<li data-option="" data-value="0.020">
											<span>分</span>
										</li>
									</ul>
								</div>
								<select id="model" name="model" class="cs-select state">
									<option value="0.000" selected="">模式</option>
									<option value="2.000">元</option>
									<option value="0.200">角</option>
									<option value="0.020">分</option>
								</select>
							</div>
						</div>
						<div class="timer">
							<input type="text" autocomplete="off" name="fromTime" value="2016-10-11 10:29" id="datetimepicker_fromTime" class="timer Wdate"  onclick="WdatePicker()">
							<span class="icon icon-calendar"></span>
						</div>
						<div class="sep icon-exchange"></div>
						<div class="timer">
							<input type="text" autocomplete="off" name="toTime" value="2016-10-18 10:31" id="datetimepicker_toTime" class="timer Wdate"  onclick="WdatePicker()">
							<span class="icon icon-calendar"></span>
						</div>
						<button id="queryRecord" class="btn btn-brown icon-search">查询</button>
				</div>
				<div class="body">
					<div class="empty">
						<table  cellspacing="0" cellpadding="0" class="touzhutable">
							<thead>
								<th>订单编号</th>
								<th>彩种</th>
								<th>玩法</th>
								<th>期号</th>
								<th>选号</th>
								<th>投注数量</th>
								<th>盈亏</th>
								<th>投注金额</th>
								<th>投注时间</th>
								<th>状态</th>
							</thead>
						</table>
					</div>
					<div id="Pagination" class="pagination page"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@include file = '../layout/systemTip.jsp'%>
</body>
<script src="../js/agent/touzhu.js"></script>
</html>