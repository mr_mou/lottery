<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
	String appName = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
	String commonURL = basePath + appName;
%>
<!DOCTYPE html>
<html><head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="renderer" content="webkit">
<title>乐彩欢迎您</title>
<link href="<%=commonURL%>/static/css/common.css" rel="stylesheet" type="text/css">
<link href="<%=commonURL%>/static/css/icon.css" rel="stylesheet" type="text/css">
<link href="<%=commonURL%>/static/css/page.css" rel="stylesheet" type="text/css">
<link href="<%=commonURL%>/static/css/pagination.css" rel="stylesheet" type="text/css">
<script src="<%=commonURL%>/static/js/jquery.1.7.2.js"></script>
<script src="<%=commonURL%>/static/js/pagiantion.js"></script>
<script src="<%=commonURL%>/static/js/jquery.md5.js"></script>
<script src="<%=commonURL%>/static/js/WdatePicker.js"></script>
<script src="<%=commonURL%>/static/js/cacheUtil.js"></script>
<%@include file="appData.jsp"%>
</head>
<body>
<div id="dom_body" style="min-height: 900px; display: block;">
<div id="header">
	<div class="center">
		<a href="index.jsp" class="logo">乐彩欢迎您</a>
		<div class="notice">
			<span class="icon icon-volume-down"></span>
			<marquee direction="left" scrollamount="2" scrolldelay="1">乐彩娱乐欢迎您！</marquee>
			<span class="close btn btn-green icon-cancel" id="nt-close">不再显示</span>
		</div>
		<div class="client">
			<a href="javascript:;">
				<span class="icon icon-windows"></span>
				<span class="text">Windows</span>
			</a>
			<a href="/2.png" target="_blank">
				<span class="icon icon-android-1"></span>
				<span class="text">Android</span>
			</a>
			<a href="/2.png" target="_blank">
				<span class="icon icon-appstore"></span>
				<span class="text">iPhone</span>
			</a>
		</div>
	</div>
</div>
<div id="nav">
	<div class="center" id="navDiv">
		<div class="game-bg"></div>
		<a href="<%=commonURL%>/static/agent/index.jsp" class="home icon-home" id="home" data-index="1">首页</a>
		<a href="<%=commonURL%>/static/agent/set.jsp" target="_self" func="loadpage" class="icon-cog" id="user-setting" data-index="2">个人设置</a>
		<a href="<%=commonURL%>/static/agent/touzhu.jsp" target="_self" func="loadpage" class="icon-th-list" id="bet-log" data-index="3">投注记录</a>
		<div class="agent-nav">
			<a href="<%=commonURL%>/static/agent/agentCenter.jsp" target="_self" func="loadpage" class="icon-diamond" id="agent" data-index="4">代理中心</a>
			<ul class="agent-nav-list hide" style="display: none;">
				<li><a href="<%=commonURL%>/static/agent/memberManage.jsp" target="_self" func="loadpage" class="icon-address-book" id="agent-member"  data-index="11">会员管理</a></li>
				<li><a href="<%=commonURL%>/static/agent/teamManage.jsp" target="_self" func="loadpage" class="icon-list-alt" id="agent-log"  data-index="7">团队记录</a></li>
				<li><a href="<%=commonURL%>/static/agent/yingkui.jsp" target="_self" func="loadpage" class="icon-yen" id="agent-money" data-index="8">盈亏统计</a></li>
				<li><a href="<%=commonURL%>/static/agent/teamzhangbian.jsp" target="_self" func="loadpage" class="icon-chart-bar" id="agent-coin" data-index="9">帐变日志</a></li>
				<li><a href="<%=commonURL%>/static/agent/extendUrl.jsp" target="_self" func="loadpage" class="icon-link-ext" id="agent-spread"  data-index="10">推广链接</a></li>
			</ul>
		</div>
		<a href="<%=commonURL%>/static/agent/systemNotice.jsp" target="_self" func="loadpage" class="icon-bell-alt" id="system-notice"  data-index="6">系统公告</a>
		<a href="http://nc.pop800.com/web800/c.do?n=229929" target="_blank" class="sign icon-cloud">在线客服</a>
	</div>
	<script>
	var agent_nav = $('.agent-nav');
	if (agent_nav.length > 0) {
		agent_nav.hover(function() {
			$(this).find('.agent-nav-list').slideDown(218);
		}, function() {
			$(this).find('.agent-nav-list').slideUp(218);
		});
	}
	</script>
</div>
<div id="main" class="center clearfix">
	<div id="sidebar">
		<div class="game icon-th-large">游戏中心</div>
		<div class="user">
			<div class="info" id="user-info">
				<div class="username">
					<span title="atoz" class="unval icon-user">代理名称：<span id="user-Name"></span></span>
				</div>
				
			</div>
			<div class="opt" id="optDiv">
				<div class="line">
					<a href="<%=commonURL%>/static/agent/tixian.jsp" target="_self" func="loadpage" id="user-cash" data-index="12">
						<span class="icon icon-paper-plane-empty"></span>
						<span class="text">提现</span>
					</a>
					
				</div>
				<div class="line">
					<a href="<%=commonURL%>/static/agent/sixin.jsp" target="_self" func="loadpage" class="pr" id="message-receive" data-index="13">
						<span class="icon icon-mail"></span>
						<span class="text">私信</span>
					</a>
					<a href="<%=commonURL%>/static/agent/zhangbian.jsp" target="_self" func="loadpage" id="user-coin" data-index="14">
						<span class="icon icon-chart-line"></span>
						<span class="text">帐变</span>
					</a>
					<a href="javascript:void(0);" target="_self" func="loadpage" id="layout">
						<span class="icon icon-off"></span>
						<span class="text">退出</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<script src="<%=commonURL%>/static/js/playerCommon.js"></script>
	<div id="container">
		<div id="container_warp" style="display: block;">

	<!-- </div>
	</div>
</div> -->
