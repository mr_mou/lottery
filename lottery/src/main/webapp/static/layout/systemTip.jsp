<%@ page pageEncoding="utf-8"%>
<div id="loading-page" class="dialogue" style="display: none;">
	<div class="dialogue-warp">正在努力加载中，请稍后...</div>
</div>
<div id="dialogue" class="dialogue">
	<div class="dialogue-warp">
		<div class="dialogue-head">
			<span class="dialogue-title icon-lamp">系统提示</span>
		</div>
		<div class="dialogue-body"></div>
		<div class="dialogue-foot">
			<div class="dialogue-auto">
				<span class="dialogue-sec"></span>秒后自动关闭
			</div>
			<div class="right">
				<button class="dialogue-yes btn btn-blue icon-ok"></button>
				<button class="dialogue-no btn btn-white icon-undo"></button>
			</div>
		</div>
	</div>
</div>
</div>