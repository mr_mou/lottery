<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page language="java" import="java.util.*, com.lottery.common.util.*"%>
<%
	String appName = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
	String commonURL = basePath + appName;
%>
<!DOCTYPE html>
<html><head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="renderer" content="webkit">
<title>乐彩欢迎您</title>
<link href="<%=commonURL%>/static/css/common.css" rel="stylesheet" type="text/css">
<link href="<%=commonURL%>/static/css/icon.css" rel="stylesheet" type="text/css">
<link href="<%=commonURL%>/static/css/page.css" rel="stylesheet" type="text/css">
<link href="<%=commonURL%>/static/css/pagination.css" rel="stylesheet" type="text/css">
<script src="<%=commonURL%>/static/js/jquery.1.7.2.js"></script>
<script src="<%=commonURL%>/static/js/pagiantion.js"></script>
<script src="<%=commonURL%>/static/js/jquery.md5.js"></script>
<script src="<%=commonURL%>/static/js/WdatePicker.js"></script>
<%@include file="appData.jsp"%>

</head>
<body>
<div id="dom_body" style="min-height: 900px; display: block;">
<div id="header">
	<div class="center">
		<a class="logo">欢迎您</a>
		<div class="notice">
			<span class="icon icon-volume-down"></span>
			<marquee direction="left" scrollamount="2" scrolldelay="1">乐彩娱乐欢迎您！</marquee>
			<span class="close btn btn-green icon-cancel" id="nt-close">不再显示</span>
		</div>
		<div class="client">
			<a href="javascript:;">
				<span class="icon icon-windows"></span>
				<span class="text">Windows</span>
			</a>
			<a href="/2.png" target="_blank">
				<span class="icon icon-android-1"></span>
				<span class="text">Android</span>
			</a>
			<a href="/2.png" target="_blank">
				<span class="icon icon-appstore"></span>
				<span class="text">iPhone</span>
			</a>
		</div>
	</div>
</div>
<div id="nav">
	<div class="center" id="navDiv">
		<div class="game-bg"></div>
		<a href="<%=commonURL%>/static/player/index.jsp" class="home icon-home" id="home" target="_self" data-func="loadpage" data-index="1">首页</a>
		<a href="<%=commonURL%>/static/player/set.jsp" target="_self" data-func="loadpage" class="icon-cog" id="user-setting" data-index="2">个人设置</a>
		<a href="<%=commonURL%>/static/player/touzhu.jsp" target="_self" data-func="loadpage" class="icon-th-list" id="bet-log" data-index="3">投注记录</a>
		<a href="<%=commonURL%>/static/player/yingkui.jsp" target="_self" data-func="loadpage" class="icon-chart-pie" id="user-money" data-index="4">盈亏报表</a>
		<a href="<%=commonURL%>/static/player/systemTell.jsp" target="_self" data-func="loadpage" class="icon-bell-alt" id="system-notice" data-index="6">系统公告</a>
		<a href="http://nc.pop800.com/web800/c.do?n=229929" target="_blank" class="sign icon-cloud">在线客服</a>
	</div>
</div>
<div id="main" class="center clearfix">
	<div id="sidebar">
		<div class="game icon-th-large">游戏中心</div>
		<div class="user">
			<div class="info" id="user-info">
				<div class="username">
					<span title="seaman" class="unval icon-user">彩神大名：<span id="user-Name"></span></span>
				</div>
				<div class="balance icon-fire-1">可用余额：<span id="yu-e"></span>元</div>
			</div>
			<div class="opt" id="opt">
				<div class="line">
					<a href="<%=commonURL%>/static/player/chongzhi.jsp" target="_self" data-func="loadpage" id="user-recharge" data-index="1">
						<span class="icon icon-credit-card"></span>
						<span class="text">充值</span>
					</a>
					<a href="<%=commonURL%>/static/player/tixian.jsp" target="_self" data-func="loadpage" id="user-cash" data-index="2">
						<span class="icon icon-paper-plane-empty"></span>
						<span class="text">提现</span>
					</a>
					<a href="<%=commonURL%>/static/player/sixin.jsp" target="_self" data-func="loadpage" class="pr" id="message-receive" data-index="3">
						<span class="icon icon-mail"></span>
						<span class="text">私信</span>
					</a>
				</div>
				<div class="line">
					<!-- <a href="<%=commonURL%>/static/player/qiandao.jsp" target="_self" data-func="loadpage" data-index="4">
						<span class="icon icon-calendar-empty"></span>
						<span class="text">签到</span>
					</a>-->
					<a href="<%=commonURL%>/static/player/zhangbian.jsp" target="_self" data-func="loadpage" id="user-coin" data-index="5">
						<span class="icon icon-chart-line"></span>
						<span class="text">帐变</span>
					</a>
					<a href="javascript:void(0);" target="_self" data-func="loadpage" data-index="6" id="layout">
						<span class="icon icon-off"></span>
						<span class="text">退出</span>
					</a>
				</div>
			</div>
		</div>
		<script src="<%=commonURL%>/static/js/playerCommon.js"></script>
		<div class="gs" id="game-nav">
			<div class="g">
				<div class="game-name icon-bookmark">
					<span class="text">时时彩</span>
					<span class="count">4 款彩种</span>
				</div>
				<ul class="list" style="display: none;">
					<li>
						<a data-id="1" href="<%=commonURL%>/static/player/game001.jsp" target="_self" data-func="loadpage">
							重庆时时彩						
						</a>
					</li>
					<li>
						<a data-id="14" href="<%=commonURL%>/static/player/game001.jsp" target="_self" data-func="loadpage">
							乐彩五分彩						
						</a>
					</li>
					<li>
						<a data-id="26" href="<%=commonURL%>/static/player/game001.jsp" target="_self" data-func="loadpage">
							乐彩两分彩						
						</a>
					</li>
					<li>
						<a data-id="5" href="<%=commonURL%>/static/player/game001.jsp" target="_self" data-func="loadpage">
							乐彩分分彩				
						</a>
					</li>
				</ul>
			</div>
			<div class="g">
				<div class="game-name icon-bookmark">
					<span class="text">11选5</span>
					<span class="count">3 款彩种</span>
				</div>
				<ul class="list">
					<li>
						<a data-id="7" href="<%=commonURL%>/static/player/game001.jsp" target="_self" data-func="loadpage">
							山东11选5						
						</a>
					</li>
					<li>
						<a data-id="6" href="<%=commonURL%>/static/player/game001.jsp" target="_self" data-func="loadpage">
							广东11选5						
						</a>
					</li>
					<li>
						<a data-id="16" href="<%=commonURL%>/static/player/game001.jsp" target="_self" data-func="loadpage">
							江西11选5						
						</a>
					</li>
									
				</ul>
			</div>
			<div class="g">
				<div class="game-name icon-bookmark">
					<span class="text">低频彩</span>
					<span class="count">2 款彩种</span>
				</div>
				<ul class="list">
					<li>
						<a data-id="9" href="<%=commonURL%>/static/player/game001.jsp" target="_self" data-func="loadpage">
							福彩3D						
						</a>
					</li>
					<li>
						<a data-id="10" href="<%=commonURL%>/static/player/game001.jsp" target="_self" data-func="loadpage">
							排列三						
						</a>
					</li>
				</ul>
			</div>
			<div class="g">
				<div class="game-name icon-bookmark">
					<span class="text">PK10</span>
					<span class="count">1 款彩种</span>
				</div>
				<ul class="list">
					<li>
						<a data-id="20" href="<%=commonURL%>/static/player/game001.jsp" target="_self" data-func="loadpage">
							北京PK拾				
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div id="container">
		<div id="container_warp" style="display: block;">

		<!-- </div>
	</div>
</div> -->
