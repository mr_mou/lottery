;$(function() {
	$('#navDiv').find('a').each(function(i, element) {
		var $element = $(element);
		var index = parseInt($(element).attr('data-index'));
		if (4 === index) {
			$element.addClass('on');
		} else {
			$element.removeClass('on');
		}
	});
	var name = window.getUrl.geturlset('username');
	$("#username").val(name);
	$("#state").find('.cs-placeholder').click(function(){
		$("#state").addClass("cs-active");
	});

	$('#queryMoneyAccountDetail').on('click', function() {
		$('#moneyAccountList tbody').remove();
		var beginDate = $('#datetimepicker_fromTime').val();
		var endDate = $('#datetimepicker_toTime').val();
		if (beginDate > endDate) {
			window.alert('起始时间大于结束时间');
			return false;
		}
		var param = {
			beginDate: beginDate + ':00',
			endDate: endDate + ':00'
		}
		$.ajax({
			type: 'POST',
			url: window._commonURL + '/rest/money/queryMoneyAccountList',
			data: JSON.stringify(param),
			dataType: 'json',
			contentType: 'application/json; charset=UTF-8',
			async: true,
			success: function(response) {
				console.log(response);
				var optTypes = ['', '用户充值', '系统充值', '充值奖励', '提现冻结', '上级转款', '提现失败返还', '提现成功扣除', '绑定银行卡奖励', '开奖扣除', '中奖奖金', '撤单返款', '追号投注', '追号撤单', '未开奖返还', '代理分红', '充值佣金', '消费佣金', '亏损佣金', '转款给下级', '信用大转盘', '积分兑换'];
				if (response && response.status === '00') {
					var length = response.moneyAccountList && response.moneyAccountList.length || 0;
					if(length === 0){
						alert('暂无数据');
					}else{
						var tbody = '<tbody>';
						for (var index = 0; index < length; index++) {
							var tr = '<tr><td>' + response.moneyAccountList[index].moneyAccountId + '</td>' +
									 '<td>' + optTypes[+response.moneyAccountList[index].optType] + '</td>' +
									 '<td>' + response.moneyAccountList[index].optMoney + ' 元</td>' +
									 '<td>' + response.moneyAccountList[index].totalMoney + ' 元</td>' +
									 '<td>' + (response.moneyAccountList[index].remark || '') + '</td>' +
									 '<td>' + (new Date(response.moneyAccountList[index].createDate)) + '</td>';
							tbody += tr;
						}
						tbody += '</tbody>';
						$('#moneyAccountList').append(tbody);
					}
				} else {
					window.alert(response.errorMsg);
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				console.log('error function info ---> XMLHttpRequest: ' + XMLHttpRequest + ', textStatus: ' + textStatus + ', errorThrown: ' + errorThrown);
			},
			complete: function(XMLHttpRequest) {
				console.log('complete function info ---> XMLHttpRequest: ' +  XMLHttpRequest);
			}
		});
	});
});
function stateFun(e){
	var e=window.event||e;
	e = e.srcElement||e.target; 
	if(e.nodeName == 'SPAN'){
		var val = $(e).parent().attr('data-value');
		var text = $(e).text();
		$("#state").attr('data-index',val);
		$("#state").find('.cs-placeholder').text(text);
	};
	$("#state").removeClass('cs-active');
};
function modelFunc(e){
	var e=window.event||e;
	e = e.srcElement||e.target;
	if(e.nodeName == 'A'){
		var val = $(e).attr('data-type');
		var text = $(e).text();
		clearBackground();
		$(e).css('background','#fff');
		$("#input-type").val(val);
		$("#text-type").text(text);
	};
}
function clearBackground(){
	$("#coin-panel").find('a').each(function(i,e){
		$(e).css('background','#ece0c9');
	});
}