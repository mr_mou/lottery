;(function(){
	$(function() {
		$('#navDiv').find('a').each(function(i, element) {
			var $element = $(element);
			var index = parseInt($(element).attr('data-index'));
			if (2 === index) {
				$element.addClass('on');
			} else {
				$element.removeClass('on');
			}
		});
		
		//adminSet2
		var dataindex = geturlset('set');
		if(dataindex == '1' || dataindex == ''){
			$("#payboxset").hide();
			$("#lotteryboxset").hide();
			baseInfoFun();
			$("#basesset").show();
		}else if(dataindex == '2'){
			$("#basesset").hide();
			$("#lotteryboxset").hide();
			payWayFun();
			$("#payboxset").show();
		}else{
			$("#basesset").hide();
			$("#payboxset").hide();
			lotteryListFun();
			$("#lotteryboxset").show();
		};
		//加载页面后 从后台查询数据
		
		//基本信息数据
		function baseInfoFun(){
			$("#mask").show();
			window.ajax.ajax('/lottery/rest/admin/queryPlatformBaseInfo','',queryBaseInfo);
			function queryBaseInfo(res){
				var code = res.status;
				var res = res.data;
				if(code == '00'){
					if(res.cancelOrderSwitch == 1){
						$('.yescd').attr('checked',true);
					}else{
						$('.nocd').attr('checked',true);
					};
					$('.txnum').val(res.userCashNumber);
					$('.zyl').val(res.customPlayReturnRate*100);
					$("#baseDataId").val(res.baseInfoId);
				}else{
					alert('系统繁忙，稍后再试');
				}
				$("#mask").hide();
			};
		}
		//支付信息数据
		function payWayFun(){
			$("#mask").show();
			window.ajax.ajax('/lottery/rest/admin/queryImageList','',queryPayList);
			function queryPayList(res){
				var dataList = res.dataList;
				var code = res.status;
				var payList = '';
				
				if(code == '00'){
					if(!dataList){
						alert('暂无支付方式数据，请新增！');
						return false;
					};
					$.each(dataList,function(i,b){
						var inputradio = '';
						var name = 'payway'+i;
						if(b.platformImageStatusCode == '1'){
							inputradio += '<span><label class="two">是否启用</label>'
							+'<input type="radio" value="1" name="'+name+'" checked>'
							+'<label>是</label>'
							+'<input type="radio" value="0" name="'+name+'">'
							+'<label>否</label></span>'
						}else{
							inputradio += '<span><label class="two">是否启用</label>'
							+'<input type="radio" value="1" name="'+name+'">'
							+'<label for="yes">是</label>'
							+'<input type="radio" value="0" name="'+name+'" checked>'
							+'<label for="yes">否</label></span>'
						};
						payList += '<li>'
							+'<span class="one"><input type="text" value="'+b.platformImageName+'" placeholder="请填写支付类型" class="payInput"></span>&nbsp;&nbsp;&nbsp;'
							+'<span class="two">上传二维码图片: <img src="'+b.platformImageData+'" alt="" class="codeImg" data-id="'+b.platformImageId+'" data-status="'+b.platformImageStatusCode+'" data-type="'+b.platformImageType+'"></span>'
							+'<span><input type="file" class="file"><button class="upLoad">上传</button></span>&nbsp;&nbsp;&nbsp;'
							+''+inputradio+''
							+'<span><button class="paysavebtn">保存</button></span>'
						+'</li>'
					})
					$("#payUl").html(payList);
				}else{
					alert('系统繁忙，稍后再试');
				}
				$("#mask").hide();
			}
		}
		//彩种玩法数据
		function lotteryListFun(){
			$("#mask").show();
			window.ajax.ajax('/lottery/rest/admin/queryLotteryTypePlay','',queryLotteryPlay);
			function queryLotteryPlay(res){
				console.log(res);
				var list = res.dataList;
				var code  = res.status;
				var lotterys = '';
				if(code == '00'){
					
					$.each(list,function(i,b){
						 lotterys += '<div class="lottery">'
							+'<div class="lottery-head">'
							+'<p>'
								+'<span class="lotteryName big"><input type="text" class="big read" placeholder="填写彩种名称" value="'+b.lotteryTypeName+'" data-id="'+b.lotteryTypeId+'" disabled="disabled"></span>'
								+'<span class="two">走势图：<input type="text" class="type2" placeholder="走势图路径" value="'+b.lotteryTypeImgUrl+'"></span>'
								+'<span class="two"><span class="two">基本单价：<input type="text" class="type2" placeholder="基本单价" value="'+b.lotteryTypeBaseAmount+'"></span></span>'
								+'<span class="two"><button class="openLottery">展开彩种玩法</button></span>'
							+'</p>'
						+'</div>'
						+'<div class="lottery-play-way" style="display:none;">';
						//对应彩种玩法数据
						$.each(b.lotteryPlayList,function(j,c){
							var name = 'qiyong'+j;
							var inputradio = '';
							if(c.statusCode == '1'){
								inputradio += '<span><label class="two">是否启用</label>'
								+'<input type="radio" value="1" name="'+name+'" checked>'
								+'<label>是</label>'
								+'<input type="radio" value="0" name="'+name+'">'
								+'<label>否</label></span>'
							}else{
								inputradio += '<span><label class="two">是否启用</label>'
								+'<input type="radio" value="1" name="'+name+'">'
								+'<label for="yes">是</label>'
								+'<input type="radio" value="0" name="'+name+'" checked>'
								+'<label for="yes">否</label></span>'
							};
							lotterys += '<p class="playwayp">'
											+'<span class="lotteryName rightspan"><input type="text" class="big1 read" placeholder="填写玩法名称" disabled="disabled" value="'+c.lotteryPlayName+'" data-id="'+c.lotteryPlayId+'" data-type="'+c.lotteryTypeId+'"></span>'
											+'<span class="two">基本倍率：<input type="text" class="type2" placeholder="基本倍率" value="'+c.lotteryPlayBasePrize+'"></span>'
											+''+inputradio+''
										+'</p>'
						})

						//连接后面的元素
						lotterys += '</div>'
								+'</div>';
					})
					$("#lotteryBox").html(lotterys);
				}else{
					alert('系统繁忙，稍后再试');
				}
				$("#mask").hide();
			}
		};
		function geturlset(name){
			var reg = new RegExp('(^|&)'+ name + '=([^&]*)(&|$)');
			var r = window.location.search.substr(1).match(reg);
			if (r != null)
				return unescape(r[2]);
			 return '';
		};
		function playwaybox(){
			var  playway = $('.lottery-play-way');
			for(var i=0;i<playway.length;i++){
				$(playway[i]).slideUp();
				$(playway[i]).prev().find('.openLottery').text('展开彩种玩法');
			};
		};
		

		$("body").on('click','.paybtn',function(){
			$($(this).parent()).remove();
		});
		

		$("body").on('click','.openLottery',function(){
			var divbox = $(this).parent().parent().parent().next();
			var that = $(this);
			if(divbox.is(':visible')){
				that.text('展开彩种玩法');
				divbox.slideUp();
			}else{
				playwaybox();
				that.text('折叠彩种玩法');
				divbox.slideDown();
			}
		});

		
		//增加支付方式
		$('body').on('click','#addPayWay',function(){
			var name = 'payway'+ $("#payUl").find('li').length;
			var list = '<li>'
						+'<span class="one"><input type="text" value="" placeholder="请填写支付类型" class="payInput"></span>&nbsp;&nbsp;&nbsp;'
						+'<span class="two">上传二维码图片: <img src="" alt="" class="codeImg" data-id="" data-status="" data-type=""></span>'
						+'<span><input type="file" class="file"><button class="upLoad">上传</button></span>&nbsp;&nbsp;&nbsp;'
						+'<span><label class="two">是否启用</label>'
							+'<input type="radio" value="1" name="'+name+'" checked>'
							+'<label for="yes">是</label>'
							+'<input type="radio" value="0" name="'+name+'">'
							+'<label for="yes">否</label></span>'
						+'<span><button class="paysavebtn">保存</button></span>'
					+'</li>';
			$("#payUl").append(list);
		});
		//修改后保存
		
		//基本信息修改
		$('body').on('click','#baseBtn',function(){
			var data = {
					userCashNumber:$('.txnum').val() || undefined,
					customPlayReturnRate:$('.zyl').val()/100 || undefined,
					baseInfoId:$("#baseDataId").val() || undefined,
					cancelOrderSwitch:$(".isCd").find('input[type=radio]:checked').val() || undefined
			}
			var url = '/lottery/rest/admin/setBaseInfo';
			window.ajax.ajax(url,data,setBaseInfo);
		});
		function setBaseInfo(res){
			var code = res.status;
			if(code == '00'){
				alert('基本设置更新成功');
				window.location.href = window.location.href;
			}else{
				alert('系统繁忙，稍后再试');
			}
		};
		//支付方式修改
		$('body').on('click','.paysavebtn',function(){
			var that = $(this);
			var data = {
				platformImageId:that.parent().prev().prev().prev().find('img').attr('data-id') || undefined,
				platformImageType:that.parent().prev().prev().prev().find('img').attr('data-type') || undefined,
				platformImageData:that.parent().prev().prev().prev().find('img').attr('src') || undefined,
				platformImageStatusCode:that.parent().prev().find('input[type=radio]:checked').val() || undefined,
				platformImageName:that.parent().prev().prev().prev().prev().find('input').val() || undefined
			}
			var url = '/lottery/rest/admin/mergeImage';
			window.ajax.ajax(url,data,savepayinfo);
		});
		function savepayinfo(res){
			var code = res.status;
			if(code == '00'){
				alert('支付方式更新成功');
				window.location.href = window.location.href;
			}else{
				alert('系统繁忙，稍后再试');
			}
			
		}
		/*转换函数*/
		$('body').on('change','.file',function(){
			var that1 = $(this);
	        var imgFile = new FileReader();
	        imgFile.readAsDataURL(that1[0].files[0]);
	        imgFile.onload = function () {
	            var imgData = this.result; //base64数据  
	            that1.parent().prev().find('img').attr('src',imgData);
	        }
	    });
	    //彩种修改保存
		$('body').on('click','#lotteryListBtn',function(){
			var allArr = [];
			var lotteryObj = {};
			var playObj = {};
			var divs = $("#lotteryBox").find(".lottery");
			for(var i=0;i<divs.length;i++){
				lotteryObj.lotteryPlayList = [];
				var that = $(divs[i]).find('.lottery-head').find('p').find('.lotteryName')||undefined;
				lotteryObj.lotteryTypeId = that.find('input').attr('data-id')||undefined;
				lotteryObj.lotteryTypeName = that.find('input').val()||undefined;
				lotteryObj.lotteryTypeImgUrl = that.next().find('input').val()||undefined;
				lotteryObj.lotteryTypeBaseAmount = that.next().next().find('input').val()||undefined;
				var ps = $(divs[i]).find('.lottery-play-way').find('p');
				if(!ps.length){
					lotteryObj.lotteryPlayList = [];
				}else{
					for(var j=0;j<ps.length;j++){
						var sel = $(ps[j]).find('.rightspan');
						playObj.lotteryPlayId = sel.find('input').attr('data-id')||undefined;
						playObj.lotteryPlayName = sel.find('input').val()||undefined;
						playObj.lotteryTypeId = sel.find('input').attr('data-type')||undefined;
						playObj.lotteryPlayBasePrize = sel.next().find('input').val()||undefined;
						playObj.statusCode = sel.next().next().find('input[type=radio]:checked').val()||undefined;
						lotteryObj.lotteryPlayList.push(playObj);
						playObj = {};
					};
				}
				allArr.push(lotteryObj);
				lotteryObj = {};
			};
			var data = {
				commonList:allArr || undefined
			};
			var url = '/lottery/rest/admin/updateLotteryTypePlay';
			console.log(data);
			$.ajax({
				dataType: 'json',
				type: 'POST',
				url: url,
				data: JSON.stringify(data),
				contentType:'application/json;utf-8',
				success: saveLotteryInfo,
				error:function(XMLHttpRequest, textStatus, errorThrown){
					alert('系统异常，请稍后再试');
					console.log('error function info ---> XMLHttpRequest: ' + XMLHttpRequest + ', textStatus: ' + textStatus + ', errorThrown: ' + errorThrown);
				},
				complete: function(XMLHttpRequest) {
					console.log('complete function info ---> XMLHttpRequest: ' +  XMLHttpRequest);
				}
			});
			//window.ajax.ajax(url,JSON.stringify(data),saveLotteryInfo);
		});
		function saveLotteryInfo(res){
			var code = res.status;
			if(code == '00'){
				alert('彩种玩法更新成功');
				window.location.href = window.location.href;
			}else{
				alert('系统繁忙，稍后再试');
			}
		};
	});
	
	
	// //................
	// window.onscroll = function(){
	// 	var t = document.documentElement.scrollTop || document.body.scrollTop;
	// 	var top_div = document.getElementById('gamesetNav');
	// 	if(t >= 645){
	// 		top_div.style.position = "fixed";
	// 		top_div.style.top = '10px';
	// 		top_div.style.left = '10px';
	// 		top_div.style.display = 'block';
	// 	}else{
	// 		top_div.style.position = "inherit";
	// 		top_div.style.top = 'auto';
	// 		top_div.style.left = 'auto';
	// 		top_div.style.display = 'none';
	// 	};
	// }
})()