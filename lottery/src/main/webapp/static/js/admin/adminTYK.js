;$(function() {
	$('#navDiv').find('a').each(function(i, element) {
		var $element = $(element);
		var index = parseInt($(element).attr('data-index'));
		if (4 === index) {
			$element.addClass('on');
		} else {
			$element.removeClass('on');
		}
	});
	var name = window.getUrl.geturlset('username');
	$("#username").val(name);
	$("#state").find('.cs-placeholder').click(function(){
		$("#state").addClass("cs-active");
	});
	$("#model").find('.cs-placeholder').click(function(){
		$("#model").addClass("cs-active");
	});
});
function stateFun(e){
	var e=window.event||e;
	e = e.srcElement||e.target; 
	if(e.nodeName == 'SPAN'){
		var val = $(e).parent().attr('data-value');
		var text = $(e).text();
		$("#state").attr('data-index',val);
		$("#state").find('.cs-placeholder').text(text);
	};
	$("#state").removeClass('cs-active');
};
function modelFun(e){
	var e=window.event||e;
	e = e.srcElement||e.target; 
	if(e.nodeName == 'SPAN'){
		var val = $(e).parent().attr('data-value');
		var text = $(e).text();
		$("#model").attr('data-index',val);
		$("#model").find('.cs-placeholder').text(text);
	};
	$("#model").removeClass('cs-active');
};