;$(function() {
	$('#navDiv').find('a').each(function(i, element) {
		var $element = $(element);
		var index = parseInt($(element).attr('data-index'));
		if (6 === index) {
			$element.addClass('on');
		} else {
			$element.removeClass('on');
		}
	});

	(function() {
		$.ajax({
			type: 'POST',
			url: window._commonURL + '/rest/message/querySystemTips',
			data: {},
			dataType: 'json',
			async: true,
			success: function(response) {
				console.log(response);
				if (response && response.status === '00') {
					sessionStorage.clear();
					window.cacheUtil.clearCookie();
					var length = response.systemTipList.length;
					var tbody = '<tbody>';
					for (var index = 0; index < length; index++) {
						sessionStorage.setItem(response.systemTipList[index].messageId, JSON.stringify(response.systemTipList[index]));
						window.cacheUtil.setCookie(response.systemTipList[index].messageId, JSON.stringify(response.systemTipList[index]));
						var tr = '<tr id="' + response.systemTipList[index].messageId + '">' + 
								 '<td class="tleft"><a href="javascript:void(0)" onclick="showContent(' + response.systemTipList[index].messageId + ')">' + response.systemTipList[index].messageTitle + '</a></td>' +
								 '<td>' + response.systemTipList[index].createDate + '</td>';
						tbody += tr;
					}
					tbody += '</tbody>';
					$('#systemTips').append(tbody);
				} else {
					window.alert(response.errorMsg);
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				console.log(XMLHttpRequest);
				console.log(textStatus);
				console.log(errorThrown);
			},
			complete: function(XMLHttpRequest, textStatus) {
				console.log(XMLHttpRequest),
				console.log(textStatus);
			}
		});
	})();
	
});
function showContent(messageId) {
	var message = sessionStorage.getItem(messageId) && JSON.parse(sessionStorage.getItem(messageId)) || window.cacheUtil.getCookie(messageId) && JSON.parse(window.cacheUtil.getCookie(messageId));
	var messageContent =message.messageContent || '';
	showDialog({body: '<div class="dialogue-icon error icon-attention-alt"></div><div class="dialogue-text">' + messageContent + '</div>',
										time: 60,
										okBtn: '确定'});
}

function showDialog(param) {
	if (param) {
		$('.dialogue-body').html(param.body);
		if (param.time) {
			showLeftTime(+param.time);
		}
		if (param.okBtn) {
			$('.dialogue-yes').html(param.okBtn).show().on('click', function() {
				$('#dialogue').hide();
				window.clearTimeout(window.timeout);
			});
		} else {
			$('.dialogue-yes').hide();
		}
		if (param.noBtn) {
			$('.dialogue-no').html(param.noBtn).show().on('click', function() {
				$('#dialogue').hide();
				window.clearTimeout(window.timeout);
			});
		} else {
			$('.dialogue-no').hide();
		}
		$('#dialogue').show();
	}
}

function showLeftTime(time) {
	$('.dialogue-sec').html(time);
	if (time-- < 0) {
		$('#dialogue').hide();
		return false;
	}
	window.timeout = window.setTimeout(function() {
		showLeftTime(time);
	}, 1000);
}