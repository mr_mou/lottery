
;(function($, global, undefined) {
	var selectTypes = ['全', '大', '小', '单', '双', '清'];
	var selectClazz = ['action none', 'action even', 'action odd', 'action small', 'action large', 'action all'];

	var gameView = function() {

	};

	gameView.prototype.createGame = function(gameData, dataId) {
		if (!gameData) {
			return;
		}
		$('#num-select').html('');
		$('.play-list').html('')
		$('.play-info').html('')
		for (var index = 0; index < gameData.playBaseList.length; index++) {
			var playBase = gameData.playBaseList[index];
			$('.play-list').append(createPlayList(playBase.name, playBase.dataId, playBase.clazz));
		}

		$('.play-info').append(createPlayInfo(gameData.playInfo.info, gameData.playInfo.example, gameData.playInfo.help));


		if (+dataId === 12 
			|| +dataId === 22 || +dataId === 24
			|| +dataId === 32 || +dataId === 34 || +dataId === 36
			|| +dataId === 42 || +dataId === 44 || +dataId === 46
			|| +dataId === 52 || +dataId === 54
			|| +dataId === 62 || +dataId === 64) {
			$('#num-select').html('<div class="pp pp_fix_1" action="tzSscInput" length="4" random="sscRandom">' +
						'<textarea id="textarea-code" placeholder="请按照玩法说明填写您选择的号码"></textarea>' +
						'<a href="javascript:;" id="clear_num_func">双击清空号码</a></div>');
		} else {
			var gameTarget = [];
			var oneLineInfo = '';
			for (index = gameData.gameStarLength - 1; index >= 0; index--) {
				oneLineInfo = '<div class="pp" action="tzAllSelect" length="' + gameData.gameStarLength + '" random="sscRandom">';
				oneLineInfo += ('<div class="title">' + createCellTitle(index) + '</div>');
				for (var cursor = 0; cursor < 10; cursor++) {
					if (cursor < 5) {
						oneLineInfo += createCellContent(cursor, cursor % 2 === 0 ? 'code min s' : 'code min d');
					} else {
						oneLineInfo += createCellContent(cursor, cursor % 2 === 0 ? 'code max s' : 'code max d');
					}
				}
				for (cursor = 5; cursor >= 0; cursor--) {
					oneLineInfo += createCellContent(selectTypes[cursor], selectClazz[cursor]);
				}
				oneLineInfo += '</div>'
				gameTarget.push(oneLineInfo);
				oneLineInfo = '';
			}
			
			$('#num-select').append(gameTarget.join(''));
		}

		if (gameData.eventFuctionList && gameData.eventFuctionList.length) {
			for (index = 0; index < gameData.eventFuctionList.length; index++) {
				gameData.eventFuctionList[index]();
			}
		}
	}

	gameView.prototype.createSingleGame = function() {

	}


	function createPlayList(text, dataId, clazz) {
		return '<a data-id="' + dataId + '" href="javascript:;" class="' + (clazz && clazz || '') + '">' + text + '</a>'
	}

	function createPlayInfo(info, example, help) {
		var playInfo = '<div class="help">'
						+ info
						+ '<a href="javascript:;" class="btn btn-green"><span class="icon-lamp showeg">示例</span></a>'
						+ '<a href="javascript:;" class="btn btn-blue"><span class="icon-help showeg">说明</span></a></div>'
						+ '<div id="play-example" class="play-eg hide" style="top: 309px; left: 716.623046875px; display: none;">' + example + '</div>'
						+ '<div id="play-help" class="play-eg hide" style="top: 309px; left: 774.9453125px; display: none;">' + help + '</div>';
		return playInfo;
	}

	function createCellTitle(index) {
		var title = '';
		switch (+index) {
			case 4: title = '万位'; break;
			case 3: title = '千位'; break;
			case 2: title = '百位'; break;
			case 1: title = '十位'; break;
			case 0: title = '个位'; break;
			default: title = '程序员累趴了，请原谅他。。。'; break;
		}
		return title;
	}

	function createCellContent(content, clazz) {
		return '<input type="button" value="' + content + '" class="' + clazz + '">';
	}

	gameView.prototype.showDialog = function(param) {
		if (param) {
			$('.dialogue-body').html(param.body);
			if (param.time) {
				showLeftTime(+param.time);
			}
			if (param.okBtn) {
				$('.dialogue-yes').html(param.okBtn).show().on('click', function() {
					$('#dialogue').hide();
					global.clearTimeout(global.timeout);
				});
			} else {
				$('.dialogue-yes').hide();
			}
			if (param.noBtn) {
				$('.dialogue-no').html(param.noBtn).show().on('click', function() {
					$('#dialogue').hide();
					global.clearTimeout(global.timeout);
				});
			} else {
				$('.dialogue-no').hide();
			}
			$('#dialogue').show();
		}
	}

	function showLeftTime(time) {
		$('.dialogue-sec').html(time);
		if (time-- < 0) {
			$('#dialogue').hide();
			return false;
		}
		global.timeout = global.setTimeout(function() {
			showLeftTime(time);
		}, 1000);
	}


	global.gameView = new gameView();


})(jQuery, window);