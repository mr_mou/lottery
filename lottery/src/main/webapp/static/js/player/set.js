;$(function(){
	$('#navDiv').find('a').each(function(i, element) {
		var $element = $(element);
		var index = parseInt($(element).attr('data-index'));
		if (2 === index) {
			$element.addClass('on');
		} else {
			$element.removeClass('on');
		}
	});
	var changeloginPw =  function(res){
		console.log('change password: ' + res);
		if (res) {
			if (res.status === '00') {
				window.alert('修改密码成功');
				$('#loginOldPassword').val('');
				$('#loginNewPassword').val('');
				$('#confirmLoginNewPassword').val('');
			}else{
				window.alert(res.errorMsg);
			}
		}
	};
	$("#changeLoginPw").click(function() {
		var oldPwd = $('#loginOldPassword').val();
		var newPwd = $('#loginNewPassword').val();
		var confirmNewPwd = $('#confirmLoginNewPassword').val();
		if (!oldPwd) {
			window.alert('旧密码不能为空');
			return false;
		}
		if (!newPwd || !confirmNewPwd) {
			window.alert('新密码不能为空');
			return false;
		}
		if (newPwd !== confirmNewPwd) {
			window.alert('新密码必须相同');
			return false;
		}
		var url = window._commonURL+'/rest/user/modifyPwd';
		var data = {
			oldPwd: $.md5(oldPwd),
			newPwd: $.md5(newPwd)
		};
		window.ajax.ajax(url,data,changeloginPw);
	});

	$("#changeMoneyPw").click(function() {
		var oldPwd = $('#moneyOldPassword').val();
		var newPwd = $('#moneyNewPassword').val();
		var confirmNewPwd = $('#confirmMoneyNewPassword').val();
		if (!oldPwd) {
			window.alert('旧密码不能为空');
			return false;
		}
		if (!newPwd || !confirmNewPwd) {
			window.alert('新密码不能为空');
			return false;
		}
		if (newPwd !== confirmNewPwd) {
			window.alert('新密码必须相同');
			return false;
		}
		var password = {
			oldPasswrod: $.md5(oldPwd),
			newPassword: $.md5(newPwd)
		};

		$.ajax({
			type: 'POST',
			url: window._commonURL + '/rest/bank/modifyWithdrawPassword',
			data: JSON.stringify(password),
			async: true,
			dataType: 'json',
			contentType: 'application/json; charset=UTF-8',
			success: function(response) {
				console.log(response);
				if (response && response.status === '00') {
					window.alert('修改密码成功');
					$('#moneyOldPassword').val('');
					$('#moneyNewPassword').val('');
					$('#confirmMoneyNewPassword').val('');
				} else {
					window.alert(response.errorMsg);
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				console.log('error function info ---> XMLHttpRequest: ' + XMLHttpRequest + ', textStatus: ' + textStatus + ', errorThrown: ' + errorThrown);
			},
			complete: function(XMLHttpRequest) {
				console.log('complete function info ---> XMLHttpRequest: ' +  XMLHttpRequest);
			}
		});
	});

	$.ajax({
		type: 'POST',
		url: window._commonURL + '/rest/user/info',
		data: {},
		async: true,
		dataType: 'json',
		contentType: 'application/json; charset=UTF-8',
		success: function(response) {
			console.log(response);
			if (response && response.status === '00') {
				$('#accountName').text(response.userName || '');
				var accountType = '会员';
				accountType = response.role === 'agent' ? '代理' : response.role === 'admin' ? '管理员' : '会员';
				$('#accountType').text(accountType);
				$('#leftMoney').text((response.amount || 0) + ' 元');
				$('#registerTime').text(response.createTime || '');
				$('#tencentQQ').text(response.qq || '');
				$('#telephone').text(response.mobile || '');
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			console.log('error function info ---> XMLHttpRequest: ' + XMLHttpRequest + ', textStatus: ' + textStatus + ', errorThrown: ' + errorThrown);
		},
		complete: function(XMLHttpRequest) {
			console.log('complete function info ---> XMLHttpRequest: ' +  XMLHttpRequest);
		}

	});

	$('#setBankInfo').on('click', function() {
		var bankCode = $('#bankId option:selected').attr('value');
		var bankType = $('#bankId option:selected').text();
		var account = $('#account').val();
		var userName = $('#userName').val();
		var countName = $('#countName').val();
		var coinPassword = $('#coinPassword').val();
		var type = $(this).attr('data-type');
		if (!account) {
			window.alert('银行账户不能为空');
			return false;
		}
		if (!userName) {
			window.alert('银行户名不能为空');
			return false;
		}
		if (!countName) {
			window.alert('开户行不能为空');
			return false;
		}

		if (type === '1' && !coinPassword) {
			window.alert('密码不能为空');
			return false;
		}
		var bankInfo = {
			bankCode: bankCode,
			bankType: bankType,
			bankAccount: account,
			bankUser: userName,
			bankName: countName,
			optType: type
		}
		if (type === '1') {
			bankInfo.password = $.md5(coinPassword);
		}

		$.ajax({
			type: 'POST',
			url: window._commonURL + '/rest/bank/saveBankInfo',
			data: bankInfo,
			async: true,
			dataType: 'json',
			success: function(response) {
				console.log(response);
				if (response && response.status === '00') {
					window.alert('操作成功');
					$('#coinPassword').parent().parent().remove();
					$('#setBankInfo').text('修改银行账户').attr('data-type', '2');
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				console.log('error function info ---> XMLHttpRequest: ' + XMLHttpRequest + ', textStatus: ' + textStatus + ', errorThrown: ' + errorThrown);
			},
			complete: function(XMLHttpRequest) {
				console.log('complete function info ---> XMLHttpRequest: ' +  XMLHttpRequest);
			}
		});
 	});

	(function() {
		$.ajax({
			type: 'POST',
			url: window._commonURL + '/rest/bank/queryBankInfo',
			data: {},
			async: true,
			dataType: 'json',
			success: function(response) {
				console.log(response);
				if (response && response.status === '00' && response.bankCode) {
					$('#bankId option').each(function(index, element) {
						if ($(this).val() === response.bankCode) {
							$(this).attr('selected', 'selected');
							return false;
						}
					});
					$('#account').val(response.bankAccount);
					$('#userName').val(response.bankUser);
					$('#countName').val(response.bankName);
					$('#coinPassword').parent().parent().remove();
					$('#setBankInfo').text('修改银行账户').attr('data-type', '2');
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				console.log('error function info ---> XMLHttpRequest: ' + XMLHttpRequest + ', textStatus: ' + textStatus + ', errorThrown: ' + errorThrown);
			},
			complete: function(XMLHttpRequest) {
				console.log('complete function info ---> XMLHttpRequest: ' +  XMLHttpRequest);
			}
		});
	})();

});
