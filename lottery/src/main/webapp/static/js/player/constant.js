;(function($, global, undefined) {
	var gameDatas = [];

	var gameData = {};
	gameData.gameName = 'fiveStarGame';
	gameData.gameStarLength = 5;
	gameData.playBaseList = [{name: '五星复式', dataId: 11, clazz: 'on'}, {name: '五星单式', dataId: 12, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：在万位、千位、百位、十位、个位任意位置上任意选择1个或1个以上号码。',
						 example: '投注方案：12345，开奖号码：12345，即中五星直选',
				 		 help: '从万位、千位、百位、十位、个位中选择一个5位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖。'};			
	gameDatas[1] = gameData;
	gameDatas[11] = gameData;

	gameData = {};
	gameData.gameName = 'fiveStarGame';
	gameData.gameStarLength = 5;
	gameData.playBaseList = [{name: '五星复式', dataId: 11, clazz: ''}, {name: '五星单式', dataId: 12, clazz: 'on'}];
	gameData.playInfo = {info: '玩法说明：手动输入号码，至少输入1个五位数号码组成一注。',
						 example: '如：输入1234567890，则你投两注分别为12345和67890，如果开奖号为12345，则你中奖一注。',
				 		 help: '手动输入一个5位数号码组成一注，所选号码与开奖号码的万位、千位、百位、十位、个位相同，且顺序一致，即为中奖。'};
	gameDatas[12] = gameData;

	gameData = {};
	gameData.gameName = 'fourStarGame';
	gameData.gameStarLength = 4;
	gameData.playBaseList = [{name: '前四复式', dataId: 21, clazz: 'on'}, {name: '前四单式', dataId: 22, clazz: ''},
							{name: '后四复式', dataId: 23, clazz: ''}, {name: '后四单式', dataId: 24, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：从万位、千位、百位、十位各选一个号码组成一注。',
						 example: '投注方案：1234，开奖号码：1234，即中前四直选',
						 help: '从万位、千位、百位、十位中选择一个4位数号码组成一注，所选号码与开奖号码前4位相同，且顺序一致，即为中奖。'};
	gameDatas[2] = gameData;
	gameDatas[21] = gameData;

	gameData = {};
	gameData.gameName = 'fourStarGame';
	gameData.gameStarLength = 4;
	gameData.playBaseList = [{name: '前四复式', dataId: 21, clazz: ''}, {name: '前四单式', dataId: 22, clazz: 'on'},
							{name: '后四复式', dataId: 23, clazz: ''}, {name: '后四单式', dataId: 24, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：手动输入号码，至少输入1个四位数号码组成一注。',
						 example: '投注方案：1234，开奖号码：1234，即中前四直选',
						 help: '手动输入一个4位数号码组成一注，所选号码与开奖号码的万位、千位、百位、十位相同，且顺序一致，即为中奖。'};
	gameDatas[22] = gameData;

	gameData = {};
	gameData.gameName = 'fourStarGame';
	gameData.gameStarLength = 4;
	gameData.playBaseList = [{name: '前四复式', dataId: 21, clazz: ''}, {name: '前四单式', dataId: 22, clazz: ''},
							{name: '后四复式', dataId: 23, clazz: 'on'}, {name: '后四单式', dataId: 24, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：手动输入号码，至少输入1个四位数号码组成一注。',
						 example: '投注方案：1234，开奖号码：1234，即中前四直选',
						 help: '手动输入一个4位数号码组成一注，所选号码与开奖号码的万位、千位、百位、十位相同，且顺序一致，即为中奖。'};
	gameDatas[23] = gameData;

	gameData = {};
	gameData.gameName = 'fourStarGame';
	gameData.gameStarLength = 4;
	gameData.playBaseList = [{name: '前四复式', dataId: 21, clazz: ''}, {name: '前四单式', dataId: 22, clazz: ''},
							{name: '后四复式', dataId: 23, clazz: ''}, {name: '后四单式', dataId: 24, clazz: 'on'}];
	gameData.playInfo = {info: '玩法说明：手动输入号码，至少输入一个四位数号码组成一注。',
						 example: '投注方案：1234，开奖号码：1234，即中四星直选。',
						 help: '手动输入一个4位数号码组成一注，所选号码与开奖号码的千位、百位、十位、个位相同，且顺序一致，即为中奖。'};
	gameDatas[24] = gameData;

	gameData = {};
	gameData.gameName = 'threeStarGame';
	gameData.gameStarLength = 3;
	gameData.playBaseList = [{name: '前三复式', dataId: 31, clazz: 'on'}, {name: '前三单式', dataId: 32, clazz: ''},
						{name: '中三复式', dataId: 33, clazz: ''}, {name: '中三单式', dataId: 34, clazz: ''},
						{name: '后三复式', dataId: 35, clazz: ''}, {name: '后三单式', dataId: 36, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：从万位、千位、百位各选一个号码组成一注。',
						 example: '投注方案：123，开奖号码：前三位123，即中前三直选。',
						 help: '从万位、千位、百位中选择一个三位数号码组成一注，所选号码与开奖号码前三位相同，且顺序一致，即为中奖。'};
	gameDatas[3] = gameData;
	gameDatas[31] = gameData;

	gameData = {};
	gameData.gameName = 'threeStarGame';
	gameData.gameStarLength = 3;
	gameData.playBaseList = [{name: '前三复式', dataId: 31, clazz: ''}, {name: '前三单式', dataId: 32, clazz: 'on'},
						{name: '中三复式', dataId: 33, clazz: ''}, {name: '中三单式', dataId: 34, clazz: ''},
						{name: '后三复式', dataId: 35, clazz: ''}, {name: '后三单式', dataId: 36, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：手动输入号码，至少输入1个三位数号码组成一注。',
						 example: '投注方案：123，开奖号码：前三位123，即中前三直选。',
						 help: '手动输入一个3位数号码组成一注，所选号码与开奖号码的前三位相同，且顺序一致，即为中奖！'};
	gameDatas[32] = gameData;

	gameData = {};
	gameData.gameName = 'threeStarGame';
	gameData.gameStarLength = 3;
	gameData.playBaseList = [{name: '前三复式', dataId: 31, clazz: ''}, {name: '前三单式', dataId: 32, clazz: ''},
						{name: '中三复式', dataId: 33, clazz: 'on'}, {name: '中三单式', dataId: 34, clazz: ''},
						{name: '后三复式', dataId: 35, clazz: ''}, {name: '后三单式', dataId: 36, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：从千位、百位、十位各选一个号码组成一注。',
						 example: '投注方案：1,2,3   开奖号码：0,1,2,3,4  即中 中三复试',
						 help: '从千位、百位、十位中选择一个三位数号码组成一注，所选号码与开奖号码中三位相同，且顺序一致，即为中奖。'};
	gameDatas[33] = gameData;

	gameData = {};
	gameData.gameName = 'threeStarGame';
	gameData.gameStarLength = 3;
	gameData.playBaseList = [{name: '前三复式', dataId: 31, clazz: ''}, {name: '前三单式', dataId: 32, clazz: ''},
						{name: '中三复式', dataId: 33, clazz: ''}, {name: '中三单式', dataId: 34, clazz: 'on'},
						{name: '后三复式', dataId: 35, clazz: ''}, {name: '后三单式', dataId: 36, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：从千位、百位、十位各选一个号码组成一注。',
						 example: '',
						 help: '从千位、百位、十位中选择一个三位数号码组成一注，所选号码与开奖号码中三位相同，且顺序一致，即为中奖。'};
	gameDatas[34] = gameData;

	gameData = {};
	gameData.gameName = 'threeStarGame';
	gameData.gameStarLength = 3;
	gameData.playBaseList = [{name: '前三复式', dataId: 31, clazz: ''}, {name: '前三单式', dataId: 32, clazz: ''},
						{name: '中三复式', dataId: 33, clazz: ''}, {name: '中三单式', dataId: 34, clazz: ''},
						{name: '后三复式', dataId: 35, clazz: 'on'}, {name: '后三单式', dataId: 36, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：从千位、百位、十位各选一个号码组成一注。',
						 example: '投注方案：123，开奖号码：后三位123，即中后三直选。',
						 help: '从百位、十位、个位中选择一个三位数号码组成一注，所选号码与开奖号码后三位相同，且顺序一致，即为中奖。'};
	gameDatas[35] = gameData;

	gameData = {};
	gameData.gameName = 'threeStarGame';
	gameData.gameStarLength = 3;
	gameData.playBaseList = [{name: '前三复式', dataId: 31, clazz: ''}, {name: '前三单式', dataId: 32, clazz: ''},
						{name: '中三复式', dataId: 33, clazz: ''}, {name: '中三单式', dataId: 34, clazz: ''},
						{name: '后三复式', dataId: 35, clazz: ''}, {name: '后三单式', dataId: 36, clazz: 'on'}];
	gameData.playInfo = {info: '玩法说明：从千位、百位、十位各选一个号码组成一注。',
						 example: '投注方案：123，开奖号码：后三位123，即中后三直选。',
						 help: '手动输入一个3位数号码组成一注，所选号码与开奖号码的后三位相同，且顺序一致，即为中奖！'};
	gameDatas[36] = gameData;

	gameData = {};
	gameData.gameName = 'threeStarComposeGame';
	gameData.gameStarLength = 1;
	gameData.playBaseList = [{name: '前三组三', dataId: 41, clazz: 'on'}, {name: '前三组六', dataId: 42, clazz: ''},
						{name: '中三组三', dataId: 43, clazz: ''}, {name: '中三组六', dataId: 44, clazz: ''},
						{name: '后三组三', dataId: 45, clazz: ''}, {name: '后三组六', dataId: 46, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：从0-9中任意选择2个或2个以上号码。',
						 example: '投注方案：122  开奖号码：前三位221(顺序不限)，即中前三组选三',
						 help: '从0-9中选择2个或者2个以上数字组成两注或两注以上，所选号码与开奖号码前三位相同，顺序不限，即为中奖。'};
	gameDatas[4] = gameData;
	gameDatas[41] = gameData;

	gameData = {};
	gameData.gameName = 'threeStarComposeGame';
	gameData.gameStarLength = 1;
	gameData.playBaseList = [{name: '前三组三', dataId: 41, clazz: ''}, {name: '前三组六', dataId: 42, clazz: 'on'},
						{name: '中三组三', dataId: 43, clazz: ''}, {name: '中三组六', dataId: 44, clazz: ''},
						{name: '后三组三', dataId: 45, clazz: ''}, {name: '后三组六', dataId: 46, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：从0-9中任意选择3个或3个以上号码。',
						 example: '投注方案：135   开奖号码：前三位513(顺序不限)，即中前三组选六。',
						 help: '从0-9中选择3个或者3个以上数字组成1注或1注以上，所选号码与开奖号码前三位相同，顺序不限，即为中奖。'};
	gameDatas[42] = gameData;

	gameData = {};
	gameData.gameName = 'threeStarComposeGame';
	gameData.gameStarLength = 1;
	gameData.playBaseList = [{name: '前三组三', dataId: 41, clazz: ''}, {name: '前三组六', dataId: 42, clazz: ''},
						{name: '中三组三', dataId: 43, clazz: 'on'}, {name: '中三组六', dataId: 44, clazz: ''},
						{name: '后三组三', dataId: 45, clazz: ''}, {name: '后三组六', dataId: 46, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：从0-9中任意选择2个或2个以上号码。',
						 example: '投注方案：122  开奖号码：中三位221(顺序不限)，即中 中三组选三',
						 help: '从0-9中选择2个或者2个以上数字组成两注或两注以上，所选号码与开奖号码中三位相同，顺序不限，即为中奖。'};
	gameDatas[43] = gameData;

	gameData = {};
	gameData.gameName = 'threeStarComposeGame';
	gameData.gameStarLength = 1;
	gameData.playBaseList = [{name: '前三组三', dataId: 41, clazz: ''}, {name: '前三组六', dataId: 42, clazz: ''},
						{name: '中三组三', dataId: 43, clazz: ''}, {name: '中三组六', dataId: 44, clazz: 'on'},
						{name: '后三组三', dataId: 45, clazz: ''}, {name: '后三组六', dataId: 46, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：从0-9中任意选择3个或3个以上号码。',
						 example: '投注方案：135   开奖号码：中三位513(顺序不限)，即中 中三组选六。',
						 help: '从0-9中选择3个或者3个以上数字组成1注或1注以上，所选号码与开奖号码中三位相同，顺序不限，即为中奖。'};
	gameDatas[44] = gameData;

	gameData = {};
	gameData.gameName = 'threeStarComposeGame';
	gameData.gameStarLength = 1;
	gameData.playBaseList = [{name: '前三组三', dataId: 41, clazz: ''}, {name: '前三组六', dataId: 42, clazz: ''},
						{name: '中三组三', dataId: 43, clazz: ''}, {name: '中三组六', dataId: 44, clazz: ''},
						{name: '后三组三', dataId: 45, clazz: 'on'}, {name: '后三组六', dataId: 46, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：从0-9中任意选择2个或2个以上号码。',
						 example: '投注方案：12   开奖号码：后三位221(顺序不限)，即中后三组选三。',
						 help: '从0-9中选择2个或者2个以上数字组成2注或2注以上，所选号码与开奖号码后三位相同，顺序不限，即为中奖。'};
	gameDatas[45] = gameData;

	gameData = {};
	gameData.gameName = 'threeStarComposeGame';
	gameData.gameStarLength = 1;
	gameData.playBaseList = [{name: '前三组三', dataId: 41, clazz: ''}, {name: '前三组六', dataId: 42, clazz: ''},
						{name: '中三组三', dataId: 43, clazz: ''}, {name: '中三组六', dataId: 44, clazz: ''},
						{name: '后三组三', dataId: 45, clazz: ''}, {name: '后三组六', dataId: 46, clazz: 'on'}];
	gameData.playInfo = {info: '玩法说明：从0-9中任意选择3个或3个以上号码。',
						 example: '投注方案：135   开奖号码：后三位513(顺序不限)，即中后三组选六。',
						 help: '从0-9中任意选择3个号码组成一注，所选号码与开奖号码的百位、十位、个位相同，顺序不限，即为中奖。'};
	gameDatas[46] = gameData;

	gameData = {};
	gameData.gameName = 'twoStarDirectGame';
	gameData.gameStarLength = 2;
	gameData.playBaseList = [{name: '前二复式', dataId: 51, clazz: 'on'}, {name: '前二单式', dataId: 52, clazz: ''},
						{name: '后二复式', dataId: 53, clazz: ''}, {name: '后二单式', dataId: 54, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：从万位、千位中至少各选1个号码组成一注。',
						 example: '投注方案：12  开奖号码：12345，即中前二直选',
						 help: '从万位、千位中选择一个2位数号码组成一注，所选号码与开奖号码的前二位相同，且顺序一致，即为中奖。'};
	gameDatas[5] = gameData;
	gameDatas[51] = gameData;

	gameData = {};
	gameData.gameName = 'twoStarDirectGame';
	gameData.gameStarLength = 2;
	gameData.playBaseList = [{name: '前二复式', dataId: 51, clazz: ''}, {name: '前二单式', dataId: 52, clazz: 'on'},
						{name: '后二复式', dataId: 53, clazz: ''}, {name: '后二单式', dataId: 54, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：手动输入号码，至少输入1个二位数号码组成一注。',
						 example: '投注方案：12  开奖号码：12345，即中前二直选',
						 help: '手动输入一个2位数号码组成一注，输入号码与开奖号码前二位相同，且顺序一致，即为中奖。'};
	gameDatas[52] = gameData;

	gameData = {};
	gameData.gameName = 'twoStarDirectGame';
	gameData.gameStarLength = 2;
	gameData.playBaseList = [{name: '前二复式', dataId: 51, clazz: ''}, {name: '前二单式', dataId: 52, clazz: ''},
						{name: '后二复式', dataId: 53, clazz: 'on'}, {name: '后二单式', dataId: 54, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：从十位、个位中至少各选1个号码组成一注。',
						 example: '投注方案：45  开奖号码：12345，即中后二直选',
						 help: '从十位、个位中选择一个2位数号码组成一注，所选号码与开奖号码的后二位相同，且顺序一致，即为中奖。'};
	gameDatas[53] = gameData;

	gameData = {};
	gameData.gameName = 'twoStarDirectGame';
	gameData.gameStarLength = 2;
	gameData.playBaseList = [{name: '前二复式', dataId: 51, clazz: ''}, {name: '前二单式', dataId: 52, clazz: ''},
						{name: '后二复式', dataId: 53, clazz: ''}, {name: '后二单式', dataId: 54, clazz: 'on'}];
	gameData.playInfo = {info: '玩法说明：手动输入号码，至少输入1个二位数号码组成一注。',
						 example: '投注方案：45  开奖号码：12345，即中后二直选',
						 help: '手动输入一个2位数号码组成一注，输入号码与开奖号码后二位相同，且顺序一致，即为中奖。'};
	gameDatas[54] = gameData;

	gameData = {};
	gameData.gameName = 'twoStarComposeGame';
	gameData.gameStarLength = 1;
	gameData.playBaseList = [{name: '前二组选复式', dataId: 61, clazz: 'on'}, {name: '前二组选单式', dataId: 62, clazz: ''},
						{name: '后二组选复式', dataId: 63, clazz: ''}, {name: '后二组选单式', dataId: 64, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：从0-9中任意选择2个或2个以上号码。',
						 example: '投注方案：12   开奖号码：前二位12或21(顺序不限，不含对子号)，即中前二组选。',
						 help: '从0-9中选2个号码组成一注，所择号码与开奖号码的万位、千位相同，顺序不限，即为中奖。'};
	gameDatas[6] = gameData;
	gameDatas[61] = gameData;

	gameData = {};
	gameData.gameName = 'twoStarComposeGame';
	gameData.gameStarLength = 1;
	gameData.playBaseList = [{name: '前二组选复式', dataId: 61, clazz: ''}, {name: '前二组选单式', dataId: 62, clazz: 'on'},
						{name: '后二组选复式', dataId: 63, clazz: ''}, {name: '后二组选单式', dataId: 64, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：手动输入号码，至少输入1个二位数号码组成一注。',
						 example: '投注方案：12   开奖号码：前二位12或21(顺序不限，不含对子号)，即中前二组选。',
						 help: '手动输入一个2位数号码组成一注，输入号码与开奖号码的前二位相同，顺序不限，即为中奖。'};
	gameDatas[62] = gameData;

	gameData = {};
	gameData.gameName = 'twoStarComposeGame';
	gameData.gameStarLength = 1;
	gameData.playBaseList = [{name: '前二组选复式', dataId: 61, clazz: ''}, {name: '前二组选单式', dataId: 62, clazz: ''},
						{name: '后二组选复式', dataId: 63, clazz: 'on'}, {name: '后二组选单式', dataId: 64, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：从0-9中任意选择2个或2个以上号码。',
						 example: '投注方案：12   开奖号码：后二位12或21(顺序不限，不含对子号)，即中后二组选。',
						 help: '从0-9中选择2个号码组成一注，所选号码与开奖号码的后二位相同，顺序不限，即为中奖。'};
	gameDatas[63] = gameData;

	gameData = {};
	gameData.gameName = 'twoStarComposeGame';
	gameData.gameStarLength = 1;
	gameData.playBaseList = [{name: '前二组选复式', dataId: 61, clazz: ''}, {name: '前二组选单式', dataId: 62, clazz: ''},
						{name: '后二组选复式', dataId: 63, clazz: ''}, {name: '后二组选单式', dataId: 64, clazz: 'on'}];
	gameData.playInfo = {info: '玩法说明：手动输入号码，至少输入1个二位数号码组成一注。',
						 example: '投注方案：12   开奖号码：后二位12或21(顺序不限，不含对子号)，即中后二组选。',
						 help: '手动输入一个2位数号码组成一注，输入号码与开奖号码的后二位相同，顺序不限，即为中奖。'};
	gameDatas[64] = gameData;

	gameData = {};
	gameData.gameName = 'fixedGame';
	gameData.gameStarLength = 5;
	gameData.playBaseList = [{name: '五星定位胆', dataId: 71, clazz: 'on'}];
	gameData.playInfo = {info: '玩法说明：在万位、千位、百位、十位、个位任意位置上任意选择1个或1个以上号码。',
						 example: '投注方案：个位 1    开奖号码：个位 1，即中定位胆个位。',
						 help: '从万位、千位、百位、十位、个位任意1个位置或多个位置上选择1个号码，所选号码与相同位置上的开奖号码一致，即为中奖。'};
	gameDatas[7] = gameData;
	gameDatas[71] = gameData;

	gameData = {};
	gameData.gameName = 'notFixedGame';
	gameData.gameStarLength = 1;
	gameData.playBaseList = [{name: '前三一码', dataId: 81, clazz: 'on'}, {name: '中三一码', dataId: 82, clazz: ''},
						{name: '后三一码', dataId: 83, clazz: ''}, {name: '任三一码', dataId: 84, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：从0-9中任意选择1个以上号码。',
						 example: '投注方案：1   开奖号码：前三位至少出现1个1，即中前三一码不定位。',
						 help: '从0-9中选择1个号码，每注由1个号码组成，只要开奖号码万位、千位、百位中包含所选号码，即为中奖。'};
	gameDatas[8] = gameData;
	gameDatas[81] = gameData;

	gameData = {};
	gameData.gameName = 'notFixedGame';
	gameData.gameStarLength = 1;
	gameData.playBaseList = [{name: '前三一码', dataId: 81, clazz: ''}, {name: '中三一码', dataId: 82, clazz: 'on'},
						{name: '后三一码', dataId: 83, clazz: ''}, {name: '任三一码', dataId: 84, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：从0-9中任意选择1个以上号码。',
						 example: '投注方案：1   开奖号码：千位、百位、十位至少出现1个1，即中中三一码不定位。',
						 help: '从0-9中选择1个号码，每注由1个号码组成，只要开奖号码千位、百位、十位中包含所选号码，即为中奖。'};
	gameDatas[82] = gameData;

	gameData = {};
	gameData.gameName = 'notFixedGame';
	gameData.gameStarLength = 1;
	gameData.playBaseList = [{name: '前三一码', dataId: 81, clazz: ''}, {name: '中三一码', dataId: 82, clazz: ''},
						{name: '后三一码', dataId: 83, clazz: 'on'}, {name: '任三一码', dataId: 84, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：从0-9中任意选择1个以上号码。',
						 example: '投注方案：1   开奖号码：后三位至少出现1个1，即中后三一码不定位。',
						 help: '从0-9中选择1个号码，每注由1个号码组成，只要开奖号码百位、十位、个位中包含所选号码，即为中奖。'};
	gameDatas[83] = gameData;

	gameData = {};
	gameData.gameName = 'notFixedGame';
	gameData.gameStarLength = 1;
	gameData.playBaseList = [{name: '前三一码', dataId: 81, clazz: ''}, {name: '中三一码', dataId: 82, clazz: ''},
						{name: '后三一码', dataId: 83, clazz: ''}, {name: '任三一码', dataId: 84, clazz: 'on'}];
	gameData.playInfo = {info: '玩法说明：从万位、千位、百位、十位、个位中选择三个位置，从0-9中任意选择1个号码组成一注。',
						 example: '投注方案：选择位置：万位、十位、个位，选择号码 1   开奖号码：万位、十位、个位至少出现1个1，即中任三一码不定位。',
						 help: '从万位、千位、百位、十位、个位中选择三个位置，从0-9中任意选择1个号码组成一注。开奖号码的指定位置包含所选号码，即为中奖。'};
	gameDatas[84] = gameData;

	gameData = {};
	gameData.gameName = 'anySelectGame';
	gameData.gameStarLength = 5;
	gameData.playBaseList = [{name: '任二复式', dataId: 91, clazz: 'on'}, {name: '任二单式', dataId: 92, clazz: ''},
						{name: '任三复式', dataId: 93, clazz: ''}, {name: '任三单式', dataId: 94, clazz: ''},
						{name: '任三组三', dataId: 95, clazz: ''}, {name: '任三组六', dataId: 96, clazz: ''},
						{name: '任四复式', dataId: 97, clazz: ''}, {name: '任四单式', dataId: 98, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：从万位、千位、百位、十位、个位中选择两位各选1个号码组成一注。',
						 example: '投注方案：百位1，十位2  开奖号码：12123，即中任二直选',
						 help: '从万位、千位、百位、十位、个位中选择两位各选1个号码组成一注，所选号码与开奖号码指定位置上的号码相同，且顺序一致，即为中奖。'};
	gameDatas[9] = gameData;
	gameDatas[91] = gameData;

	gameData = {};
	gameData.gameName = 'anySelectGame';
	gameData.gameStarLength = 5;
	gameData.playBaseList = [{name: '任二复式', dataId: 91, clazz: ''}, {name: '任二单式', dataId: 92, clazz: 'on'},
						{name: '任三复式', dataId: 93, clazz: ''}, {name: '任三单式', dataId: 94, clazz: ''},
						{name: '任三组三', dataId: 95, clazz: ''}, {name: '任三组六', dataId: 96, clazz: ''},
						{name: '任四复式', dataId: 97, clazz: ''}, {name: '任四单式', dataId: 98, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：从万位、千位、百位、十位、个位中选择两个位置，至少手动输入一个两位数的号码组成一注。',
						 example: '投注方案：选择万位、个位，输入号码13。开奖号码：12223，即中任二直选。',
						 help: '从万位、千位、百位、十位、个位中选择两个位置。至少手动输入一个两位数的号码组成一注，所选号码与开奖号码的指定位置上的号码相同，且顺序一致，即为中奖。'};
	gameDatas[92] = gameData;

	gameData = {};
	gameData.gameName = 'anySelectGame';
	gameData.gameStarLength = 5;
	gameData.playBaseList = [{name: '任二复式', dataId: 91, clazz: ''}, {name: '任二单式', dataId: 92, clazz: ''},
						{name: '任三复式', dataId: 93, clazz: 'on'}, {name: '任三单式', dataId: 94, clazz: ''},
						{name: '任三组三', dataId: 95, clazz: ''}, {name: '任三组六', dataId: 96, clazz: ''},
						{name: '任四复式', dataId: 97, clazz: ''}, {name: '任四单式', dataId: 98, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：从万位、千位、百位、十位、个位中选择三位各选1个号码组成一注。',
						 example: '投注方案：万位选1，百位选2，个位选3。开奖号码：12223，即中任三直选。',
						 help: '从万位、千位、百位、十位、个位中选择三位各选1个号码组成一注，所选号码与开奖号码的指定位置上的号码相同，且顺序一致，即为中奖。'};
	gameDatas[93] = gameData;

	gameData = {};
	gameData.gameName = 'anySelectGame';
	gameData.gameStarLength = 5;
	gameData.playBaseList = [{name: '任二复式', dataId: 91, clazz: ''}, {name: '任二单式', dataId: 92, clazz: ''},
						{name: '任三复式', dataId: 93, clazz: ''}, {name: '任三单式', dataId: 94, clazz: 'on'},
						{name: '任三组三', dataId: 95, clazz: ''}, {name: '任三组六', dataId: 96, clazz: ''},
						{name: '任四复式', dataId: 97, clazz: ''}, {name: '任四单式', dataId: 98, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：从万位、千位、百位、十位、个位中选择三个位置，至少手动输入一个三位数的号码组成一注。',
						 example: '投注方案：选择万位、百位、个位，输入号码123。开奖号码：12223，即中任三直选。',
						 help: '从万位、千位、百位、十位、个位中选择三个位置。至少手动输入一个三位数的号码组成一注，所选号码与开奖号码的指定位置上的号码相同，且顺序一致，即为中奖。'};
	gameDatas[94] = gameData;

	gameData = {};
	gameData.gameName = 'anySelectGame';
	gameData.gameStarLength = 5;
	gameData.playBaseList = [{name: '任二复式', dataId: 91, clazz: ''}, {name: '任二单式', dataId: 92, clazz: ''},
						{name: '任三复式', dataId: 93, clazz: ''}, {name: '任三单式', dataId: 94, clazz: ''},
						{name: '任三组三', dataId: 95, clazz: 'on'}, {name: '任三组六', dataId: 96, clazz: ''},
						{name: '任四复式', dataId: 97, clazz: ''}, {name: '任四单式', dataId: 98, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：万位、千位、百位、十位、个位中选择三个位置，且从0-9中任意选择两个号码组成一注。',
						 example: '投注方案：选择位置千位、十位、个位，选择号码01，开奖号码：90501，即中任三组三。',
						 help: '万位、千位、百位、十位、个位中选择三个位置，且从0-9中任意选择两个号码组成一注。所选号码与开奖号码的指定位置上的号码相同，且顺序不限，即为中奖。'};
	gameDatas[95] = gameData;

	gameData = {};
	gameData.gameName = 'anySelectGame';
	gameData.gameStarLength = 5;
	gameData.playBaseList = [{name: '任二复式', dataId: 91, clazz: ''}, {name: '任二单式', dataId: 92, clazz: ''},
						{name: '任三复式', dataId: 93, clazz: ''}, {name: '任三单式', dataId: 94, clazz: ''},
						{name: '任三组三', dataId: 95, clazz: ''}, {name: '任三组六', dataId: 96, clazz: 'on'},
						{name: '任四复式', dataId: 97, clazz: ''}, {name: '任四单式', dataId: 98, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：万位、千位、百位、十位、个位中选择三个位置，且从0-9中任意选择三个号码组成一注。',
						 example: '投注方案：选择位置千位、十位、个位，选择号码135，开奖号码：95813，即中任三组六',
						 help: '万位、千位、百位、十位、个位中选择三个位置，且从0-9中任意选择三个号码组成一注。所选号码与开奖号码的指定位置上的号码相同，且顺序不限，即为中奖。'};
	gameDatas[96] = gameData;

	gameData = {};
	gameData.gameName = 'anySelectGame';
	gameData.gameStarLength = 5;
	gameData.playBaseList = [{name: '任二复式', dataId: 91, clazz: ''}, {name: '任二单式', dataId: 92, clazz: ''},
						{name: '任三复式', dataId: 93, clazz: ''}, {name: '任三单式', dataId: 94, clazz: ''},
						{name: '任三组三', dataId: 95, clazz: ''}, {name: '任三组六', dataId: 96, clazz: ''},
						{name: '任四复式', dataId: 97, clazz: 'on'}, {name: '任四单式', dataId: 98, clazz: ''}];
	gameData.playInfo = {info: '玩法说明：从万位、千位、百位、十位、个位中选择四位各选1个号码组成一注。',
						 example: '投注方案：万位选1，百位选2，十位选3，个位选4。开奖号码：12234，即中任四直选。',
						 help: '从万位、千位、百位、十位、个位中选择四位各选1个号码组成一注，所选号码与开奖号码的指定位置上的号码相同，且顺序一致，即为中奖。'};
	gameDatas[97] = gameData;

	gameData = {};
	gameData.gameName = 'anySelectGame';
	gameData.gameStarLength = 5;
	gameData.playBaseList = [{name: '任二复式', dataId: 91, clazz: ''}, {name: '任二单式', dataId: 92, clazz: ''},
						{name: '任三复式', dataId: 93, clazz: ''}, {name: '任三单式', dataId: 94, clazz: ''},
						{name: '任三组三', dataId: 95, clazz: ''}, {name: '任三组六', dataId: 96, clazz: ''},
						{name: '任四复式', dataId: 97, clazz: ''}, {name: '任四单式', dataId: 98, clazz: 'on'}];
	gameData.playInfo = {info: '玩法说明：从万位、千位、百位、十位、个位中选择四个位置，至少手动输入一个四位数的号码组成一注。',
						 example: '投注方案：万位选1，百位选2，十位选3，个位选4。开奖号码：12234，即中任四直选。1*345，则为中奖，如果是13*45或134*5等都不中奖。',
						 help: ' 从万位、千位、百位、十位、个位中选择四个位置。至少手动输入一个四位数的号码组成一注，所选号码与开奖号码的指定位置上的号码相同，且顺序一致，即为中奖。'};
	gameDatas[98] = gameData;



	global.gameDatas = gameDatas;


})(jQuery, window);