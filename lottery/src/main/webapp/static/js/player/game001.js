;(function($, global, undefined) {
	// $('#navDiv').find('a').each(function(i, element) {
	// 	var $element = $(element);
	// 	var index = $(element).attr('data-index');
	// 	if (1 === index) {
	// 		$element.addClass('on');
	// 	} else {
	// 		$element.removeClass('on');
	// 	}
	// });

	var eventFuctionList = [];

	$('#group_list').children('li').on('click', function(element) {
		var $this = $(this);
		$($this.children('a')[0]).addClass('on');
		var $siblings = $(this).siblings('li');
		for (var index = 0; index < $siblings.length; index++) {
			$($($siblings[index]).children('a')[0]).removeClass('on');
		}
		var $a = $this.children()[0];
		var dataId = $($a).attr('data-id');
		showDiffGames(+dataId);
	});


	function showDiffGames(dataId) {
		var gameData = global.gameDatas[+dataId];
		global.gameView.createGame(generateGameData(gameData), +dataId);
	}

	function generateGameData(data) {
		var gameData = {};
		gameData.gameName = data.gameName;
		gameData.gameStarLength = data.gameStarLength;
		gameData.playBaseList = data.playBaseList;
		gameData.playInfo = data.playInfo;
		eventFuctionList.splice(0, eventFuctionList.length);
		eventFuctionList.push(bindPlayInfoEvent);
		eventFuctionList.push(bindPlayListEvent);
		eventFuctionList.push(bindNumSelectEvent);
		eventFuctionList.push(bindClearTextArea);
		eventFuctionList.push(bindValidateInputContent);
		gameData.eventFuctionList = eventFuctionList;

		return gameData;
	}

	function bindPlayInfoEvent() {
		$('.play-info').find('a').hover(function() {
			if ($(this).attr('class') === 'btn btn-green') {
				$('#play-example').show();
			} else {
				$('#play-help').show();
			}
		}, function() {
			$('#play-example').hide();
			$('#play-help').hide();
		});
	}

	function bindPlayListEvent() {
		$('.play-list').children('a').on('click', function() {
			var dataId = $(this).attr('data-id');
			showDiffGames(+dataId);
		});
	}

	function bindNumSelectEvent() {
		$('#num-select').find('input').on('click', function() {
			if ($(this).val() === '清' || $(this).val() === '双' || $(this).val() === '单' ||
				$(this).val() === '小' || $(this).val() === '大' || $(this).val() === '全') {
				numberFastOpt(this);
			} else {
				var indexOfChecked = $(this).attr('class').split(' ').indexOf('checked');
				if (indexOfChecked >= 0) {
					$(this).removeClass('checked');
				} else {
					$(this).addClass('checked');
				}
			}
			global.gameCode.calculateResult();
		});
	}

	function bindClearTextArea() {
		$('#clear_num_func').on('dblclick', function() {
			$('#textarea-code').val('');
		});
	}

	function bindValidateInputContent() {
		$("#textarea-code").keyup(function () {
                $(this).val($(this).val().replace(/[^0-9\s]/g, ''));
            }).bind("paste", function () {  //CTR+V事件处理    
                $(this).val($(this).val().replace(/[^0-9\s]/g, ''));
            }).css("ime-mode", "disabled"); 
	}

	function numberFastOpt(_this) {
		var value = $(_this).val();
		var numButton = [];
		var optButton = [];
		var indexOfOn = $(_this).attr('class').split(' ').indexOf('on');
		if (indexOfOn < 0) {
			$(_this).addClass('on');
			numButton = $(_this).siblings('input.code');
			optButton = $(_this).siblings('input.action');
			for (var index = 0; index < numButton.length; index++) {
				$(numButton[index]).removeClass('checked');//value === '清'
				if (value === '双') {
					index % 2 === 0 && $(numButton[index]).addClass('checked');
				} else if (value === '单') {
					index % 2 === 1 && $(numButton[index]).addClass('checked');
				} else if (value === '小') {
					index < 5 && $(numButton[index]).addClass('checked');
				} else if (value === '大') {
					index >= 5 && $(numButton[index]).addClass('checked');
				} else if (value === '全') {
					$(numButton[index]).addClass('checked')
				}
			}
			for (index = 0; index < optButton.length; index++) {
				$(optButton[index]).removeClass('on');
			}
		} 
	}

	$('#play-mod').children('b').on('click', function() {
		var indexOfOn = $(this).attr('class').split(' ').indexOf('on');
		if (indexOfOn < 0) {
			$(this).addClass('on');
			var numB = $(this).siblings('b') || [];
			for (var index = 0; index < numB.length; index++) {
				$(numB[index]).removeClass('on');
			}
		}
		global.gameCode.calculateResult();
	});

	$('.icon-minus').on('click', function() {
		var multiples = +$('#beishu-value').val();
		if (multiples && multiples > 1) {
			$('#beishu-value').val(--multiples);
		}
	});

	$('.icon-plus').on('click', function() {
		var multiples = +$('#beishu-value').val();
		if (multiples) {
			$('#beishu-value').val(++multiples);
		}
	});

	$('#addGameCode').on('click', function() {
		var dataId = -1;
		$('.play-list').children('a').each(function(index, element) {
			if ($(element).attr('class') === 'on') {
				dataId = +$(element).attr('data-id');
				return false;
			}
		});
		if (dataId === 12
			|| dataId === 22 || dataId === 24
			|| dataId === 32 || dataId === 34 || dataId === 36
			|| dataId === 42 || dataId === 44 || dataId === 46
			|| dataId === 52 || dataId === 54) {
			global.gameCode.createInputTypeLotteryOrder(+dataId);
		} else {
			global.gameCode.createGeneralLotteryOrder();
		}
		global.gameCode.calculateResult();
	});

	$('#clearGameCode').on('click', function() {
		global.gameCode.clearGameCode();
		global.gameCode.calculateResult();
	});

	$('#btnPostBet').on('click', function() {
		global.gameCode.saveUserLotteryData(global.gameCode.gameCodes);
	});


	global.gameCode.getOptLeftTime();
	global.gameCode.getOpenLeftTime();
	global.gameCode.getLotterResultAndShow(global.gameCode.showLeftTime, global.gameCode.OPT_LEFT_TIME);

	(function() {
		var units = ['所有', '元', '角', '分'];
		var bases = [0, 1.0, 0.1, 0.01];
		var status = ['所有', '未开奖', '开奖中', '已派奖', '未中奖', '已撤单'];
		
		$.ajax({
			type: 'POST',
			url: window._commonURL + '/rest/lottery/queryLastestLotteryDetail',
			data: JSON.stringify({}),
			async: true,
			dataType: 'json',
			contentType: 'application/json;charset=UTF-8',
			success: function(response) {
				console.log(response);
				if (!response || response.status !== '00' || !response.lotteryBettingInfoList || !response.lotteryBettingInfoList.length ===0) {
					$('#my-bets div.none').show();
					$('#my-bets table').hide();
				} else {
					$('#my-bets div.none').hide();
					$('#my-bets table').show();
					var tbody = '<tbody>';
					if (response && response.status === '00') {
						if (response.lotteryBettingInfoList && response.lotteryBettingInfoList.length > 0) {
							var length = response.lotteryBettingInfoList.length;
							for (var index = 0; index < length; index++) {
								tbody += ('<tr data-index="' + index + '">'
									+ '<td>' + response.lotteryBettingInfoList[index].lotteryId + '</td>'
									+ '<td>' + response.lotteryBettingInfoList[index].lotteryType + '</td>'
									+ '<td>' + response.lotteryBettingInfoList[index].lotteryPlay + '</td>'
									+ '<td>' + response.lotteryBettingInfoList[index].lotteryPeriods + '</td>'
									+ '<td>' + response.lotteryBettingInfoList[index].lotteryBettingCode + '</td>'
									+ '<td>' + response.lotteryBettingInfoList[index].lotteryBettingNumber + ' 注</td>'
									+ '<td>' + (response.lotteryBettingInfoList[index].lotteryBettingStatus === '3' ? response.lotteryBettingInfoList[index].lotteryBettingPrice : -(response.lotteryBettingInfoList[index].lotteryBettingAmount * bases[response.lotteryBettingInfoList[index].lotteryBettingMode])) + ' 元</td>'
									+ '<td>' + response.lotteryBettingInfoList[index].lotteryBettingAmount * bases[response.lotteryBettingInfoList[index].lotteryBettingMode] + ' 元</td>'
									+ '<td>' + new Date(response.lotteryBettingInfoList[index].createdDate).format('yyyy-MM-dd hh:mm:ss') + '</td>'
									+ (response.lotteryBettingInfoList[index].lotteryBettingStatus === '1' ? '<td><button onclick=window.gameCode.cancelOrder(' + response.lotteryBettingInfoList[index].lotteryId + ')>撤单</button></td>' : response.lotteryBettingInfoList[index].lotteryBettingStatus === '2' ? '<td>开奖中</td>' : response.lotteryBettingInfoList[index].lotteryBettingStatus === '3' ? '<td>已中奖</td>' : response.lotteryBettingInfoList[index].lotteryBettingStatus === '4' ? '<td>未中奖</td>' : response.lotteryBettingInfoList[index].lotteryBettingStatus === '5' ? '<td>已撤单</td>' : '<td>你想咋样</td>'));
							}
						}
					}
					tbody += '</tbody>';

					$('.touzhutable').children('tbody').remove();
					$('.touzhutable').append(tbody);
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				console.log(XMLHttpRequest);
				console.log(textStatus);
				console.log(errorThrown);
			},
			complete: function(XMLHttpRequest, textStatus) {
				console.log(XMLHttpRequest),
				console.log(textStatus);
			}
		});	
	})();

})(jQuery, window);
