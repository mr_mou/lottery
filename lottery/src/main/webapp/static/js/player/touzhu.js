;$(function(){
	$('#navDiv').find('a').each(function(i, element) {
		var $element = $(element);
		var index = parseInt($(element).attr('data-index'));
		if (3 === index) {
			$element.addClass('on');
		} else {
			$element.removeClass('on');
		}
	});
	
	
	$("#state").find('.cs-placeholder').click(function(){
		$("#state").addClass("cs-active");
	});
	$("#unit").find('.cs-placeholder').click(function(){
		$("#unit").addClass("cs-active");
	});

	window.stateFun = function(e){
		var e=window.event||e;
		e = e.srcElement||e.target; 
		if(e.nodeName == 'SPAN'){
			var val = $(e).parent().attr('data-value');
			var text = $(e).text();
			$("#state").attr('data-index',val);
			$("#state").find('.cs-placeholder').text(text);
		};
		$("#state").removeClass('cs-active');
	};
	window.unitFun = function(e){
		var e=window.event||e;
		e = e.srcElement||e.target; 
		if(e.nodeName == 'SPAN'){
			var val = $(e).parent().attr('data-value');
			var text = $(e).text();
			$("#unit").attr('data-index',val);
			$("#unit").find('.cs-placeholder').text(text);
		};
		$("#unit").removeClass('cs-active');
	};
	
	$('#queryRecord').on('click', function() {
		var units = ['所有', '元', '角', '分'];
		var bases = [0, 1.0, 0.1, 0.01];
		var status = ['所有', '未开奖', '开奖中', '已派奖', '未中奖', '已撤单'];
		var lotteryType = $('#betteryType').children('option:selected').text().trim();
		var betteryId = $('#betId').val();
		var state = $('#state').children('span.cs-placeholder').text();
		var unit = $('#unit').children('span.cs-placeholder').text();
		var beginTime = $('#datetimepicker_fromTime').val() + ':00';
		var endTime = $('#datetimepicker_toTime').val() + ':59';
		lotteryType = '选择彩种' !== lotteryType ? lotteryType : undefined;
		betteryId = betteryId ? +betteryId : undefined;
		state = '所有状态' !== state ? (status.indexOf(state) + '') : undefined;
		unit = '所有' !== unit ? (units.indexOf(unit) + '') : undefined;

		if (beginTime > endTime) {
			window.alert('起期不能大于止期');
			return;
		}

		var data = {
				lotteryType: lotteryType,
				lotteryId: betteryId,
				lotteryBettingStatus: state,
				lotteryBettingMode: unit,
				createdDate: beginTime,
				updatedDate: endTime
			};

		$.ajax({
			type: 'POST',
			url: '../../rest/lottery/queryLotteryDetail',
			data: JSON.stringify(data),
			async: true,
			dataType: 'json',
			contentType: 'application/json;charset=UTF-8',
			success: function(response) {
				console.log(response);
				var tbody = '<tbody>';
				if (response && response.status === '00') {
					if (response.lotteryBettingInfoList && response.lotteryBettingInfoList.length > 0) {
						var length = response.lotteryBettingInfoList.length;
						var totalCount = response.totalCount;
						for (var index = 0; index < length; index++) {
							tbody += ('<tr data-index="' + index + '">'
								+ '<td>' + response.lotteryBettingInfoList[index].lotteryId + '</td>'
								+ '<td>' + response.lotteryBettingInfoList[index].lotteryType + '</td>'
								+ '<td>' + response.lotteryBettingInfoList[index].lotteryPlay + '</td>'
								+ '<td>' + response.lotteryBettingInfoList[index].lotteryPeriods + '</td>'
								+ '<td>' + response.lotteryBettingInfoList[index].lotteryBettingCode + '</td>'
								+ '<td>' + response.lotteryBettingInfoList[index].lotteryBettingNumber + ' 注</td>'
								+ '<td>' + (response.lotteryBettingInfoList[index].lotteryBettingStatus === '3' ? response.lotteryBettingInfoList[index].lotteryBettingPrice : -(response.lotteryBettingInfoList[index].lotteryBettingAmount * bases[response.lotteryBettingInfoList[index].lotteryBettingMode])) + ' 元</td>'
								+ '<td>' + response.lotteryBettingInfoList[index].lotteryBettingAmount * bases[response.lotteryBettingInfoList[index].lotteryBettingMode] + ' 元</td>'
								+ '<td>' + new Date(response.lotteryBettingInfoList[index].createdDate).format('yyyy-MM-dd hh:mm:ss') + '</td>'
								+ (response.lotteryBettingInfoList[index].lotteryBettingStatus === '1' ? '<td><button onclick=cancelOrder(' + response.lotteryBettingInfoList[index].lotteryId + ')>撤单</button></td>' : response.lotteryBettingInfoList[index].lotteryBettingStatus === '2' ? '<td>开奖中</td>' : response.lotteryBettingInfoList[index].lotteryBettingStatus === '3' ? '<td>已中奖</td>' : response.lotteryBettingInfoList[index].lotteryBettingStatus === '4' ? '<td>未中奖</td>' : response.lotteryBettingInfoList[index].lotteryBettingStatus === '5' ? '<td>已撤单</td>' : '<td>你想咋样</td>'));
						}
					}
				}
				tbody += '</tbody>';

				$('.touzhutable').children('tbody').remove();
				$('.touzhutable').append(tbody);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				console.log(XMLHttpRequest);
				console.log(textStatus);
				console.log(errorThrown);
			},
			complete: function(XMLHttpRequest, textStatus) {
				console.log(XMLHttpRequest),
				console.log(textStatus);
			}
		});	
	});

	window.cancelOrder = function(lotteryId) {
		var bettingInfo = {
			lotteryId: lotteryId
		}

		$.ajax({
			type: 'POST',
			url: '../../rest/lottery/cancelOrder',
			data: bettingInfo,
			dataType: 'json',
			// contentType: 'application/json; charset=UTF-8',
			async: true,
			success: function(response) {
				console.log(response);
				if (response && response.status === '00') {
					window.alert('撤单成功');
				} else {
					window.alert('撤单失败');
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				console.log(XMLHttpRequest);
				console.log(textStatus);
				console.log(errorThrown);
				window.alert('系统错误');
			},
			complete: function(XMLHttpRequest, textStatus) {
				console.log(XMLHttpRequest),
				console.log(textStatus);
			}
		});
	}
});



