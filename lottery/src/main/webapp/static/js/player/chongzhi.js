;$(function(){
	$('#opt').find('a').each(function(i, element) {
		var $element = $(element);
		var index = parseInt($(element).attr('data-index'));
		if (1 === index) {
			$element.addClass('on');
		} else {
			$element.removeClass('on');
		}
	});
	$(".dialogue-yes").click(function(){
		$("#dialogue").fadeOut();
	});
	$(".submit").click(function(){

	});
	$("#recharge-type").bind('click',function(){
		$("#bank-list").addClass('hide').slideToggle();
	});
	$("#chongzhi").click(function() {
		var money = $('#input-money').val();
		if (!money) {
			window.alert('请输入充值金额');
		} else if (parseInt(money) < 10 || parseInt(money) > 50000) {
			window.alert('充值金额在10-50000之间');
		} else {
			var remark = $('#remark').val();
			var inBankType = $('#bankName').text();
			var inBankCode = $('#bank-id').val();
			var optType = '01';
			var moneyInOut = {
				inBankType: $('#bankName').text(),
				inBankCode: $('#bank-id').val(),
				optType: '01',
				amount: money,
				remark: $('#remark').val(),
				state: '01'
			}
			$.ajax({
				type: 'POST',
				url: window._commonURL + '/rest/money/saveMoneyInOutInfo',
				data: moneyInOut,
				dataType: 'json',
				async: true,
				success: function(response) {
					console.log(response);
					if (response && response.status === '00') {
						window.alert('保存成功');
					} else {
						window.alert(response.errorMsg);
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					console.log('error function info ---> XMLHttpRequest: ' + XMLHttpRequest + ', textStatus: ' + textStatus + ', errorThrown: ' + errorThrown);
				},
				complete: function(XMLHttpRequest) {
					console.log('complete function info ---> XMLHttpRequest: ' +  XMLHttpRequest);
				}
			});
		}
	});

	$('#queryMoneyDetail').on('click', function() {
		$('#moneyDetailList tbody').remove();
		var beginDate = $('#datetimepicker_fromTime').val();
		var endDate = $('#datetimepicker_toTime').val();
		if (beginDate > endDate) {
			window.alert('起始时间大于结束时间');
			return false;
		}
		var param = {
			beginDate: beginDate + ':00',
			endDate: endDate + ':00'
		}
		$.ajax({
			type: 'POST',
			url: window._commonURL + '/rest/money/queryMoneyInList',
			data: JSON.stringify(param),
			dataType: 'json',
			contentType: 'application/json; charset=UTF-8',
			async: true,
			success: function(response) {
				console.log(response);
				var states = ['', '待审核', '充值成功', '取现成功', '拒绝充值', '拒绝取现'];
				if (response && response.status === '00') {
					var length = response.moneyInList && response.moneyInList.length || 0;
					var tbody = '<tbody>';
					for (var index = 0; index < length; index++) {
						var tr = '<tr><td>' + response.moneyInList[index].moneyInOutId + '</td>' +
								 '<td>' + (response.moneyInList[index].optType === '01' ? '充值' : '提取') + '</td>' +
								 '<td>' + response.moneyInList[index].inBankType + '</td>' +
								 '<td>' + response.moneyInList[index].amount + ' 元</td>' +
								 '<td>' + (response.moneyInList[index].remark || '') + '</td>' +
								 '<td>' + (new Date(response.moneyInList[index].createDate)) + '</td>' + 
								 '<td>' + states[+response.moneyInList[index].state] + '</td></tr>';
						tbody += tr;
					}
					tbody += '</tbody>';
					$('#moneyDetailList').append(tbody);
				} else {
					window.alert(response.errorMsg);
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				console.log('error function info ---> XMLHttpRequest: ' + XMLHttpRequest + ', textStatus: ' + textStatus + ', errorThrown: ' + errorThrown);
			},
			complete: function(XMLHttpRequest) {
				console.log('complete function info ---> XMLHttpRequest: ' +  XMLHttpRequest);
			}
		});
	});

});
 function choseBankFun(e){
 	var e=window.event||e;
	e = e.srcElement||e.target; 
	if(e.nodeName == 'IMG'){
		var val = $(e).attr('data-id');
		clearActive();
		$(e).addClass("active");
		$("#bankName").text($(e).attr('title'));
		$("#bank-id").val(val);
		$("#bank-list").addClass('hide').slideUp();
	};
 };
function clearActive(){
	$("#bank-list").find('img').each(function(i,element){
		$(element).removeClass('active');
	})
}