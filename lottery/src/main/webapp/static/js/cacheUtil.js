;(function($, global, undefined) {
	var CacheUtil = function() {

	}

	CacheUtil.prototype.setCookie = function(name, value) {
		var Days = 30;
		var exp = new Date();
		exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
		document.cookie = name + "="+ escape(value) + ";expires=" + exp.toGMTString();
	}

	CacheUtil.prototype.getCookie = function(name) {
		var arr;
		var reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
		return (arr = document.cookie.match(reg)) && unescape(arr[2]) || null;
	}

	CacheUtil.prototype.deleteCookie = function(name) {
		var exp = new Date();
		exp.setTime(exp.getTime() - 1);
		var cval = getCookie(name);
		if (cval) {
			document.cookie = name + "="+ cval + ";expires=" + exp.toGMTString();	
		}
	}

	CacheUtil.prototype.clearCookie = function() {
		var keys = document.cookie.match(/[^ =;]+(?=\=)/g); 
		if (keys) { 
			for (var i = keys.length; i--;) {
				document.cookie = keys[i] + '=0;expires=' + new Date(0).toUTCString();
			}
		}
	}

	global.cacheUtil = new CacheUtil();
})(jQuery, window);