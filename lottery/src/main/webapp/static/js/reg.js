$(function(){
	// 加载动画
	var reg = $('#reg');
	var height = $(window).height();
	if (height > 560) {
		var ph = (height - 560) / 2;
		reg.animate({top: ph + 'px'}, 500);
	} else {
		reg.css('margin-bottom', '30px').animate({top: '30px'}, 500);
	}
	// 表单处理
	var lid = $('#lid');
	var username = $('#username');
	var password = $('#password');
	var password_repeat = $('#password_repeat');
	var qq = $('#qq');
	var submit = $('#submit');
	var error_value = $('#error_value');
	var requesting = false;
	var input_event = function(dom) {
		dom.bind('focus', function() {
			var p = $(this).parent();
			p.addClass('focus');
			if (p.hasClass('has_tip')) p.next().slideDown();
		});
		dom.bind('blur', function() {
			var p = $(this).parent();
			p.removeClass('focus');
			if (p.hasClass('has_tip')) p.next().slideUp();
		});
	};
	input_event(username);
	input_event(password);
	input_event(password_repeat);
	input_event(qq);
	document.onkeydown = function(e) {
		var ev = document.all ? window.event : e;
		if (ev.keyCode == 13) submit.trigger('click');
	}
	submit.bind('click', function() {
		var t = $(this).find('span');
		var error_tip = function(val) {
			error_value.text(val).parent().slideDown();
			setTimeout(function() {
				error_value.parent().slideUp(function() {
					t.fadeOut(function() {
						$(this).text('提交注册').fadeIn(function() {
							requesting = false;
						});
					});
				});
			}, 2000);
		};
		if (requesting) return false;
		requesting = true;
		t.fadeOut(function() {
			$(this).text('提交中...').fadeIn(function() {
				$.ajax({
					url: '/lottery/rest/user/checkName',
					type: 'POST',
					dataType: 'json',
					data: {userName:username.val()},
					error: function() {
						error_tip('请求失败，请重试');
					},
					success: function(res) {
						var res = JSON.parse(res);
						var code = res.status;
						if(code == '00'){
							$.ajax({
								url: window.location.href,
								type: 'POST',
								cache: false,
								dataType: 'json',
								data: {lid: lid.val(), userName: username.val(), password: $.md5(password.val()), password_repeat: $.md5(password_repeat.val()), qq: qq.val()},
								error: function() {
									error_tip('请求失败，请重试');
								},
								success: function(res) {
									var res = JSON.parse(res);
									var code = res.status;
									var msg = res.errorMsg;
									if (code == '00') {
										error_value.parent().addClass('success');
										error_tip('注册成功');
										setTimeout(function() {
											reg.fadeOut(function() {
												window.location.href = $('#login').attr('href');
											});
										}, 2000);
										
									}else{
										error_tip(decodeURIComponent(msg));
									}
								},
							});
						}else{
							error_tip('用户名已存在');
							return;
						}
					},
				})
				
			});
		});
	});
});