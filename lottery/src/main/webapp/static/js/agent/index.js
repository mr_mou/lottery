;(function($, global, undefined) {
	$('#navDiv').find('a').each(function(i, element) {
		var $element = $(element);
		var index = parseInt($(element).attr('data-index'));
		if (1 === index) {
			$element.addClass('on');
		} else {
			$element.removeClass('on');
		}
	});

	(function() {
		$.ajax({
			type: 'POST',
			url: window._commonURL + '/rest/userMgrForAgent/queryAllSubUsersAndAgents',
			data: {},
			dataType: 'json',
			async: true,
			success: function(response) {
				console.log(response);
				var subAgents = 0;
				var subPlayers = 0;
				if (response && response.status === '00') {
					var length = response.allSubUsersAndAgents && response.allSubUsersAndAgents.length || 0;
					for (var index = 0; index < length; index++) {
						if ('player' === response.allSubUsersAndAgents[index].role) {
							subPlayers++;
						} else if ('agent' === response.allSubUsersAndAgents[index].role) {
							subAgents++;
						}
					}
				}
				$('#subAgents').text(subAgents);
				$('#subPlayers').text(subPlayers);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				console.log(XMLHttpRequest);
				console.log(textStatus);
				console.log(errorThrown);
			},
			complete: function(XMLHttpRequest, textStatus) {
				console.log(XMLHttpRequest),
				console.log(textStatus);
			}
		});

		var units = ['所有', '元', '角', '分'];
		var bases = [0, 1.0, 0.1, 0.01];
		var status = ['所有', '未开奖', '开奖中', '已派奖', '未中奖', '已撤单'];
		
		$.ajax({
			type: 'POST',
			url: window._commonURL + '/rest/lottery/queryLastestLotteryDetail',
			data: JSON.stringify({}),
			async: true,
			dataType: 'json',
			contentType: 'application/json;charset=UTF-8',
			success: function(response) {
				console.log(response);
				var tbody = '<tbody>';
				if (response && response.status === '00') {
					if (response.lotteryBettingInfoList && response.lotteryBettingInfoList.length > 0) {
						var length = response.lotteryBettingInfoList.length;
						for (var index = 0; index < length; index++) {
							tbody += ('<tr data-index="' + index + '">'
								+ '<td>' + response.lotteryBettingInfoList[index].lotteryId + '</td>'
								+ '<td>' + response.lotteryBettingInfoList[index].userName + '</td>'
								+ '<td>' + response.lotteryBettingInfoList[index].lotteryType + '</td>'
								+ '<td>' + response.lotteryBettingInfoList[index].lotteryPlay + '</td>'
								+ '<td>' + response.lotteryBettingInfoList[index].lotteryPeriods + '</td>'
								+ '<td>' + response.lotteryBettingInfoList[index].lotteryBettingCode + '</td>'
								+ '<td>' + response.lotteryBettingInfoList[index].lotteryBettingNumber + ' 注</td>'
								+ '<td>' + (response.lotteryBettingInfoList[index].lotteryBettingStatus === '3' ? response.lotteryBettingInfoList[index].lotteryBettingPrice : -(response.lotteryBettingInfoList[index].lotteryBettingAmount * bases[response.lotteryBettingInfoList[index].lotteryBettingMode])) + ' 元</td>'
								+ '<td>' + response.lotteryBettingInfoList[index].lotteryBettingAmount * bases[response.lotteryBettingInfoList[index].lotteryBettingMode] + ' 元</td>'
								+ '<td>' + new Date(response.lotteryBettingInfoList[index].createdDate).format('yyyy-MM-dd hh:mm:ss') + '</td>');
						}
					}
				}
				tbody += '</tbody>';

				$('.touzhutable').children('tbody').remove();
				$('.touzhutable').append(tbody);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				console.log(XMLHttpRequest);
				console.log(textStatus);
				console.log(errorThrown);
			},
			complete: function(XMLHttpRequest, textStatus) {
				console.log(XMLHttpRequest),
				console.log(textStatus);
			}
		});	

	})();
	
})(jQuery, window);