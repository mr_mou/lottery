;$(function(){
	var time = new Date();
	var hour = time.getHours();
	$('#optDiv').find('a').each(function(i, element) {
		var $element = $(element);
		var index = parseInt($(element).attr('data-index'));
		if (12 === index) {
			$element.addClass('on');
		} else {
			$element.removeClass('on');
		}
	});
	$("#cash-type").click(function(){
		window.location.href = 'set.jsp';
	});
	$("#tixian").click(function() {
		var totalT = $('#totalT').text();
		var haveT = $('#haveT').text();
		var T = totalT - haveT;
		if (hour < 11 || hour > 23) {
			window.alert('不好意思，此时段不能提现');
		} else if( T == 0 ) {
			window.alert('不好意思，已超过提现次数');
		} else if (!$("#input-money").val()) {
			window.alert('请输入提取金额');
		} else if (parseInt($("#input-money").val()) < 100) {
			window.alert('不好意思，提取金额不能少于100');
		} else if (!$("#input-password").val()) {
			window.alert('请输入提取资金密码');
		} else {
			var money = $('#input-money').val();
			var param = {
				amount: $('#input-money').val(),
				password: $.md5($('#input-password').val()),
				remark: $('#remark').val(),
				optType: '02',
				state: '01'
			}
			$.ajax({
				type: 'POST',
				url: window._commonURL + '/rest/money/saveMoneyInOutInfo',
				data: param,
				dataType: 'json',
				async: true,
				success: function(response) {
					console.log(response);
					if (response && response.status === '00') {
						window.alert('保存成功');
						$('#input-money').val('');
						$('#input-password').val('');
						$('#remark').val('');
					} else {
						window.alert(response.errorMsg);
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					console.log('error function info ---> XMLHttpRequest: ' + XMLHttpRequest + ', textStatus: ' + textStatus + ', errorThrown: ' + errorThrown);
				},
				complete: function(XMLHttpRequest) {
					console.log('complete function info ---> XMLHttpRequest: ' +  XMLHttpRequest);
				}
			});
		}
	});


	$('#queryOutRecord').on('click', function() {
		$('#touzhutable tbody').remove();
		var beginDate = $('#datetimepicker_fromTime').val();
		var endDate = $('#datetimepicker_toTime').val();
		if (beginDate > endDate) {
			window.alert('起始时间大于结束时间');
			return false;
		}
		var param = {
			beginDate: beginDate + ':00',
			endDate: endDate + ':00'
		}
		$.ajax({
			type: 'POST',
			url: window._commonURL + '/rest/money/queryMoneyOutList',
			data: JSON.stringify(param),
			dataType: 'json',
			contentType: 'application/json; charset=UTF-8',
			async: true,
			success: function(response) {
				console.log(response);
				var states = ['', '待审核', '充值成功', '取现成功', '拒绝充值', '拒绝取现'];
				if (response && response.status === '00') {
					var length = response.moneyInList && response.moneyInList.length || 0;
					var tbody = '<tbody>';
					for (var index = 0; index < length; index++) {
						var tr = '<tr><td>' + response.moneyInList[index].moneyInOutId + '</td>' +
								 '<td>' + (response.moneyInList[index].optType === '01' ? '充值' : '提取') + '</td>' +
								 '<td>' + response.moneyInList[index].amount + ' 元</td>' +
								 '<td>' + (response.moneyInList[index].remark || '') + '</td>' +
								 '<td>' + (new Date(response.moneyInList[index].createDate)) + '</td>' + 
								 '<td>' + states[+response.moneyInList[index].state] + '</td></tr>';
						tbody += tr;
					}
					tbody += '</tbody>';
					$('#touzhutable').append(tbody);
				} else {
					window.alert(response.errorMsg);
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				console.log('error function info ---> XMLHttpRequest: ' + XMLHttpRequest + ', textStatus: ' + textStatus + ', errorThrown: ' + errorThrown);
			},
			complete: function(XMLHttpRequest) {
				console.log('complete function info ---> XMLHttpRequest: ' +  XMLHttpRequest);
			}
		});
	});
});