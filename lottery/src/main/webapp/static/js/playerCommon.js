$(function() {
	// 加载动画
	$.inited = false;
	$.dom_body = $('#dom_body');
	$.dom_body.css('min-height', window.screen.height).fadeIn();
	
	// 左侧菜单
	var game_nav = $('#game-nav');
	game_nav.find('.game-name').bind('click', function() {
		if ($(this).hasClass('icon-bookmark-empty')) {
			$(this).removeClass('icon-bookmark-empty').addClass('icon-bookmark').next().slideUp();
		}else{
			game_nav.find('.icon-bookmark-empty').removeClass('icon-bookmark-empty').addClass('icon-bookmark').next().slideUp();
			$(this).removeClass('icon-bookmark').addClass('icon-bookmark-empty').next().slideDown();
		}
	});
	//退出
	$("#layout").bind('click',function(){
		var layout = confirm('确定要退出吗？')
		if(layout){
			window.location.href = '/lottery/rest/logout';
			window.sessionStorage.removeItem('userCode');
		}else{
			return;
		}
	});
	$("#nt-close").click(function(){
		$(this).parent().fadeOut();
	});
	var ajaxFun = {
		ajax:function(url,data,callBack){
			$.ajax({
				dataType: 'json',
				type: 'POST',
				url: url,
				data: data,
				success: callBack,
				error:function(XMLHttpRequest, textStatus, errorThrown){
					alert('系统异常，请稍后再试');
					console.log('error function info ---> XMLHttpRequest: ' + XMLHttpRequest + ', textStatus: ' + textStatus + ', errorThrown: ' + errorThrown);
				},
				complete: function(XMLHttpRequest) {
					console.log('complete function info ---> XMLHttpRequest: ' +  XMLHttpRequest);
				}
			});
		}
	};
	window.ajax = ajaxFun;

	var url = '/lottery/rest/user/info';
	var data ={};
	window.ajax.ajax(url,data,userInfoFun);
	function userInfoFun(res){
		var code = res.status;
		var msg = res.errorMsg;
		if(code == '00'){
			$("#user-Name").text(res.userName);
			window.sessionStorage.setItem('userCode',res.userCode);
			$("#yu-e").text(res.amount);
		}
	};


	Date.prototype.format = function(fmt) {
		var o = {   
		    'M+': this.getMonth() + 1,                 //月份   
		    'd+': this.getDate(),                    //日   
		    'h+': this.getHours(),                   //小时   
		    'm+': this.getMinutes(),                 //分   
		    's+': this.getSeconds(),                 //秒   
		    'q+': Math.floor((this.getMonth() + 3) / 3), //季度   
		    'S': this.getMilliseconds()             //毫秒   
		};   
		if(/(y+)/.test(fmt)) {
			fmt = fmt.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length));
		}   
		       
		for(var k in o) {
			if(new RegExp('(' + k + ')').test(fmt)) {
				fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00'+ o[k]).substr(('' + o[k]).length)));   
			}
		}
		return fmt;   
	}
 	var getUrl = function(){};
 	getUrl.prototype.geturlset = function(name){
		var reg = new RegExp('(^|&)'+ name + '=([^&]*)(&|$)');
		var r = window.location.search.substr(1).match(reg);
		if (r != null)
			return unescape(r[2]);
		 return '';
	};
	window.getUrl = new getUrl();
});
