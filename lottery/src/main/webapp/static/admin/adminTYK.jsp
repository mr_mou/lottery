<%@ page pageEncoding="utf-8"%>
<%@include file = '../layout/adminCommon.jsp'%>
			<div id="agent-money-dom" class="common">
				<div class="head">
					<div class="name icon-yen">盈亏统计</div>
					<div class="search"  func="form_submit">
						<div class="select-box mode">
							
						<div class="cs-select mode" tabindex="0" id="state"><span class="cs-placeholder">所有成员</span><div class="cs-options"><ul onclick="stateFun()"><li data-option="" data-value="0"><span>所有成员</span></li><li data-option="" data-value="1"><span>直属下级</span></li><li data-option="" data-value="2"><span>所有下级</span></li></ul></div><select name="a_type" class="cs-select mode">
								<option value="0" selected="">所有成员</option>
								<option value="1">直属下级</option>
								<option value="2">所有下级</option>
							</select></div></div>
						<input type="text" name="username" value="" id="username" class="input" style="width:100px" onfocus="if(this.value==='用户名') this.value='';" onblur="if (this.value==='') this.value='用户名';">
						<div class="timer">
							<input type="text" autocomplete="off" name="fromTime" value="2016-12-12 16:28" id="datetimepicker_fromTime" class="timer Wdate" onclick="WdatePicker()">
							<span class="icon icon-calendar"></span>
						</div>
						<div class="sep icon-exchange"></div>
						<div class="timer">
							<input type="text" autocomplete="off" name="toTime" value="2016-12-19 16:30" id="datetimepicker_toTime" class="timer Wdate" onclick="WdatePicker()">
							<span class="icon icon-calendar"></span>
						</div>
						<button type="submit" class="btn btn-brown icon-search">查询</button>
					</div>
				</div>
				<div class="body">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tbody>
							<tr class="title">
								<td>用户名</td>
								<td>总收入</td>
								<td>总支出</td>
								<td>总结余</td>
								<td>查看</td>
							</tr>
							<tr>
								<td>atoz004</td>
								<td>0.000</td>
								<td>0.000</td>
								<td>0.000</td>
								<td>
									<a href="/agent/money?parentId=1200&amp;fromTime=2016-12-12 16:28&amp;toTime=2016-12-19 16:30" container="#agent-money-dom .body" data-ispage="true" target="ajax" func="loadpage" class="icon-download">下级</a>
									<a href="/agent/money?uid=1200&amp;fromTime=2016-12-12 16:28&amp;toTime=2016-12-19 16:30" container="#agent-money-dom .body" data-ispage="true" target="ajax" func="loadpage" class="icon-upload">上级</a>
								</td>
							</tr>
							<tr>
								<td>本页总结</td>
								<td>0.000</td>
								<td>0.000</td>
								<td>0.000</td>
								<td>--</td>
							</tr>
							<tr>
								<td>团队总结</td>
								<td>0.000</td>
								<td>0.000</td>
								<td>0.000</td>
								<td>--</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<%@include file = '../layout/systemTip.jsp'%>
</body>
<script src="../js/admin/adminTYK.js"></script>
</html>