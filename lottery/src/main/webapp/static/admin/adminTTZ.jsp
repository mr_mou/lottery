<%@ page pageEncoding="utf-8"%>
<%@include file = '../layout/adminCommon.jsp'%>
			<div id="agent-log-dom" class="common">
				<div class="head">
					<div class="name icon-th-list">团队记录</div>
					<div  class="search" data-ispage="true" container="#agent-log-dom .body" target="ajax" func="form_submit">
						<select name="type" id="betteryType">
							<option>选择彩种</option>
							<option disabled="">时时彩</option>
							<option value="1">&nbsp;&nbsp;&nbsp;|--重庆时时彩</option>
							<option value="14">&nbsp;&nbsp;&nbsp;|--乐彩五分彩</option>
							<option value="26">&nbsp;&nbsp;&nbsp;|--乐彩两分彩</option>
							<option value="5">&nbsp;&nbsp;&nbsp;|--乐彩分分彩</option>
							<option disabled="">11选5</option>
							<option value="7">&nbsp;&nbsp;&nbsp;|--山东11选5</option>
							<option value="6">&nbsp;&nbsp;&nbsp;|--广东11选5</option>
							<option value="16">&nbsp;&nbsp;&nbsp;|--江西11选5</option>
							<option disabled="">快三</option>
							<option disabled="">低频彩</option>
							<option value="9">&nbsp;&nbsp;&nbsp;|--福彩3D</option>
							<option value="10">&nbsp;&nbsp;&nbsp;|--排列三</option>
							<option disabled="">PK10</option>
							<option disabled="">快乐8</option>
							<option disabled="">快乐十分</option>
						</select>
						<div class="select-box">
							<div class="cs-select" tabindex="0" id="state">
								<span class="cs-placeholder">所有状态</span>
								<div class="cs-options">
									<ul onclick="stateFun()">
										<li data-option="" data-value="0"><span>所有状态</span>
										</li>
										<li data-option="" data-value="1"><span>已派奖</span></li><li data-option="" data-value="2"><span>未中奖</span></li><li data-option="" data-value="3"><span>未开奖</span></li><li data-option="" data-value="4"><span>追号</span></li>
										<li data-option="" data-value="5"><span>合买跟单</span>
										</li>
										<li data-option="" data-value="6"><span>撤单</span></li>
									</ul>
								</div>
								<select name="state" class="cs-select">
									<option value="0" selected="">所有状态</option>
									<option value="1">已派奖</option>
									<option value="2">未中奖</option>
									<option value="3">未开奖</option>
									<option value="4">追号</option>
									<option value="5">合买跟单</option>
									<option value="6">撤单</option>
								</select>
							</div>
						</div>
						<div class="select-box mode">
							<div class="cs-select mode" tabindex="0" id="model">
								<span class="cs-placeholder">所有成员</span>
								<div class="cs-options">
									<ul onclick="modelFun()">
										<li data-option="" data-value="0"><span>所有成员</span>
										</li>
										<li data-option="" data-value="1"><span>直属下级</span>
										</li>
										<li data-option="" data-value="2"><span>所有下级</span>
										</li>
									</ul>
								</div>
								<select name="a_type" class="cs-select mode">
									<option value="0" selected="">所有成员</option>
									<option value="1">直属下级</option>
									<option value="2">所有下级</option>
								</select>
							</div>
						</div>
						<input type="text" name="username" value="" id="username" class="input" style="width:54px" onfocus="if(this.value==='用户名') this.value='';" onblur="if (this.value==='') this.value='用户名';">
						<div class="timer">
							<input type="text" autocomplete="off" name="fromTime" value="2016-12-13 10:05" id="datetimepicker_fromTime" class="timer Wdate" onclick="WdatePicker()">
							<span class="icon icon-calendar"></span>
						</div>
						<div class="sep icon-exchange"></div>
						<div class="timer">
							<input type="text" autocomplete="off" name="toTime" value="2016-12-20 10:07" id="datetimepicker_toTime" class="timer Wdate" onclick="WdatePicker()">
							<span class="icon icon-calendar"></span>
						</div>
						<button type="submit" id="queryRecord" class="btn btn-brown icon-search">查询</button>
					</div>
				</div>
				<div class="body">
					<div class="empty">
						<table  cellspacing="0" cellpadding="0" class="touzhutable">
							<thead>
								<th>订单编号</th>
								<th>彩种</th>
								<th>玩法</th>
								<th>期号</th>
								<th>选号</th>
								<th>投注数量</th>
								<th>盈亏</th>
								<th>投注金额</th>
								<th>投注时间</th>
								<th>状态</th>
							</thead>
						</table>
						<div id="Pagination" class="pagination page"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@include file = '../layout/systemTip.jsp'%>
</body>
<script src="../js/admin/adminTTZ.js"></script>
</html>