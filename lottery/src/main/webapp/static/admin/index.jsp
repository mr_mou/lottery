<%@ page pageEncoding="utf-8"%>
<%@include file = '../layout/adminCommon.jsp'%>
			<div id="home" class="common">
				<div class="head">
					<div class="name icon-sitemap">平台总体状况</div>
				</div>
				<div class="bonus_data" style="height:450px;line-height: 56px;overflow: hidden;">
					<div id="yt_bonu" class="yt">
						<div class="over">
							<p class="p">平台统计</p>
						</div>
						<div class="adminNum">
							<table class="table">
								<tbody>
									<tr>
										<td>平台代理数量</td>
										<td>50</td>
										<td>总销量</td>
										<td>600</td>
										<td>总充值</td>
										<td>500</td>
										<td>总返点</td>
										<td>500</td>
										<td>总余额</td>
										<td style="border-right:none;">50026</td>
									</tr>
									<tr>
										<td>平台玩家数量</td>
										<td>500</td>
										<td>总返奖</td>
										<td>500</td>
										<td>总提款</td>
										<td>500</td>
										<td>总分红</td>
										<td>500</td>
										<td>总盈亏</td>
										<td style="border-right:none;">500</td>
									</tr>
								</tbody>
							</table>
						</div>					
					</div>
					<div id="yt_bonu2" class="yt">
						<div class="over">
							<p class="p">今日统计</p>
						</div>
						<div class="adminNum">
							<table class="table">
								<tbody>
									<tr>
										<td>今日新增代理</td>
										<td>500</td>
										<td>今日销量</td>
										<td>500</td>
										<td>今日返奖金额</td>
										<td>500</td>
										<td>今日充值</td>
										<td style="border-right:none;">500</td>
									</tr>
									<tr>
										<td>今日新增玩家</td>
										<td>500</td>
										<td>今日订单</td>
										<td>500</td>
										<td>今日中奖单</td>
										<td>500</td>
										<td>今日提款</td>
										<td style="border-right:none;">500</td>
									</tr>
								</tbody>
							</table>
						</div>					
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@include file = '../layout/systemTip.jsp'%>
</body>
<script src="<%=commonURL%>/static/js/admin/adminIndex.js"></script>
</html>