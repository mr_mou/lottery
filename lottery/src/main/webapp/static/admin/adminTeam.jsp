<%@ page pageEncoding="utf-8"%>
<%@include file = '../layout/adminCommon.jsp'%>
			<div id="agent-member-dom" class="common">
				<div class="head">
					<div class="name icon-address-book">会员管理</div>
					<div class="search" data-ispage="true" container="#agent-member-dom .body" target="ajax" func="form_submit">
						<div class="select-box mode">
							<div class="cs-select mode" tabindex="0" id="state">
								<span class="cs-placeholder">所有成员</span>
								<div class="cs-options">
									<ul onclick="stateFun()">
										<li data-option="" data-value="0">
											<span>所有成员</span>
										</li>
										<li data-option="" data-value="1">
											<span>直属下级</span>
										</li>
										<li data-option="" data-value="2">
											<span>所有下级</span>
										</li>
									</ul>
								</div>
								<select name="type" class="cs-select mode">
									<option value="0" selected="">所有成员</option>
									<option value="1">直属下级</option>
									<option value="2">所有下级</option>
								</select>
							</div>
						</div>
						<div class="select-box state">
							<div class="cs-select state" tabindex="0" id="model">
								<span class="cs-placeholder">状态</span>
								<div class="cs-options">
									<ul onclick="modelFun()">
										<li data-option="" data-value="-1">
											<span>所有</span>
										</li>
										<li data-option="" data-value="0">
											<span>在线</span>
										</li>
										<li data-option="" data-value="1">
											<span>离线</span>
										</li>
									</ul>
								</div>
								<select name="online" class="cs-select state">
									<option selected="">状态</option>
									<option value="0">在线</option>
									<option value="1">离线</option>
								</select>
							</div>
						</div>
						<input type="text" name="username" value="用户名" class="input" style="width:100px" onfocus="if(this.value==='用户名') this.value='';" onblur="if (this.value==='') this.value='用户名';">
						<button type="submit" class="btn btn-brown icon-search">查询</button>
					</div>
					<a href="javascript:void(0);" class="member_add btn btn-green icon-plus" id="addPerple">添加会员</a>
				</div>
				<div class="member_add_box" target="ajax" func="form_submit" id="addDiv" style="display:none;height: auto;">
					<div class="item">
						<div class="name">用户名</div>
						<div class="value fandian">
							<input type="text" name="username" id="username" required="" title="用户名" placeholder="请输入用户名" style="width:300px">
						</div>
					</div>
					<div class="item">
						<div class="name">登录密码</div>
						<div class="value fandian">
							<input type="text" name="password" id="password" required="" title="登录密码" placeholder="请输入登录密码" style="width:300px">
						</div>
					</div>
					<div class="item">
						<div class="name">腾讯QQ</div>
						<div class="value fandian">
							<input type="text" name="qq" id="qq" required="" title="腾讯QQ" placeholder="请输入腾讯QQ" style="width:300px">
						</div>
					</div>
					<div class="item">
						<div class="name">手机号码</div>
						<div class="value fandian">
							<input type="text" name="telphone" id="telphone" required="" title="手机号码" placeholder="请输入手机号码" style="width:300px" maxlength="11">
						</div>
					</div>
					<div class="item">
						<div class="name">会员类型</div>
						<div class="value type">
							<label><input type="radio" name="mantype" value="1" title="代理" checked="checked">代理</label>
							<label><input name="mantype" type="radio" value="0" title="会员">会员</label>
						</div>
					</div>
					<div id="fandian">
						<div class="item">
							<div class="name">销量佣金</div>
							<div class="value fandian">
								<input type="text" name="fanDian" id="xiaoliang" required="" title="销量佣金" placeholder="返点最大值" style="width:90px">
							</div>
							<div class="addon name">%，设置上限：0.9</div>
						</div>
						<div class="item">
							<div class="name">盈利分红</div>
							<div class="value fandian">
								<input type="text" name="fanDian" id="fenhong" required="" title="盈利分红" placeholder="返点最大值" style="width:90px">
							</div>
							<div class="addon name">%，设置上限：0.9</div>
						</div>
					</div>
					<button type="submit" class="btn btn-blue icon-ok" id="sureAdd">确认添加</button>
				</div>
				<div class="body1" style="padding: 8px;">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tbody>
						<tr class="title">
							<td>用户名</td>
							<td>用户类型</td>
							<td>返点</td>
							<td>余额</td>
							<td>状态</td>
							<td>在线</td>
							<td>注册时间</td>
							<td>操作</td>
						</tr>
						<tr>
							<td>atoz002</td>
							<td>会员</td>
							<td>0.3%</td>
							<td>0.000</td>
							<td>正常</td>
							<td><span class="green">在线</span></td>
							<td>2016-10-17</td>
							<td style="text-align:left;padding:10px 15px">
								<a class="icon-download" href="<%=commonURL%>/static/admin/adminTeam.jsp?username=atoz004" target="_self" func="loadpage">下级</a>
								<a class="icon-sweden" href="<%=commonURL%>/static/admin/adminTTZ.jsp?username=atoz004" target="_self" func="loadpage">投注</a>
								<a class="icon-yen YK" href="<%=commonURL%>/static/admin/adminTYK.jsp?username=atoz004" target="_self" func="loadpage">盈亏</a>
								<a class="icon-chart-bar" href="<%=commonURL%>/static/admin/adminTZB.jsp?username=atoz004" target="_self" func="loadpage">帐变</a>
								<div style="margin-top:5px">
									<a class="icon-mail" href="<%=commonURL%>/static/admin/adminSixin.jsp?username=atoz004" target="_self" func="loadpage">私信</a>
									<a class="icon-exchange" href="javascript:void(0)"  target="ajax" func="loadpage">转账</a>			<a href="javascript:void(0)" class="icon-pencil-squared"  target="ajax" func="loadpage">改返点</a>
								</div>
							</td>
						</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

	</div>
	<div id="ZZ" class="dialogue mid" style="display: none;">
		<div class="dialogue-warp" style="margin-top: -90px;">
			<div class="dialogue-head">
				<span class="dialogue-title icon-lamp">系统提示</span>
			</div>
			<div class="dialogue-body" style="min-height: 0px;"><div target="ajax" func="form_submit" style="height:40px"><input type="hidden" name="uid" value="1200"><input type="text" name="money" placeholder="请输入您需要转账的数额" style="float:left;width:400px;padding:10px 15px"><button type="submit" class="btn btn-blue" style="float:left;margin-left:15px;width:120px;height:39px;line-height:35px;font-size:14px">转账</button></div></div>
			<div class="dialogue-foot" style="display: block;">
				<div class="dialogue-auto" style="display: none;">
					<span class="dialogue-sec">9</span>秒后自动关闭
				</div>
				<div class="right">
					<button class="dialogue-yes btn btn-blue icon-ok" style="display: none;">确定</button>
					<button class="dialogue-no btn btn-white icon-undo QXZZ" style="display: inline-block;">取消转账</button>
				</div>
			</div>
		</div>
	</div>
	<div id="GD" class="dialogue mid" style="display: none;">
		<div class="dialogue-warp" style="margin-top: -107px;">
			<div class="dialogue-head">
				<span class="dialogue-title icon-lamp">系统提示</span>
			</div>
			<div class="dialogue-body" style="min-height: 0px;"><div  func="form_submit" style="height:40px"><input type="hidden" name="uid" value="1197"><input type="text" name="fandian" placeholder="请输入新的返点" style="float:left;width:400px;padding:10px 15px"><button type="submit" class="btn btn-blue" style="float:left;margin-left:15px;width:120px;height:39px;line-height:35px;font-size:14px">修改</button></div><style>.uetip .btn{padding:0 5px;margin:0 5px}</style><p class="uetip" style="font-size:12px;color:#999;margin-top:10px">新返点必须是<span class="btn btn-red">0.1</span>的倍数，并且不得小于<span class="btn btn-blue">0.1</span>不得大于<span class="btn btn-green">0.9</span></p></div>
			<div class="dialogue-foot" style="display: block;">
				<div class="dialogue-auto" style="display: none;">
					<span class="dialogue-sec">9</span>秒后自动关闭
				</div>
				<div class="right">
					<button class="dialogue-yes btn btn-blue icon-ok" style="display: none;">确定</button>
					<button class="dialogue-no btn btn-white icon-undo QXGD" style="display: inline-block;">取消修改
					</button>
				</div>
			</div>
		</div>
	</div>
</div>

<%@include file = '../layout/systemTip.jsp'%>
</body>
<script src="../js/admin/adminTeam.js"></script>
</html>