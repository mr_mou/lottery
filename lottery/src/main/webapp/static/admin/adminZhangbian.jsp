<%@ page pageEncoding="utf-8"%>
<%@include file = '../layout/adminCommon.jsp'%>
			<div id="coin-panel" class="panel-head">
				<div class="warp" onclick="modelFunc()">
					<div class="item">
						<div class="item-name icon-right-dir">账户类</div>
						<div class="item-data">
							<a href="javascript:;" data-type="55">注册奖励</a>
							<a href="javascript:;" data-type="1">用户充值</a>
							<a href="javascript:;" data-type="9">系统充值</a>
							<a href="javascript:;" data-type="54">充值奖励</a>
							<a href="javascript:;" data-type="106">提现冻结</a>
							<a href="javascript:;" data-type="12">上级转款</a>
							<a href="javascript:;" data-type="8">提现失败返还</a>
							<a href="javascript:;" data-type="107">提现成功扣除</a>
							<a href="javascript:;" data-type="51">绑定银行奖励</a>
						</div>
					</div>
					<div class="item">
						<div class="item-name icon-right-dir">游戏类</div>
						<div class="item-data">
							<a href="javascript:;" data-type="101">投注扣款</a>
							<a href="javascript:;" data-type="108">开奖扣除</a>
							<a href="javascript:;" data-type="6">中奖奖金</a>
							<a href="javascript:;" data-type="7">撤单返款</a>
							<a href="javascript:;" data-type="102">追号投注</a>
							<a href="javascript:;" data-type="5">追号撤单</a>
							<a href="javascript:;" data-type="255">未开奖返还</a>
						</div>
					</div>
					<div class="item">
						<div class="item-name icon-right-dir">代理类</div>
						<div class="item-data">
							<a href="javascript:;" data-type="2">下级返点</a>
							<a href="javascript:;" data-type="3">代理分红</a>
							<a href="javascript:;" data-type="52">充值佣金</a>
							<a href="javascript:;" data-type="53">消费佣金</a>
							<a href="javascript:;" data-type="56">亏损佣金</a>
							<a href="javascript:;" data-type="13">转款给下级</a>
						</div>
					</div>
					<div class="item">
						<div class="item-name icon-right-dir">活动类</div>
						<div class="item-data">
							<a href="javascript:;" data-type="50">签到赠送</a>
							<a href="javascript:;" data-type="120">幸运大转盘</a>
							<a href="javascript:;" data-type="121">积分兑换</a>
						</div>
					</div>
				</div>
				<span class="triangle"></span>
			</div>
			<div id="coin-log" class="common">
				<div class="head">
					<div class="name icon-chart-line">帐变记录</div>
					<div  class="search" data-ispage="true"   func="form_submit">
						<div class="select trans fixed" id="select-type">
							<input type="hidden" name="type" id="input-type" value="0">
							<span class="icon icon-dot-circled"></span>
							<span class="text" id="text-type" style="width: 122px;">请选择上面帐变类型</span>
						</div>
						<div class="timer">
							<input type="text" autocomplete="off" name="fromTime" value="2016-10-11 17:22" id="datetimepicker_fromTime" class="timer Wdate" onclick="WdatePicker()">
							<span class="icon icon-calendar"></span>
						</div>
						<div class="sep icon-exchange"></div>
						<div class="timer">
							<input type="text" autocomplete="off" name="toTime" value="2016-10-18 17:24" id="datetimepicker_toTime" class="timer Wdate" onclick="WdatePicker()">
							<span class="icon icon-calendar"></span>
						</div>
						<button type="submit" class="btn btn-brown icon-search" id="queryMoneyAccountDetail">查询</button>
					</div>
				</div>
				<div class="body">
					<div class="empty">
						<table  cellspacing="0" cellpadding="0" class="touzhutable" id="moneyAccountList">
							<thead>
								<th>单号</th>
								<th>类型</th>
								<th>时间</th>
								<th>资金</th>
								<th>金额</th>
								<th>备注</th>
							</thead>
							<!-- <tbody>
								<tr>
									<td>T232323232</td>
									<td>重庆时时彩</td>
									<td>五星自选</td>
									<td>9.23</td>
									<td>20161019005</td>
									<td>01,02</td>
								</tr>
							</tbody> -->
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@include file = '../layout/systemTip.jsp'%>
</body>
<script src="../js/admin/adminZhangbian.js"></script>
</html>