drop table if exists message;
create table message(
	message_id bigint unsigned not null auto_increment comment '主键',
	message_title varchar(200) not null comment '消息标题',
	message_content text(1000) not null comment '消息内容',
	message_state varchar(2) default '01' comment '消息状态，01：未读，02：已读',
	send_user_code varchar(32) not null comment '发送方',
	receive_user_code varchar(32) not null comment '接收方',
	created_date timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
	update_date timestamp default CURRENT_TIMESTAMP comment '更新时间',
	PRIMARY KEY (`message_id`)
) comment='消息';