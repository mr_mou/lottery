create table base_lottery_type(
	created_user varchar(20) NOT NULL default 'sys' comment '创建人',
    created_date timestamp NOT NULL default current_timestamp comment '创建时间',
    updated_user varchar(20) default 'sys' comment '修改人',
    updated_date timestamp  default current_timestamp comment '修改时间',
	lottery_type_id int auto_increment primary key comment '彩种id',
    lottery_type_name varchar(100) not null comment'彩种名称',
    lottery_type_img_url varchar(2100) comment '彩种走势图url',
    lottery_type_base_amount double not null comment '彩种单价基础值'
)

create table base_lottery_play(
	created_user varchar(20) NOT NULL default 'sys' comment '创建人',
    created_date timestamp NOT NULL default current_timestamp comment '创建时间',
    updated_user varchar(20) default 'sys' comment '修改人',
    updated_date timestamp  default current_timestamp comment '修改时间',
    lottery_play_id int auto_increment primary key comment'玩法id',
    lottery_play_name varchar(50) not null comment '玩法名称',
    lottery_play_base_prize double not null comment '玩法奖金基础值',
	lottery_type_id int not null comment '彩种id',
    status_code int default 1 comment '1:启用，0:禁用'
)


create table platform_image(
	created_user varchar(20) NOT NULL default 'sys' comment '创建人',
    created_date timestamp NOT NULL default current_timestamp comment '创建时间',
    updated_user varchar(20) default 'sys' comment '修改人',
    updated_date timestamp  default current_timestamp comment '修改时间',
    platform_image_id int auto_increment primary key comment'图片id',
    platform_image_name varchar(100) not null comment '图片名称',
    platform_image_data MEDIUMTEXT comment '图片base64流',
    platform_image_status_code int default 1 comment '图片状态，1:启用 2:禁用',
    platform_image_type int default 1 comment '图片类型，1:收款账号 9:其他'
)

create table platform_base_info(
	created_user varchar(20) NOT NULL default 'sys' comment '创建人',
    created_date timestamp NOT NULL default current_timestamp comment '创建时间',
    updated_user varchar(20) default 'sys' comment '修改人',
    updated_date timestamp  default current_timestamp comment '修改时间',
    base_info_id int auto_increment primary key comment'主键id',
    user_cash_number int not null default 0 comment '用户提现次数',
    custom_play_return_rate double not null comment '自有玩法返奖率',
	cancel_order_switch int not null default 1 comment '撤单开关：1、可撤单 0、不可撤单'
)

insert into platform_base_info(custom_play_return_rate)values(1);
