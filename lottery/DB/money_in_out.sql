DROP TABLE IF EXISTS money_in_out;
CREATE TABLE money_in_out(
	money_in_out_id bigint unsigned not null auto_increment comment '主键',
	user_code varchar(32) not null comment '用户编码',
	in_bank_code varchar(6) comment '充值银行卡代码',
	in_bank_type varchar(32) comment '充值银行卡类型',
	amount decimal(12, 2) not null comment '充值，取现金额',
	opt_type varchar(2) not null comment '操作类似，01：充值，02：提取',
	state varchar(2) not null comment '状态，01：待审核，02：充值成功，03：取现成功，04：拒绝充值，05：拒绝取现',
	remark varchar(200) comment '备注',
	created_date timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
	update_date timestamp default CURRENT_TIMESTAMP comment '更新时间',
	PRIMARY KEY (`money_in_out_id`)
) comment='充值，取现表';

DROP TABLE IF EXISTS money_account;
CREATE TABLE money_account(
	money_account_id bigint unsigned not null auto_increment comment '主键',
	user_code varchar(32) not null comment '用户编码',
	opt_type varchar(2) not null comment '操作类型，1:用户充值，2：系统充值，3：充值奖励，4：提现冻结，5：上级转款，6：提现失败返还，7：提现成功扣除，9：绑定银行卡奖励，10：开奖扣除，11：中奖奖金，12：撤单返款，13：追号投注，14：追号撤单，15：未开奖返还，16：代理分红，17：充值佣金，18：消费佣金，19：亏损佣金，20：转款给下级，21：信用大转盘，22：积分兑换',
	opt_money decimal(12, 2) not null comment '充值，取现的金额',
	total_money decimal(12, 2) not null comment '剩余金额',
	remark varchar(200) comment '备注',
	created_date timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
	update_date timestamp default CURRENT_TIMESTAMP comment '更新时间',
	PRIMARY KEY (`money_account_id`)
) comment='账变表';

