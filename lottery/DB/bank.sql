DROP TABLE IF EXISTS player_bank_info;
CREATE TABLE player_bank_info (
	bank_id bigint unsigned not null auto_increment comment 'id',
	user_code varchar(32) not null comment '用户编码',
	bank_code varchar(32) not null comment '银行类型代码',
	bank_type varchar(32) not null comment '银行类型',
	bank_account varchar(32) not null comment '银行卡账户',
	bank_user varchar(32) not null comment '银行卡所属用户',
	bank_name varchar(100) not null comment '开户行',
	bank_password varchar(32) not null comment '取款密码',
	create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
	update_time timestamp default CURRENT_TIMESTAMP comment '修改时间',
	PRIMARY KEY (`bank_id`)
) comment = '银行卡信息表';
