/*
SQLyog Ultimate v11.24 (32 bit)
MySQL - 5.5.28 : Database - db_9a2a9a_lottery
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_9a2a9a_lottery` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `db_9a2a9a_lottery`;

/*Table structure for table `account` */

DROP TABLE IF EXISTS `account`;

CREATE TABLE `account` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_code` varchar(32) NOT NULL COMMENT '用户编码',
  `user_name` varchar(64) NOT NULL COMMENT '用户名',
  `PASSWORD` varchar(32) NOT NULL COMMENT '密码',
  `real_name` varchar(64) DEFAULT NULL COMMENT '真实姓名',
  `mobile` varchar(16) DEFAULT NULL COMMENT '手机',
  `mail` varchar(64) DEFAULT NULL COMMENT '邮箱',
  `qq` varchar(16) DEFAULT NULL COMMENT 'qq',
  `role` varchar(6) DEFAULT 'player' COMMENT '三种角色，不分开建表了。管理员:admin; 代理: agent; 玩家:player',
  `STATUS` char(1) DEFAULT '1' COMMENT '状态，0失效，1有效',
  `create_time` timestamp NULL DEFAULT NULL,
  `modify_time` timestamp NULL DEFAULT NULL,
  `modify_user` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_user_code_index` (`user_code`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

/*Data for the table `account` */

/*Table structure for table `agent_player` */

DROP TABLE IF EXISTS `agent_player`;

CREATE TABLE `agent_player` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `agent` varchar(32) NOT NULL COMMENT '代理人code',
  `player` varchar(32) NOT NULL COMMENT '玩家code',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='代理和玩家关系表';

/*Data for the table `agent_player` */

/*Table structure for table `lottery_record_detail` */

DROP TABLE IF EXISTS `lottery_record_detail`;

CREATE TABLE `lottery_record_detail` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `created_user` varchar(20) NOT NULL COMMENT '创建人',
  `created_date` datetime NOT NULL COMMENT '创建时间',
  `updated_user` varchar(20) DEFAULT NULL COMMENT '修改人',
  `updated_date` datetime DEFAULT NULL COMMENT '修改时间',
  `lottery_user_id` varchar(32) NOT NULL COMMENT '投注玩家id',
  `lottery_code` varchar(200) NOT NULL COMMENT '彩票开奖号码',
  `lottery_type` varchar(50) NOT NULL COMMENT '彩票种类',
  `lottery_play` varchar(50) NOT NULL COMMENT '彩票玩法',
  `lottery_betting_code` varchar(200) NOT NULL COMMENT '投注号码',
  `lottery_betting_number` int(11) NOT NULL DEFAULT '1' COMMENT '投注数量',
  `lottery_betting_times` double NOT NULL COMMENT '投注金额',
  `lottery_betting_mode` char(1) NOT NULL COMMENT '投注模式（1、元 2、角 3、分）',
  `lottery_betting_prize` double NOT NULL DEFAULT '0' COMMENT '投注奖金',
  `lottery_betting_status` char(2) DEFAULT '1' COMMENT '状态:1、未开奖 2、开奖中 3、已中奖 4、未中奖 5、已撤单',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `lottery_record_detail` */

/*Table structure for table `sequence` */

DROP TABLE IF EXISTS `sequence`;

CREATE TABLE `sequence` (
  `seq_name` varchar(50) NOT NULL COMMENT '序列名称',
  `current_val` int(11) NOT NULL COMMENT '当前值',
  `increment_val` tinyint(4) NOT NULL DEFAULT '1' COMMENT '步长(跨度)',
  PRIMARY KEY (`seq_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='序列表，可添加多个序列';

/*Data for the table `sequence` */

insert  into `sequence`(`seq_name`,`current_val`,`increment_val`) values ('account_seq',0,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
