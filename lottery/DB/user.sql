CREATE DATABASE /*!32312 IF NOT EXISTS*/`lottery` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `lottery`;

drop table if exists sequence;   
create table sequence (   
    seq_name        varchar(50) not null comment '序列名称',    
    current_val     INT not null comment '当前值',    
    increment_val   TINYINT not null DEFAULT 1 comment '步长',   
    PRIMARY KEY (seq_name)
) comment='序列表，可添加多个序列';
-- init data
insert into sequence(seq_name,current_val) values('account_agent_seq',0);
insert into sequence(seq_name,current_val) values('account_player_seq',0);

DROP TABLE IF EXISTS `account`;
CREATE TABLE account (
  id int(8) unsigned not null AUTO_INCREMENT comment 'id',
  user_code varchar(32) NOT NULL comment '用户编码',
  user_name varchar(64) NOT NULL comment '用户名',
  password varchar(32) NOT NULL comment '密码',
  real_name varchar(64) default null comment '真实姓名', 
  mobile varchar(16) default null comment '手机', 
  mail varchar(64) default null comment '邮箱', 
  qq varchar(16) default null comment 'qq', 
  role varchar(6) default 'player' comment '三种角色，不分开建表了。管理员:admin; 代理: agent; 玩家:player', 
  status char(1) default '1' comment '状态，0失效，1有效',
  layer tinyint unsigned comment '层级，管理为1级，依此类推',
  create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
  modify_time timestamp not null default '2016-01-01 00:00:00'comment '修改时间',
  modify_user varchar(32) default null comment '修改人',
  PRIMARY KEY (`id`),
  UNIQUE (user_name)
) comment='用户表';

CREATE UNIQUE INDEX account_user_code_index ON account(user_code);
-- init admin
insert into account(user_code, user_name, password, role, layer) 
	values('admin1','admin','e10adc3949ba59abbe56e057f20f883e','admin',1);

DROP TABLE IF EXISTS `agent_player`;
CREATE TABLE agent_player (
  id int(8) unsigned not null AUTO_INCREMENT comment 'id',
  agent varchar(32) not null comment '代理人code',
  player varchar(32) not null comment '玩家code',
  PRIMARY KEY (`id`)
) comment='代理和玩家关系表';
