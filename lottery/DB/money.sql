DROP TABLE IF EXISTS player_money_detail;
CREATE TABLE player_money_detail (
  id bigint unsigned not null auto_increment comment 'id',
  user_code varchar(32) not null comment '用户编码',
  amount decimal(12,2) not null default 0 comment '金额，单位为元',
  type tinyint unsigned default 0 comment '类型，0:支出，1:收入',
  reason tinyint unsigned default 1 comment '资金变更的原因，1:银行卡，2:支付宝，3:微信，4:中奖奖金，8:系统优惠,9:提现',
  relation_id varchar(32) default null comment '关联id,比如当reason为奖金时关联购彩id，为系统优惠时关联优惠活动id',
  create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
  PRIMARY KEY (`id`)
) comment='玩家资金流水表-不包含购彩';

DROP TABLE IF EXISTS player_money;
CREATE TABLE player_money (
  id int(16) unsigned not null auto_increment comment 'id',
  user_code varchar(32) not null comment '用户编码',
  amount decimal(12,2) not null default 0 comment '金额，单位为元',
  create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
  PRIMARY KEY (`id`)
) comment='玩家现有资金表';