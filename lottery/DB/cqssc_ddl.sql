CREATE TABLE `cqssc` (
  `expect` varchar(13) COLLATE utf8mb4_bin NOT NULL,
  `opencode` varchar(9) COLLATE utf8mb4_bin DEFAULT NULL,
  `opentime` varchar(19) COLLATE utf8mb4_bin DEFAULT NULL,
  `opentimestamp` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`expect`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='重庆时时彩开奖结果表';